package com.example.baselibrary.net;


import androidx.annotation.Nullable;
import com.example.baselibrary.LoadCallback;
import retrofit2.Call;
/**
 * 预处理服务器返回的数据，需要传入一个{@link LoadCallback 对象}
 * create by zzx
 * create at 18-10-15
 */
public class HandleCallbackImpl<T> extends HandleCallback<T> {

    private LoadCallback mLoadCallback;

    public HandleCallbackImpl(@Nullable LoadCallback loadCallback) {
        this(loadCallback, HttpManager.DEFAULT_REQUEST_TAG);
    }

    public HandleCallbackImpl(@Nullable LoadCallback loadCallback, @Nullable String tag) {
        super(tag);
        this.mLoadCallback = loadCallback;
    }


    @Override
    protected void onSuccess(Call<T> call, T response) {
        if (((Response) response).data == null) {
            mLoadCallback.loadNull(((Response) response).message);
        } else {
            mLoadCallback.loadSuccess(((Response) response).data);
        }
    }

    @Override
    protected void onFailed(Throwable t) {
        mLoadCallback.loadFailed(ExceptionEngine.handleException(t).message);
    }

}
