package com.example.baselibrary.net;

public class ServerException extends RuntimeException {
    public int code;
    public String message;
}
