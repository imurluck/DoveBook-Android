package com.example.baselibrary.net;

public interface NetCallback {

    void onSuccess(String responseString, String tag);

    void onFailed(Throwable t, String tag);
}
