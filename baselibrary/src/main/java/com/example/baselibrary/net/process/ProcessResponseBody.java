package com.example.baselibrary.net.process;

import android.os.SystemClock;
import androidx.annotation.Nullable;
import com.example.baselibrary.commonutil.AppExecutors;
import okhttp3.MediaType;
import okhttp3.ResponseBody;
import okio.*;

import java.io.IOException;

public class ProcessResponseBody extends ResponseBody {

    private ProcessListener mProcessListener;

    private ResponseBody mDelegate;

    private long mRefreshTime;

    private ProcessInfo mProcessInfo;

    private BufferedSource mBufferedSource;

    private AppExecutors mAppExecutors;

    public ProcessResponseBody(@Nullable ProcessListener listener,
                               @Nullable ResponseBody delegate,
                               long refreshTime) {
        mProcessListener = listener;
        mDelegate = delegate;
        mRefreshTime = refreshTime;
        mProcessInfo = new ProcessInfo();
        mAppExecutors = new AppExecutors();
    }

    @Nullable
    @Override
    public MediaType contentType() {
        return mDelegate.contentType();
    }

    @Override
    public long contentLength() {
        return mDelegate.contentLength();
    }

    @Override
    public BufferedSource source() {
        if (mBufferedSource == null) {
            mBufferedSource = Okio.buffer(source(mDelegate.source()));
        }
        return mBufferedSource;
    }

    private Source source(Source source) {
        return new ForwardingSource(source) {

            private long totalBytesRead = 0L;
            private long lastRefreshTime = 0L;

            @Override
            public long read(Buffer sink, final long byteCount) throws IOException {
                try {
                    super.read(sink, byteCount);
                } catch (final IOException e) {
                    e.printStackTrace();
                    mAppExecutors.mainThread().execute(() -> mProcessListener.onError(e));

                }
                if (mProcessInfo.getTotleBytes() == 0) {
                    mProcessInfo.setTotleBytes(contentLength());
                }
                totalBytesRead += byteCount;
                final long curTime = SystemClock.elapsedRealtime();
                if (curTime - lastRefreshTime >= mRefreshTime
                        || totalBytesRead >= mProcessInfo.getTotleBytes()) {
                    mAppExecutors.mainThread().execute(() -> {
                        mProcessInfo.setCurrentBytes(totalBytesRead);
                        mProcessInfo.setIntervalTime(curTime - lastRefreshTime);
                        mProcessInfo.setFinish(totalBytesRead >= mProcessInfo.getTotleBytes());
                        mProcessInfo.setEachBytes(byteCount);
                        mProcessListener.onProcess(mProcessInfo);
                    });
                    lastRefreshTime = curTime;
                }
                return byteCount;
            }
        };
    }
}
