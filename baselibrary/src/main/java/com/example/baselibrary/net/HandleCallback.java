package com.example.baselibrary.net;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.HttpException;
import retrofit2.Response;
/**
 * 预处理服务器返回的数据，提前处理服务器返回的数据是否正常
 * create by zzx
 * create at 18-10-15
 */
public abstract class HandleCallback<T> implements Callback<T> {

    protected String tag;

    public HandleCallback() {
        this(HttpManager.DEFAULT_REQUEST_TAG);
    }

    public HandleCallback(String tag) {
        this.tag = tag;
    }

    @Override
    public void onResponse(Call<T> call, Response<T> response) {
        HttpManager.getInstance().removeTag(tag);
        if (response.body() == null) {
            onFailure(call, new HttpException(response));
            return ;
        }
        onSuccess(call, response.body());
    }

    @Override
    public void onFailure(Call<T> call, Throwable t) {
        HttpManager.getInstance().removeTag(tag);
        onFailed(t);
    }

    protected abstract void onSuccess(Call<T> call, T response);

    protected abstract void onFailed(Throwable t);
}
