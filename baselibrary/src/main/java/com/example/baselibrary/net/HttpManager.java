package com.example.baselibrary.net;

import android.text.TextUtils;

import com.example.baselibrary.commonutil.Logger;

import java.util.concurrent.CopyOnWriteArraySet;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
/**
 * 封装网络请求的通用方法
 * @author imurluck
 * @time 18-9-22 下午3:49
 */

public class HttpManager {

    private static final String TAG = "HttpManager";

    public static final String DEFAULT_REQUEST_TAG = "DEFAULT_REQUEST_TAG";

    private static volatile HttpManager sInstance;

    private HttpLoggingInterceptor mLogInterceptor;

    private CopyOnWriteArraySet<String> mRequestTags;

    private HttpManager() {
        mLogInterceptor = new HttpLoggingInterceptor(new HttpLoggingInterceptor.Logger() {
            @Override
            public void log(String message) {
                Logger.e(TAG, "http: " + message);
            }
        }).setLevel(HttpLoggingInterceptor.Level.BODY);
        mRequestTags = new CopyOnWriteArraySet<>();
    }

    public static HttpManager getInstance() {
        if (sInstance == null) {
            synchronized (HttpManager.class) {
                if (sInstance == null) {
                    sInstance = new HttpManager();
                }
            }
        }
        return sInstance;
    }

    /**
     * 不管当前请求是否结束，继续进行下一个请求
     * 获取Api类的对象
     * @param apiClazz Api的Class对象
     * @param baseUrl 基础URL
     * @param <API> 
     * @return
     */
    public <API> API getApiService(Class<API> apiClazz, String baseUrl) {
        return getApiService(apiClazz, baseUrl, DEFAULT_REQUEST_TAG);
    }

    /**
     *
     * @param apiClazz
     * @param baseUrl
     * @param tag 为某一个API请求标记，当前相同请求未结束时，下一次请求将会返回一个空的API 对象
     *            防止进行重复请求
     * @param <API>
     * @return
     */
    public <API> API getApiService(Class<API> apiClazz, String baseUrl, String tag) {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        OkHttpClient client = builder.connectTimeout(15, TimeUnit.SECONDS)
                .readTimeout(15, TimeUnit.SECONDS)
                .addInterceptor(mLogInterceptor)
                .build();
        return getApiService(apiClazz, client, baseUrl, tag);
    }

    /**
     * 获取Api类的对象
     * @param apiClazz Api的Class对象
     * @param client 自己提供的OkHttp客户端
     * @param baseUrl 基础URL
     * @param <API>
     * @return
     */
    public <API> API getApiService(Class<API> apiClazz, OkHttpClient client, String baseUrl, String tag) {
        if (mRequestTags.contains(tag)) {
             return null;
        }
        if (!TextUtils.equals(tag, DEFAULT_REQUEST_TAG)) {
            mRequestTags.add(tag);
        }
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        return retrofit.create(apiClazz);
    }

    public void removeTag(String tag) {
        if (mRequestTags.contains(tag)) {
            mRequestTags.remove(tag);
        }
    }
}
