package com.example.baselibrary.widget;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.airbnb.lottie.LottieAnimationView;
import com.example.baselibrary.R;

import java.util.HashMap;

/**
 * 加载view state：加载中，空页面，加载成功
 * create by zzx
 * create at 18-10-10
 */
public class LoadingStateView extends RelativeLayout {

    private static final int STATE_LOADING = 0;
    private static final int STATE_FAILED = 1;
    private static final int STATE_SUCCESS = 2;
    private static final int STATE_NO_MORE = 3;
    private static final int STATE_EMPTY = 4;

    private HashMap<Integer, View> mMap = new HashMap<>();

    private int mCurrentState = STATE_LOADING;

    private LinearLayout mContainerLayout;

    private LottieAnimationView mLoadingView;
    private TextView mFailedTv;
    private TextView mNoMoreTv;
    private LottieAnimationView mEmptyView;
    private GradientDrawable mMessageShape;

    private OnRetryListener mOnRetryListener;

    public LoadingStateView(Context context) {
        this(context, null);
    }

    public LoadingStateView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public LoadingStateView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        LayoutInflater.from(context)
                .inflate(R.layout.view_loading_state, this, true);
        setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT));
        initChild();
    }


    private void initChild() {
        mLoadingView = findViewById(R.id.loading_view);
        mEmptyView = findViewById(R.id.empty_view);
        mFailedTv = findViewById(R.id.message_tv);
        mNoMoreTv = findViewById(R.id.no_more_view);
        mFailedTv.setOnClickListener((messageTv) -> {
            if (mOnRetryListener != null) {
                mOnRetryListener.onRetry(this);
            }
        });
        mMessageShape = (GradientDrawable) mFailedTv.getBackground();
        mMap.put(STATE_LOADING, mLoadingView);
        mMap.put(STATE_FAILED, mFailedTv);
        mMap.put(STATE_EMPTY, mEmptyView);
        mMap.put(STATE_NO_MORE, mNoMoreTv);
    }

    public void setOnRetryListener(@Nullable OnRetryListener listener) {
        this.mOnRetryListener = listener;
    }

    public void setTextColor(int color) {
        mMessageShape.setStroke(1, color);
        mFailedTv.setTextColor(color);
        mNoMoreTv.setTextColor(color);
    }

    public void success() {
        mCurrentState = STATE_SUCCESS;
        updateVisibility(mCurrentState);
    }

    public void failed() {
        mCurrentState = STATE_FAILED;
        updateVisibility(mCurrentState);
    }

    public void empty() {
        mCurrentState = STATE_EMPTY;
        updateVisibility(mCurrentState);
    }

    public boolean isNoMore() {
        return mCurrentState == STATE_NO_MORE;
    }

    public boolean isLoading() {
        return mCurrentState == STATE_LOADING;
    }

    public boolean isEmpty() {
        return mCurrentState == STATE_EMPTY;
    }

    public boolean isFailed() {
        return mCurrentState == STATE_FAILED;
    }

    public boolean isSuccess() {
        return mCurrentState == STATE_SUCCESS;
    }

    private void updateVisibility(int currentState) {
        if (currentState == STATE_SUCCESS) {
            setVisibility(GONE);
            return ;
        }
        setVisibility(VISIBLE);
        for (int state : mMap.keySet()) {
            if (state == currentState) {
                mMap.get(state).setVisibility(VISIBLE);
            } else {
                mMap.get(state).setVisibility(GONE);
            }
        }
    }


    public void loading() {
        mCurrentState = STATE_LOADING;
        updateVisibility(mCurrentState);
    }

    public void noMore() {
        mCurrentState = STATE_NO_MORE;
        updateVisibility(mCurrentState);
    }



    public interface OnRetryListener {

        void onRetry(LoadingStateView loadingStateView);
    }

    @Override
    protected void onVisibilityChanged(@NonNull View changedView, int visibility) {
        if (changedView == this && visibility == GONE) {
            mLoadingView.cancelAnimation();
            mEmptyView.cancelAnimation();
        }
        if (changedView == mLoadingView) {
            if (visibility == VISIBLE) {
                mLoadingView.playAnimation();
            } else {
                mLoadingView.cancelAnimation();
            }
        }
        if (changedView == mEmptyView) {
            if (visibility == VISIBLE) {
                mEmptyView.playAnimation();
            } else {
                mEmptyView.cancelAnimation();
            }
        }
    }
}
