package com.example.baselibrary.widget;

import android.animation.Animator;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import androidx.annotation.Nullable;
import com.airbnb.lottie.LottieAnimationView;
import com.example.baselibrary.R;

import java.util.ArrayList;
import java.util.List;

public class LottieFontViewGroup extends FrameLayout {

    private static final String TAG = "LottieFontViewGroup";

    private static final int DEFAULT_SPANT_COUNT = 4;

    private int mSpantCount = DEFAULT_SPANT_COUNT;

    private int mSpace = getResources().getDimensionPixelSize(R.dimen.lottie_font_group_space);

    private List<View> mFontViews = new ArrayList<>();

    private OnPlayFinishListener mOnFinishListener;

    public LottieFontViewGroup(Context context) {
        this(context, null);
    }

    public LottieFontViewGroup(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public LottieFontViewGroup(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

    }

    public void addView(View view) {
        super.addView(view);
        mFontViews.add(view);
    }

    public void setOnFinishListener(@Nullable OnPlayFinishListener listener) {
        this.mOnFinishListener = listener;
    }

    public void play() {
        for (int i = 0; i < mFontViews.size(); i++) {
            long delay = 200 * i;
            View font = mFontViews.get(i);
            if (font instanceof LottieAnimationView) {
                if (i == mFontViews.size() - 1) {
                    ((LottieAnimationView) font).addAnimatorListener(new Animator.AnimatorListener() {
                        @Override
                        public void onAnimationStart(Animator animation) {

                        }

                        @Override
                        public void onAnimationEnd(Animator animation) {
                            if (mOnFinishListener != null) {
                                mOnFinishListener.onFinish();
                            }
                        }

                        @Override
                        public void onAnimationCancel(Animator animation) {

                        }

                        @Override
                        public void onAnimationRepeat(Animator animation) {

                        }
                    });
                }
                postDelayed(() -> {
                    ((LottieAnimationView) font).playAnimation();
                }, delay);
            }
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int width;
        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        int height;
        int heightMode = MeasureSpec.getMode(heightMeasureSpec);
        if (getChildCount() == 0) {
            return ;
        }
        if (getChildCount() < mSpantCount) {
            width = (getChildAt(0).getMeasuredWidth() + mSpace) * getChildCount() - mSpace;
            height = getChildAt(0).getMeasuredHeight();
        } else {
            width = (getChildAt(0).getMeasuredWidth() + mSpace) * mSpantCount - mSpace;
            int rows = (getChildCount() + mSpantCount - 1) / mSpantCount;
            height = (getChildAt(0).getMeasuredHeight() + mSpace) * rows - mSpace;
        }
        setMeasuredDimension(MeasureSpec.makeMeasureSpec(width, widthMode),
                MeasureSpec.makeMeasureSpec(height, heightMode));
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        if (getChildCount() == 0) {
            return ;
        }
        int currentX = 0;
        int currentY = 0;
        for (int i = 0; i < getChildCount(); i++) {
            View child = getChildAt(i);
            child.layout(currentX, currentY,
                    currentX + child.getMeasuredWidth(), currentY + child.getMeasuredHeight());
            if (i % mSpantCount == mSpantCount - 1) {
                currentX = 0;
                currentY += child.getMeasuredHeight() + mSpace;
            } else {
                currentX += child.getMeasuredWidth() + mSpace;
            }
        }
    }

    public interface OnPlayFinishListener {
        void onFinish();
    }
}
