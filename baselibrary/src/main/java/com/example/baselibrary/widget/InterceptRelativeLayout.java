package com.example.baselibrary.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.ViewConfiguration;
import android.widget.RelativeLayout;
import androidx.annotation.Nullable;

public class InterceptRelativeLayout extends RelativeLayout {

    private static final String TAG = "RecyclerRelativeLayout";

    public static final int ORIENTATION_VERTICAL = 0;
    public static final int ORIENTATION_HORIZATION = 1;

    private int mRecyclerOrientation = ORIENTATION_HORIZATION;

    private int mTouchDownX;
    private int mTouchDownY;

    private int mTouchSlop;

    private VelocityTracker mVelocityTracker;

    private OnScrollListener mOnScrollListener;

    public InterceptRelativeLayout(Context context) {
        this(context, null);
    }

    public InterceptRelativeLayout(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public InterceptRelativeLayout(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        mTouchSlop = ViewConfiguration.get(context).getScaledTouchSlop();
        mVelocityTracker = VelocityTracker.obtain();
    }


    @Override
    public boolean onInterceptTouchEvent(MotionEvent e) {
        boolean intercept = false;
        switch (e.getActionMasked()) {
            case MotionEvent.ACTION_DOWN:
                mTouchDownX = (int) (e.getX() + 0.5f);
                mTouchDownY = (int) (e.getY() + 0.5f);
                intercept = false;
                break;
            case MotionEvent.ACTION_MOVE:
                int x = (int) (e.getX() + 0.5f);
                int y = (int) (e.getY() + 0.5f);
                int dx = x - mTouchDownX;
                int dy = y - mTouchDownY;
                if (mRecyclerOrientation == ORIENTATION_VERTICAL) {
                    intercept = Math.abs(dx) > Math.abs(dy) && Math.abs(dx) > mTouchSlop;
                }
                if (mRecyclerOrientation == ORIENTATION_HORIZATION) {
                    intercept = Math.abs(dy) > Math.abs(dx) && Math.abs(dy) > mTouchSlop;
                }
                break;
            case MotionEvent.ACTION_UP:
                intercept = false;
                break;
        }
        return intercept;
    }

    @Override
    public boolean onTouchEvent(MotionEvent e) {
        mVelocityTracker.addMovement(e);
        switch (e.getActionMasked()) {
            case MotionEvent.ACTION_MOVE:
                mVelocityTracker.computeCurrentVelocity(1000);
                int x = (int) (e.getX() + 0.5f);
                int y = (int) (e.getY() + 0.5f);
                int dx = x - mTouchDownX;
                int dy = y - mTouchDownY;
                if (mOnScrollListener != null && mOnScrollListener.onScroll(mVelocityTracker, dx, dy)) {

                }
                break;
            case MotionEvent.ACTION_UP:
                mVelocityTracker.computeCurrentVelocity(1000);
                x = (int) (e.getX() + 0.5f);
                y = (int) (e.getY() + 0.5f);
                dx = x - mTouchDownX;
                dy = y - mTouchDownY;
                if (mOnScrollListener != null && mOnScrollListener.onScrollFinish(mVelocityTracker, dx, dy)) {

                }
                break;

        }
        return true;
    }

    public void setOnScrollListener(@Nullable OnScrollListener listener) {
        this.mOnScrollListener = listener;
    }

    public interface OnScrollListener {
        /**
         * 手指正在滑动
         * @param dx 横向
         * @param dy 竖向
         */
        boolean onScroll(VelocityTracker velocityTracker, int dx, int dy);

        /**
         * 滑动停止
         */
        boolean onScrollFinish(VelocityTracker velocityTracker, int dx, int dy);
    }
}
