package com.example.baselibrary.commonutil;

import android.view.View;
import com.google.android.material.snackbar.Snackbar;

public class SnackbarUtil {

    public static void shortSnackbar(View parentView, String message) {
        Snackbar.make(parentView, message, Snackbar.LENGTH_SHORT).show();
    }

    public static void longSnackbar(View parentView, String message) {
        Snackbar.make(parentView, message, Snackbar.LENGTH_LONG).show();
    }
}
