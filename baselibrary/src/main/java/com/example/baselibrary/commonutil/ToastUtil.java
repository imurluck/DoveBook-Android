package com.example.baselibrary.commonutil;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.baselibrary.R;

public class ToastUtil {

    public static void shortToast(Context context, String message) {
        Toast toast = makeToast(context, message);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.show();
    }

    public static void longToast(Context context, String message) {
        Toast toast = makeToast(context, message);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.show();
    }

    private static Toast makeToast(Context context, String message) {
        View contentView = LayoutInflater.from(context).inflate(R.layout.view_toast, null);
        ((TextView) contentView).setText(message);
        Toast toast = new Toast(context);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.setView(contentView);
        return toast;
    }
}
