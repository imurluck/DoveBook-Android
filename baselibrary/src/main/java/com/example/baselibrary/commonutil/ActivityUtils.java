package com.example.baselibrary.commonutil;


import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

public class ActivityUtils {

    public static void replaceFragment(FragmentManager manager, Fragment fragment,
                                       int containerId) {
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(containerId, fragment).commit();
    }
}
