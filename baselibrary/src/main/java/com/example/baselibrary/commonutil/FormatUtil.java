package com.example.baselibrary.commonutil;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Formatter;

public class FormatUtil {

    public static String dateFormat(long datetime) {
        long currentTime = System.currentTimeMillis();
        if (currentTime - datetime < 1000 * 60L) {
            return "刚刚";
        }
        if (currentTime - datetime < 1000L * 60 * 10) {
            return new SimpleDateFormat("m").format(new Date(currentTime - datetime)) + "分钟前";
        }
        if (currentTime - datetime < 1000L * 60 * 60) {
            return new SimpleDateFormat("mm").format(new Date(currentTime - datetime)) + "分钟前";
        }
        if (currentTime - datetime < 1000L * 60 * 60 * 24) {
            return new SimpleDateFormat("HH").format(new Date(currentTime - datetime)) + "小时前";
        }
        if (currentTime - datetime < 1000L * 60 * 60 * 24 * 365) {
            return new SimpleDateFormat("MM-dd").format(new Date(datetime));
        }
        return new SimpleDateFormat("yyyy-MM-dd").format(new Date(datetime));
    }

    public static String dateFormatWithN(long datetime) {
        long currentTime = System.currentTimeMillis();
        if (currentTime - datetime < 1000 * 60L) {
            return "刚刚";
        }
        if (currentTime - datetime < 1000L * 60 * 10) {
            return new SimpleDateFormat("m").format(new Date(currentTime - datetime)) + "分钟前";
        }
        if (currentTime - datetime < 1000L * 60 * 60) {
            return new SimpleDateFormat("mm").format(new Date(currentTime - datetime)) + "分钟前";
        }
        if (currentTime - datetime < 1000L * 60 * 60 * 24) {
            return new SimpleDateFormat("HH").format(new Date(currentTime - datetime)) + "小时前";
        }
        if (currentTime - datetime < 1000L * 60 * 60 * 24 * 365) {
            return new SimpleDateFormat("MM-dd\nHH:mm").format(new Date(datetime));
        }
        return new SimpleDateFormat("yyyy-MM-dd\nHH:mm").format(new Date(datetime));
    }

    public static String countFormat(int count) {
        if (count < 10000) {
            return count + "";
        }
        if (count % 10000 / 1000 == 0) {
            return count % 10000 + "w";
        }
        return count % 10000 + "." + count % 10000 / 1000 + "w";
    }

    public static String dateFormatWithLine(long datetime) {
        return new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss").format(new Date(datetime));
    }

    public static String distanceFormat(float distance) {
        if (distance < 1000.0f) {
            return new Formatter().format("%.0f", distance).toString() + "m";
        }
        return new Formatter().format("%.1f", distance / 1000.f).toString() + "km";
    }
}
