package com.example.baselibrary.constant;

public class Constant {

    private static final String DEBUG_HOST = "127.0.0.1:8080";
    private static final String SERVER_HOST = "39.106.8.250:8080";


    public static final String PROVIDER_AUTHORItIES = "com.example.zzx.dovebook.provider";

    public static final String BASE_URL = "http://" + SERVER_HOST + "/Dove_Book/";

    public static final int BOOK_ISBN_LENGTH = 13;


    //url
    //BookShare相关
    public static final String URL_QUERY_HOT_BOOK_SHARE = "book/bookShare/queryHotBookShare";
    public static final String URL_QUERY_BOOK_TRACE = "book/bookShare/queryBookTrace";
    public static final String URL_QUERY_MY_REQUEST = "book/bookShare/queryMyRequest";
    public static final String URL_QUERY_REQUEST_ME = "book/bookShare/queryRequestMe";
    public static final String URL_AGREE_REQUEST = "book/bookShare/agreeRequest";
    public static final String URL_REQUEST_RECEIVE = "book/bookShare/requestReceive";
    public static final String URL_QUERY_FOLLOWERS = "user/queryFollowers";
    public static final String URL_QUERY_FOLLOWINGS = "user/queryFollowings";
    public static final String URL_GET_USER_INFO = "user/getUserInfo";
    public static final String URL_QUERY_RECEIVE_BOOK_SHARE = "book/bookShare/queryReceiveBookShare";
    public static final String URL_PASS_BOOK_SHARE = "book/bookShare/passBookShare";
    public static final String URL_BOOK_SHARE_LIKE = "book/bookShare/bookShareLike";
    public static final String URL_CANCEL_BOOK_SHARE_LIKE = "book/bookShare/cancelBookShareLike";
    public static final String URL_QUERY_COMMENTS = "book/bookShare/queryComments";
    public static final String URL_ADD_COMMENT = "book/bookShare/addComment";
    public static final String URL_REPLY_COMMENT = "book/bookShare/replyComment";
    public static final String URL_SEARCH_BOOK_SHARE = "book/bookShare/searchBookShare";
    public static final String URL_UPLOAD_BOOK_SHARE = "book/bookShare/addBookShare";

    //User相关
    public static final String URL_USER_LOGIN = "user/login";
    public static final String URL_USER_SIGN_UP = "user/signup";
    public static final String URL_USER_QUERY_FOLLOW_COUNT = "user/queryFollowCount";
    public static final String URL_USER_FOLLOW = "user/follow";
    public static final String URL_USER_CANCEL_FOLLOW = "user/cancelFollow";
    public static final String URL_USER_UPDATE_AVATAR = "user/updateAvatar";


    public static final String BASE_URL_BOOK_ISBN = "http://isbn.szmesoft.com/isbn/";
    public static final String BASE_URL_BOOK_PHOTO = "http://isbn.szmesoft.com/ISBN/GetBookPhoto?ID=";

}
