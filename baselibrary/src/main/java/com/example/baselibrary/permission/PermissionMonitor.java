package com.example.baselibrary.permission;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;

/**
 * 权限监控
 * create by zzx
 * create at 18-12-13
 */
@Aspect
public class PermissionMonitor {

    private static final String TAG = "PermissionMonitor";

    @Pointcut("execution(@com.example.baselibrary.permission.PermissionCheck * *(..))")
    public void permissionCheckPointcut() {

    }

    @Around(value = "permissionCheckPointcut()")
    public void permissionCheck(ProceedingJoinPoint point) {
        MethodSignature methodSignature = (MethodSignature) point.getSignature();
        PermissionCheck permissionCheck = methodSignature.getMethod().getAnnotation(PermissionCheck.class);
        PermissionManager.getInstance().handleCheck(permissionCheck.permission(), new PermissionCallback() {

            @Override
            public void agree() {
                try {
                    point.proceed();
                } catch (Throwable throwable) {
                    throwable.printStackTrace();
                }
            }

            @Override
            public void reject() {

            }

            @Override
            public void hasAgreed() {
                try {
                    point.proceed();
                } catch (Throwable throwable) {
                    throwable.printStackTrace();
                }
            }
        });
    }

}
