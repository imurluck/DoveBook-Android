package com.example.baselibrary.permission;

public interface PermissionCallback {

    void agree();

    void reject();

    void hasAgreed();
}
