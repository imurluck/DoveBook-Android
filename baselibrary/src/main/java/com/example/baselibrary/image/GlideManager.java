package com.example.baselibrary.image;

import android.content.Context;
import android.widget.ImageView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestBuilder;
import com.bumptech.glide.RequestManager;
import com.bumptech.glide.load.engine.cache.ExternalCacheDiskCacheFactory;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.example.baselibrary.commonutil.AppExecutors;

import java.io.File;
import java.math.BigDecimal;

public class GlideManager {



    public static void loadImage(Context context, String imageUrl, ImageView targetView) {
        Glide.with(context)
                .load(imageUrl)
                .into(targetView);
    }

    public static void downloadImage(Context context, String imageUrl, RequestListener<File> listener) {
        RequestManager rm = Glide.with(context);
        RequestBuilder<File> rb = rm.downloadOnly();
        rb.load(imageUrl);
        rb.listener(listener);
        rb.preload();
    }

    public static void loadImageBlur(Context context, String imageUrl, ImageView targetView) {
        Glide.with(context)
                .load(imageUrl)
                .apply(RequestOptions.bitmapTransform(new BlurTransformation(25, 10)))
        .into(targetView);
    }

    public static void loadImage(Context context, int imageId, ImageView targetView) {
        Glide.with(context)
                .load(imageId)
                .into(targetView);
    }


    public static void loadImageBlur(Context context, int imageId, ImageView targetView) {
        Glide.with(context)
                .load(imageId)
                .apply(RequestOptions.bitmapTransform(new BlurTransformation(25, 10)))
                .into(targetView);
    }


    public static void clearDiskCache(Context context, CacheClearListener listener) {
        AppExecutors appExecutors = new AppExecutors();
        appExecutors.diskIO().execute(() -> {
            Glide.get(context).clearDiskCache();
            if (listener != null) {
                listener.onFinish(getDiskCacheSize(context));
            }
        });
    }

    public static String getDiskCacheSize(Context context) {
        File cacheDir = new File(context.getCacheDir(), ExternalCacheDiskCacheFactory.DEFAULT_DISK_CACHE_DIR);
        if (!cacheDir.exists()) {
            return getFormatSize(0.0D);
        }
        return getFormatSize(getFolderSize(cacheDir));
    }

    private static long getFolderSize(File file) {
        long size = 0;
        File[] fileList = file.listFiles();
        for (File childFile : fileList) {
            if (childFile.isDirectory()) {
                size += getFolderSize(childFile);
            } else {
                size += childFile.length();
            }
        }
        return size;
    }

    private static final String getFormatSize(double size) {
        double kiloByte = size / 1024;
        if (kiloByte < 1) {
            return size + "Byte";
        }

        double megaByte = kiloByte / 1024;
        if (megaByte < 1) {
            BigDecimal result1 = new BigDecimal(Double.toString(kiloByte));
            return result1.setScale(2, BigDecimal.ROUND_HALF_UP).toPlainString() + "KB";
        }

        double gigaByte = megaByte / 1024;
        if (gigaByte < 1) {
            BigDecimal result2 = new BigDecimal(Double.toString(megaByte));
            return result2.setScale(2, BigDecimal.ROUND_HALF_UP).toPlainString() + "MB";
        }

        double teraBytes = gigaByte / 1024;
        if (teraBytes < 1) {
            BigDecimal result3 = new BigDecimal(Double.toString(gigaByte));
            return result3.setScale(2, BigDecimal.ROUND_HALF_UP).toPlainString() + "GB";
        }
        BigDecimal result4 = new BigDecimal(teraBytes);

        return result4.setScale(2, BigDecimal.ROUND_HALF_UP).toPlainString() + "TB";

    }

    public interface CacheClearListener {

        void onFinish(String restSize);
    }
}
