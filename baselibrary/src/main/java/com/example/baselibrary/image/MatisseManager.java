package com.example.baselibrary.image;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import androidx.fragment.app.Fragment;
import com.example.baselibrary.R;
import com.zhihu.matisse.Matisse;
import com.zhihu.matisse.MimeType;

import java.util.List;

public class MatisseManager {

    public static void start(Activity activity, int resultCode) {
        start(activity, resultCode, 1);
    }

    public static void start(Activity activity, int resultCode, int maxSelectCount) {
        Matisse.from(activity)
                .choose(MimeType.allOf())
                .theme(R.style.MatisseStyle)
                .countable(true)
                .thumbnailScale(0.85f)
                .maxSelectable(maxSelectCount)
                .restrictOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED)
                .imageEngine(new GlideEngine())
                .forResult(resultCode);
    }

    public static void start(Fragment fragment, int resultCode) {
        start(fragment, resultCode, 1);
    }

    public static void start(Fragment fragment, int resultCode, int maxSelectCount) {
        Matisse.from(fragment)
                .choose(MimeType.allOf())
                .theme(R.style.MatisseStyle)
                .countable(true)
                .thumbnailScale(0.85f)
                .maxSelectable(maxSelectCount)
                .restrictOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED)
                .imageEngine(new GlideEngine())
                .forResult(resultCode);
    }

    public static List<Uri> obtainResult(Intent data) {
        return Matisse.obtainResult(data);
    }
}
