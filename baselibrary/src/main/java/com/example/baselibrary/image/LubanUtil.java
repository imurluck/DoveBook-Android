package com.example.baselibrary.image;

import android.content.Context;
import android.text.TextUtils;
import top.zibin.luban.Luban;
import top.zibin.luban.OnCompressListener;

public class LubanUtil {
    public static final void compress(Context context, String filePath, String cacheDirPath, OnCompressListener listener) {
        Luban.with(context)
                .load(filePath)
                .ignoreBy(100)
                .setTargetDir(cacheDirPath)
                .setFocusAlpha(false)
                .filter(path -> !(TextUtils.isEmpty(path) || path.toLowerCase().endsWith(".gif")))
                .setCompressListener(listener)
                .launch();
    }
}