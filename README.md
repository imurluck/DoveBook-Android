# DoveBook Android客户端
### 开发规范
#### 与服务端对接
##### 实体类型
- 客户端的实体类中String属性应初始化为"",防止服务端下发的String为null
##### 上传数据
- 客户端通过表格上传某个字段属性时，若该字段对应值为null，则表格中不应该出现该字段，
防止该字段被服务端解析成""。
#### 客户端开发规范
详情见<a href="https://edu.aliyun.com/certification/cldt04">阿里巴巴Android开发规范</a>
#### 服务器下发数据处理
如使用HandleLoadbackImpl对象进行预处理，则表示服务器下发的数据已经做了判空处理，
后续无需再做判空处理
#### 语言选型
BaseLibrary 和 插件中严禁出现Kotlin代码，否则会造成插件打包失败
