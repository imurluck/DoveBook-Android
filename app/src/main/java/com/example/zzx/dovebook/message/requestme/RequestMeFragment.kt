package com.example.zzx.dovebook.message.requestme

import android.graphics.Color
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import com.example.baselibrary.widget.LoadingStateView
import com.example.library.BaseAdapter
import com.example.zzx.dovebook.R
import com.example.zzx.dovebook.base.BaseFragment
import com.example.zzx.dovebook.base.ValueWrapper
import com.example.zzx.dovebook.message.model.BookShareRequest
import com.example.zzx.dovebook.message.model.BookShareRequestMe
import com.example.zzx.dovebook.user.UserManager
import kotlinx.android.synthetic.main.fragment_request.*


class RequestMeFragment: BaseFragment<RequestMeViewModel>() {

    private lateinit var adapter: BaseAdapter
    private lateinit var emptyView: LoadingStateView
    private lateinit var loadMoreView: LoadingStateView

    private var lastItemCreateAt = System.currentTimeMillis()

    override fun createViewModel(): Class<RequestMeViewModel> {
        return RequestMeViewModel::class.java
    }

    override fun getLayoutId(): Int {
        return R.layout.fragment_request
    }

    override fun createRepository(): Any {
        return RequestMeRepository()
    }

    override fun initViews() {
        setupRecycler()
    }

    override fun initOptions() {
        queryRequestMe()
    }

    private fun setupRecycler() {
        emptyView = LoadingStateView(activity)
        loadMoreView = LoadingStateView(activity)
        emptyView.setTextColor(ContextCompat.getColor(activity!!, R.color.support_text))
        loadMoreView.setTextColor(Color.TRANSPARENT)
        adapter = BaseAdapter.Builder().emptyView(emptyView).addRooter(loadMoreView).build()
        adapter.emptyState()
        adapter.setOnLoadMoreListener {
            if (!loadMoreView.isNoMore) {
                queryRequestMe()
            }
        }
        loadMoreView.setOnRetryListener {
            loadMoreView.loading()
            queryRequestMe()
        }
        emptyView.setOnRetryListener {
            emptyView.loading()
            queryRequestMe()
        }
        adapter.setOnLoadMoreListener {
            if (loadMoreView.isNoMore) {
                return@setOnLoadMoreListener
            }
            queryRequestMe()
        }
        recyclerView.adapter = adapter
    }

    private fun queryRequestMe() {
        val uploadUserId = UserManager.getInstance(activity).loginUserId ?: ""
        adapter.lastItem?.let {
            lastItemCreateAt = (it as BookShareRequestMe).bookShareRequest.createAt
        }
        viewModel.queryRequestMe(uploadUserId, lastItemCreateAt).observe(this, Observer {
            it!!.value ?: run {
                if (adapter.dataCount > 0) {
                    when (it.type) {
                        ValueWrapper.TYPE_FAILED -> loadMoreView.failed()
                        ValueWrapper.TYPE_NO_CONTENT -> loadMoreView.noMore()
                    }
                } else {
                    when (it.type) {
                        ValueWrapper.TYPE_FAILED -> emptyView.failed()
                        ValueWrapper.TYPE_NO_CONTENT -> emptyView.empty()
                    }
                }
                return@Observer
            }
            emptyView.success()
            if (adapter.isInEmptyState) adapter.cancelEmptyState()
            val requestMeList = it.value as List<BookShareRequestMe>
            if (requestMeList.size < 20) {
                loadMoreView.noMore()
            }
            adapter.add(requestMeList)
        })

    }

    fun agreeRequest(bookShareRequestMe: BookShareRequestMe) {
        val uploadUserId = UserManager.getInstance(activity).loginUserId ?: ""
        val bookShareRequest = bookShareRequestMe.bookShareRequest
        viewModel.agreeRequest(bookShareRequest.bsqId, uploadUserId).observe(this, Observer {
            it?.run {
                bookShareRequest.status = BookShareRequest.STATUS_AGREED
                bookShareRequestMe.updateStatus()
            }
        })
    }

}