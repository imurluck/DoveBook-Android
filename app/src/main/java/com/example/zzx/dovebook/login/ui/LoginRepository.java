package com.example.zzx.dovebook.login.ui;

import com.example.baselibrary.LoadCallback;
import com.example.baselibrary.commonutil.AppExecutors;
import com.example.baselibrary.constant.Constant;
import com.example.baselibrary.net.HttpHelper;
import com.example.zzx.dovebook.user.UserManager;
import com.example.zzx.dovebook.user.model.User;

import java.util.Map;

public class LoginRepository {

    private static final String TAG_SIGN_UP = "signUp";
    private static final String TAG_LOGIN = "login";

    private AppExecutors mAppExecutors;

    private UserManager mUserManager;

    public LoginRepository(UserManager userManager) {
        mAppExecutors = new AppExecutors();
        this.mUserManager = userManager;
    }

    public void login(Map<String, String> paraMap, LoadCallback<User> loadCallback) {
       HttpHelper.getInstance().post(Constant.URL_USER_LOGIN, paraMap, new LoadCallback<User>() {

           @Override
           public void loadSuccess(User data) {
                mUserManager.login(data);
                loadCallback.loadSuccess(data);
           }

           @Override
           public void loadFailed(String errorMessage) {
                loadCallback.loadFailed(errorMessage);
           }

           @Override
           public void loadNull(String message) {
                loadCallback.loadNull(message);
           }
       }, TAG_LOGIN);
    }

    public void signUp(Map<String, String> paraMap, LoadCallback<User> loadCallback) {
        HttpHelper.getInstance().post(Constant.URL_USER_SIGN_UP, paraMap, new LoadCallback<User>() {

            @Override
            public void loadSuccess(User data) {
                mAppExecutors.diskIO().execute(() -> mUserManager.signUp(data));
                loadCallback.loadSuccess(data);
            }

            @Override
            public void loadFailed(String errorMessage) {
                loadCallback.loadFailed(errorMessage);
            }

            @Override
            public void loadNull(String message) {
                loadCallback.loadNull(message);
            }
        }, TAG_SIGN_UP);
    }
}
