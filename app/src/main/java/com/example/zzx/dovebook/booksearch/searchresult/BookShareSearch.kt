package com.example.zzx.dovebook.booksearch.searchresult

import android.widget.TextView
import com.example.baselibrary.image.GlideManager
import com.example.library.BaseAdapter
import com.example.library.IEntity
import com.example.zzx.dovebook.R
import com.example.zzx.dovebook.base.extensions.startBookShadeDetailActivity
import com.example.zzx.dovebook.base.model.BookShare
import de.hdodenhof.circleimageview.CircleImageView

class BookShareSearch(
    val searchResultFragment: SearchResultFragment,
    val bookShare: BookShare
) : IEntity<BookShareSearch> {
    override fun bindView(
        baseAdapter: BaseAdapter,
        holder: BaseAdapter.ViewHolder,
        data: BookShareSearch,
        position: Int
    ) {
        holder.itemView.apply {
            setOnClickListener {
                context.startBookShadeDetailActivity(bookShare)
            }
            findViewById<CircleImageView>(R.id.bookImage).apply {
                GlideManager.loadImage(context, bookShare.bookImagePath, this)
            }
            findViewById<TextView>(R.id.bookDescriptionTv).text = "${bookShare.bookTitle}(${bookShare.description})"
            findViewById<TextView>(R.id.bookStateTv).apply {
                if (bookShare.bookStatus == BookShare.STATUS_DRIFTING) {
                    setBackgroundResource(R.drawable.accent_5dp)
                } else {
                    setBackgroundResource(R.drawable.red_5dp)
                }
                text = bookShare.bookStatusTip
            }
            findViewById<TextView>(R.id.userNameTv).text = bookShare.userName
        }
    }

    override fun getLayoutId(): Int = R.layout.item_search_book_share
}