package com.example.zzx.dovebook.utils

import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.view.View

class AnimationUtil {

    companion object {

        @JvmStatic
        fun hide(view: View) {
            val animatorX = ObjectAnimator.ofFloat(view, View.SCALE_X, 0.0F)
            val animatorY = ObjectAnimator.ofFloat(view, View.SCALE_Y, 0.0F)
            val animatorAlpha = ObjectAnimator.ofFloat(view, View.ALPHA, 0.0F)
            val animatorSet = AnimatorSet()
            animatorSet.setDuration(100L)
                    .playTogether(animatorX, animatorY, animatorAlpha)
            animatorSet.start()
        }

        @JvmStatic
        fun show(view: View) {
            val animatorX = ObjectAnimator.ofFloat(view, View.SCALE_X, 1.0F)
            val animatorY = ObjectAnimator.ofFloat(view, View.SCALE_Y, 1.0F)
            val animatorAlpha = ObjectAnimator.ofFloat(view, View.ALPHA, 1.0F)
            val animatorSet = AnimatorSet()
            animatorSet.setDuration(100L)
                    .playTogether(animatorX, animatorY, animatorAlpha)
            animatorSet.start()
        }
    }
}