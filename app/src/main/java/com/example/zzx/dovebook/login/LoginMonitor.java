package com.example.zzx.dovebook.login;

import android.util.Log;


import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;

@Aspect
public class LoginMonitor {

    private static final String TAG = "LoginMonitor";

    @Pointcut("execution(@com.example.zzx.dovebook.login.LoginCheck * *(..))")
    public void loginCheckPointcut() {

    }

    @Around(value = "loginCheckPointcut()")
    public void loginCheck(ProceedingJoinPoint joinPoint) {
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        LoginCheck loginCheck = signature.getMethod().getAnnotation(LoginCheck.class);
        LoginManager.getInstance().handleCheckAction(signature.getMethod().getName(), new LoginCallback() {

            @Override
            public void loginSuccess() {
                try {
                    joinPoint.proceed();
                } catch (Throwable throwable) {
                    throwable.printStackTrace();
                }
            }

            @Override
            public void loginFailed(String errorMessage) {
                Log.e(TAG, "loginFailed: " + errorMessage);
            }

            @Override
            public void hasLogin() {
                try {
                    joinPoint.proceed();
                } catch (Throwable throwable) {
                    throwable.printStackTrace();
                }
            }
        });
    }
}
