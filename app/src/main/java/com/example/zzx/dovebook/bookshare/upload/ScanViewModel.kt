package com.example.zzx.dovebook.bookshare.upload

import com.example.baselibrary.LoadCallback
import com.example.zzx.dovebook.base.BaseLiveData
import com.example.zzx.dovebook.base.BaseViewModel
import com.example.zzx.dovebook.base.ValueWrapper.*

class ScanViewModel(repository: ScanRepository): BaseViewModel<ScanRepository>(repository) {

    fun getBookInfoFromNet(ISBN: String): BaseLiveData<BookNet> {
        val result = BaseLiveData<BookNet>()
        repository.getBookInfoFromNet(ISBN, object: LoadCallback<BookNet> {
            override fun loadSuccess(data: BookNet) {
                if (data.ErrorCode == 6000) {
                    result.postValue(null, TYPE_NO_CONTENT)
                } else {
                    result.postValue(data, TYPE_NORMAL)
                }
            }

            override fun loadFailed(errorMessage: String) {
                result.postValue(null, TYPE_FAILED)
            }

            override fun loadNull(message: String?) {
            }

        })
        return result
    }
}