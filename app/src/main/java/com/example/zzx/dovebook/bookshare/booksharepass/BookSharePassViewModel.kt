package com.example.zzx.dovebook.bookshare.booksharepass

import androidx.lifecycle.LiveData
import com.amap.api.services.core.LatLonPoint
import com.example.baselibrary.LoadCallback
import com.example.baselibrary.net.HttpUtil
import com.example.zzx.dovebook.base.BaseLiveData
import com.example.zzx.dovebook.base.BaseViewModel
import com.example.zzx.dovebook.base.ValueWrapper
import com.example.zzx.dovebook.base.ValueWrapper.*

class BookSharePassViewModel(repository: BookSharePassRepository):
        BaseViewModel<BookSharePassRepository>(repository) {

    private val passBookShareKeys = arrayOf("uploadUserId", "bookId", "description",
            "latitude", "longitude")

    fun passBookShare(coverPath: String, userId: String, bookId: String?, description: String,
                      latLonPoint: LatLonPoint): LiveData<ValueWrapper<Any>> {
        val result = BaseLiveData<Boolean>()
        val coverFile = HttpUtil.createFilePart("coverFile", coverPath)
        val paraMap = HttpUtil.wrapParams(passBookShareKeys, userId, bookId, description,
                latLonPoint.latitude, latLonPoint.longitude)
        repository.passBookShare(paraMap, coverFile, object: LoadCallback<Boolean> {
            override fun loadSuccess(data: Boolean?) {
                result.postValue(data, TYPE_NORMAL)
            }

            override fun loadFailed(errorMessage: String?) {
                postErrorMessage(errorMessage)
                result.postValue(null, TYPE_FAILED)
            }

            override fun loadNull(message: String?) {
                result.postValue(null, TYPE_NO_CONTENT)
            }

        })
        return result
    }

}