package com.example.zzx.dovebook.message.model

import android.widget.RelativeLayout
import android.widget.TextView
import com.example.baselibrary.image.GlideManager
import com.example.library.BaseAdapter
import com.example.library.IEntity
import com.example.zzx.dovebook.R
import com.example.zzx.dovebook.message.MessageActivity

class BookShareMyRequest(val bookShareRequest: BookShareRequest): IEntity<BookShareMyRequest> {


    override fun getLayoutId(): Int {
        return R.layout.item_book_share_my_request
    }

    override fun bindView(baseAdapter: BaseAdapter?, holder: BaseAdapter.ViewHolder?, data: BookShareMyRequest?, position: Int) {
        (holder?.itemView as RelativeLayout).apply {
            val activity = context as MessageActivity
            val myRequestFragment = activity.myRequestFragment
            GlideManager.loadImage(activity, bookShareRequest.bookImagePath, rootView.findViewById(R.id.bookImage))
            findViewById<TextView>(R.id.bookTitle).text = bookShareRequest.bookTitle
            findViewById<TextView>(R.id.uploadUserName).text = "${bookShareRequest.userName} -> 分享"
            findViewById<TextView>(R.id.statusDescription).text = getStatusDescription()
        }

    }

    private fun getStatusDescription(): String {
        return when (bookShareRequest.status) {
            BookShareRequest.STATUS_REJECTED -> "已拒绝"
            BookShareRequest.STATUS_AGREED -> "已同意"
            else -> "等待同意"
        }
    }
}