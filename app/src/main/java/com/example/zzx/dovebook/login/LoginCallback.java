package com.example.zzx.dovebook.login;

public interface LoginCallback {

    void loginSuccess();

    void loginFailed(String errorMessage);

    void hasLogin();
}
