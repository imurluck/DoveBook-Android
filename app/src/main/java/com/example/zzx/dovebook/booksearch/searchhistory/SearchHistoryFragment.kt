package com.example.zzx.dovebook.booksearch.searchhistory

import android.text.TextUtils
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView.VERTICAL
import com.example.baselibrary.widget.LoadingStateView
import com.example.library.BaseAdapter
import com.example.zzx.dovebook.R
import com.example.zzx.dovebook.base.BaseFragment
import com.example.zzx.dovebook.booksearch.BookSearchActivity
import kotlinx.android.synthetic.main.fragment_search_history.*

class SearchHistoryFragment : BaseFragment<SearchHistoryViewModel>() {

    private lateinit var emptyView: LoadingStateView

    private lateinit var adapter: BaseAdapter

    override fun createViewModel(): Class<SearchHistoryViewModel> = SearchHistoryViewModel::class.java

    override fun createRepository(): Any {
        return SearchHistoryRepository(activity!!.application)
    }

    override fun getLayoutId(): Int = R.layout.fragment_search_history

    override fun initViews() {
        setupRecyclerView()
    }

    override fun initOptions() {
        getHistoryFromLocal()
    }

    private fun setupRecyclerView() {
        emptyView = LoadingStateView(activity)
        emptyView.empty()
        adapter = BaseAdapter.Builder()
            .emptyView(emptyView)
            .build()
        recyclerView.layoutManager = LinearLayoutManager(activity, VERTICAL, false)
        recyclerView.adapter = adapter
    }

    private fun getHistoryFromLocal() {
        val historyList = viewModel.getHistoryFromLocal(this)
        if (historyList == null) {
            adapter.emptyState()
        } else {
            adapter.cancelEmptyState()
            adapter.add(historyList)
        }
    }

    fun onHistoryClick(content: String, position: Int) {
        (activity as BookSearchActivity).searchBookShare(content)
        if (position == 0) return
        if (viewModel.updateHistory(content)) {
            adapter.remove(position)
            adapter.add(0, SearchHistory(this, content))
            recyclerView.scrollToPosition(0)
        }
    }

    fun onDeleteIconClick(content: String, position: Int) {
        if (viewModel.deleteHistory(content)) {
            adapter.remove(position)
            if (adapter.dataCount == 0) {
                adapter.emptyState()
            }
        }
    }

    fun addHistory(content: String) {
        if (viewModel.addHistory(content)) {
            if (adapter.isInEmptyState) {
                adapter.cancelEmptyState()
            }
            adapter.add(0, SearchHistory(this, content))
        } else {//表示已经存在此条记录，则需将此条记录移动到顶部
            for (index in 0 until adapter.dataCount) {
                (adapter.dataList[index] as SearchHistory).apply {
                    if (TextUtils.equals(historyContent, content) && index != 0) {
                        adapter.remove(index)
                        adapter.add(0, SearchHistory(this@SearchHistoryFragment, content))
                    }
                }
            }
        }
    }
}

