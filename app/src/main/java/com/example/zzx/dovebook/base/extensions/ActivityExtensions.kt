package com.example.zzx.dovebook.base.extensions

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import com.amap.api.services.core.LatLonPoint
import com.example.baselibrary.permission.PermissionCheck
import com.example.zzx.dovebook.R
import com.example.zzx.dovebook.base.model.BookShare
import com.example.zzx.dovebook.bookshare.booksharedetail.BookShareDetailActivity
import com.example.zzx.dovebook.bookshare.upload.BookNet
import com.example.zzx.dovebook.bookshare.upload.BookShareUploadActivity
import com.example.zzx.dovebook.bookshare.upload.ScanActivity
import com.example.zzx.dovebook.location.LocationActivity
import com.example.zzx.dovebook.login.ui.LoginActivity
import com.example.zzx.dovebook.main.MainActivity
import com.example.zzx.dovebook.message.MessageActivity
import com.example.zzx.dovebook.personalcenter.UserDetailActivity
import com.example.zzx.dovebook.personalcenter.follow.follower.FollowerActivity
import com.example.zzx.dovebook.personalcenter.follow.following.FollowingActivity
import com.example.zzx.dovebook.setting.SettingActivity

/**
 * 跳转到[BookShareDetailActivity], 传递[bookShare]过去
 */
internal fun Context.startBookShadeDetailActivity(bookShare: BookShare) {
    startActivity(Intent(this, BookShareDetailActivity::class.java).apply {
        putExtra(getString(R.string.book_share_arg_name), bookShare)
    })
}

internal fun Context.startUserDetailActivity(userId: String, userName: String?) {
    startActivity(Intent(this, UserDetailActivity::class.java).apply {
        putExtra(getString(R.string.user_id_arg_name), userId)
        putExtra(getString(R.string.user_name_arg_name), userName)
    })
}

internal fun Context.startFollowingActivity(userId: String, userName: String) {
    startActivity(Intent(this, FollowingActivity::class.java).apply {
        putExtra(getString(R.string.user_id_arg_name), userId)
        putExtra(getString(R.string.user_name_arg_name), userName)
    })
}

internal fun Context.startFollowerActivity(userId: String, userName: String) {
    startActivity(Intent(this, FollowerActivity::class.java).apply {
        putExtra(getString(R.string.user_id_arg_name), userId)
        putExtra(getString(R.string.user_name_arg_name), userName)
    })
}

internal fun Context.startSettingActivity() {
    startActivity(Intent(this, SettingActivity::class.java))
}

internal fun Context.startLoginActivity(callId: String?) {
    startActivity(Intent(this, LoginActivity::class.java).apply {
        putExtra(getString(R.string.call_id_arg_name), callId)
    })
}

internal fun Context.startMainActivity() {
    startActivity(Intent(this, MainActivity::class.java))
}

internal fun Context.startRequestMessageActivity() {
    startActivity(Intent(this, MessageActivity::class.java))
}

internal fun Context.startLocationActivity(latLonPoint: LatLonPoint?, keyword: String?) {
    startActivity(Intent(this, LocationActivity::class.java).apply {
        putExtra(getString(R.string.lat_lng_point_arg_name), latLonPoint)
        putExtra(getString(R.string.location_keyword_arg_name), keyword)
    })
}

internal fun Activity.startLocationActivityForResult(latLonPoint: LatLonPoint?, keyword: String?, requestCode: Int) {
    startActivityForResult(Intent(this, LocationActivity::class.java).apply {
        putExtra(getString(R.string.lat_lng_point_arg_name), latLonPoint)
        putExtra(getString(R.string.location_keyword_arg_name), keyword)
    }, requestCode)
}

@PermissionCheck(permission = Manifest.permission.CAMERA)
internal fun Context.startScanActivity() {
    startActivity(Intent(this, ScanActivity::class.java))
}

internal fun Context.startBookShareUploadActivity(bookNet: BookNet) {
    startActivity(Intent(this, BookShareUploadActivity::class.java).apply {
        putExtra(getString(R.string.book_net_arg_name), bookNet)
    })
}