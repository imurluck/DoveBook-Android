package com.example.zzx.dovebook.base;


import androidx.lifecycle.MutableLiveData;

public class BaseLiveData<D> extends MutableLiveData<ValueWrapper> {



    @Override
    public void postValue(ValueWrapper value) {
        super.postValue(value);
    }

    @Override
    public void setValue(ValueWrapper value) {
        super.setValue(value);
    }

    public void setValue(D value, int type) {
        ValueWrapper<D> wrapper = new ValueWrapper<>(type, value);
        super.setValue(wrapper);
    }

    public void postValue(D value, int type) {
        super.postValue(new ValueWrapper(type, value));
    }
}
