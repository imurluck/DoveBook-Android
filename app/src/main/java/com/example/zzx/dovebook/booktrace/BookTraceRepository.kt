package com.example.zzx.dovebook.booktrace

import com.example.baselibrary.LoadCallback
import com.example.baselibrary.constant.Constant
import com.example.baselibrary.net.HttpHelper
import com.example.zzx.dovebook.booktrace.model.BookTrace

class BookTraceRepository {
    private val TAG_QUERY_BOOK_TRACE = "queryBookTrace"
    private val TAG_REQUEST_RECEIVE = "requestReceive"

    fun queryBookTrace(paraMap: Map<String, String>, loadCallback: LoadCallback<List<BookTrace>>) {
        HttpHelper.getInstance().post(Constant.URL_QUERY_BOOK_TRACE, paraMap, loadCallback, TAG_QUERY_BOOK_TRACE)
    }

    fun requestReceive(paraMap: Map<String, String>, loadCallback: LoadCallback<Boolean>) {
        HttpHelper.getInstance().post(Constant.URL_REQUEST_RECEIVE, paraMap, loadCallback, TAG_REQUEST_RECEIVE)
    }

}