package com.example.zzx.dovebook.location

import android.widget.TextView
import com.amap.api.services.core.PoiItem
import com.example.library.BaseAdapter
import com.example.library.IEntity
import com.example.zzx.dovebook.R

class Location(private val poiItem: PoiItem): IEntity<Location> {


    override fun bindView(baseAdapter: BaseAdapter?, holder: BaseAdapter.ViewHolder, data: Location?, position: Int) {
        holder.itemView.apply {
            val activity = context as LocationActivity
            setOnClickListener {
                activity.returnWithLocation(poiItem)
            }
            findViewById<TextView>(R.id.locationName).text = poiItem.title
            findViewById<TextView>(R.id.locationDescription).text = "${poiItem.provinceName}${poiItem.cityName}" +
                    "${poiItem.adName}${poiItem.snippet}"
        }
    }

    override fun getLayoutId(): Int {
        return R.layout.item_location
    }


}