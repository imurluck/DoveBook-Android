package com.example.zzx.dovebook.message.myrequest

import com.example.baselibrary.LoadCallback
import com.example.baselibrary.constant.Constant
import com.example.baselibrary.net.HttpHelper
import com.example.zzx.dovebook.message.model.BookShareRequest

class MyRequestRepository {

    private val TAG_QUERY_MY_REQUEST = "query_my_request"

    fun queryMyRequest(paraMap: Map<String, String>, loadCallback: LoadCallback<List<BookShareRequest>>) {
        HttpHelper.getInstance().post(Constant.URL_QUERY_MY_REQUEST, paraMap, loadCallback, TAG_QUERY_MY_REQUEST)
    }

}
