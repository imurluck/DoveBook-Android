package com.example.zzx.dovebook.message.model

import android.view.View
import android.widget.Button
import android.widget.TextView
import com.example.baselibrary.image.GlideManager
import com.example.library.BaseAdapter
import com.example.library.IEntity
import com.example.zzx.dovebook.R
import com.example.zzx.dovebook.message.MessageActivity

class BookShareRequestMe(val bookShareRequest: BookShareRequest): IEntity<BookShareRequestMe> {

    private lateinit var statusDescription: TextView
    private lateinit var statusAgree: Button

    override fun getLayoutId(): Int {
        return R.layout.item_book_share_request_me
    }

    override fun bindView(baseAdapter: BaseAdapter?, holder: BaseAdapter.ViewHolder, data: BookShareRequestMe?, position: Int) {
        holder.itemView.apply {
            val activity = context as MessageActivity
            val fragment = activity.requestMeFragment

            GlideManager.loadImage(activity, bookShareRequest.bookImagePath, findViewById(R.id.bookImage))
            findViewById<TextView>(R.id.bookTitle).text = bookShareRequest.bookTitle
            findViewById<TextView>(R.id.requestUserName).text = "${bookShareRequest.userName} -> 请求接收"

            statusDescription = findViewById(R.id.statusDescription)
            statusDescription.visibility = getStatusDescriptionVisibility()
            statusDescription.text = getStatusDescription()

            statusAgree = findViewById(R.id.statusAgree)
            statusAgree.visibility = getStatusOperationVisibility()
            statusAgree.setOnClickListener {
                fragment.agreeRequest(this@BookShareRequestMe)
            }
        }
    }

    private fun getStatusDescriptionVisibility(): Int {
        return when (bookShareRequest.status) {
            BookShareRequest.STATUS_REQUESTING -> View.GONE
            else -> View.VISIBLE
        }
    }

    private fun getStatusOperationVisibility(): Int {
        return when (bookShareRequest.status) {
            BookShareRequest.STATUS_REQUESTING -> View.VISIBLE
            else -> View.GONE
        }
    }

    private fun getStatusDescription(): String {
        return when (bookShareRequest.status) {
            BookShareRequest.STATUS_AGREED -> "已同意"
            BookShareRequest.STATUS_REJECTED -> "已拒绝"
            BookShareRequest.STATUS_IGNORED -> "已忽略"
            else -> ""
        }
    }

    fun updateStatus() {
        statusDescription.visibility = getStatusDescriptionVisibility()
        statusDescription.text = getStatusDescription()
        statusAgree.visibility = getStatusOperationVisibility()
    }
}