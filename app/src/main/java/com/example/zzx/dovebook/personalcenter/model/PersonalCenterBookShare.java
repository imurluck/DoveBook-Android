package com.example.zzx.dovebook.personalcenter.model;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;
import com.example.baselibrary.commonutil.FormatUtil;
import com.example.baselibrary.image.GlideManager;
import com.example.library.BaseAdapter;
import com.example.library.IEntity;
import com.example.zzx.dovebook.R;
import com.example.zzx.dovebook.base.model.BookShare;

public class PersonalCenterBookShare implements IEntity<PersonalCenterBookShare> {

    private int type;

    private BookShare bookShare;

    public PersonalCenterBookShare(BookShare bookShare, int type) {
        this.type = type;
        this.bookShare = bookShare;
    }

    public static final int TYPE_UPLOAD = 0;
    public static final int TYPE_PASS = 1;
    public static final int TYPE_LIKE = 2;

    @Override
    public int getLayoutId() {
        return R.layout.item_personal_center_book_share;
    }

    public BookShare get() {
        return bookShare;
    }

    @Override
    public void bindView(BaseAdapter baseAdapter, BaseAdapter.ViewHolder holder, PersonalCenterBookShare data, int position) {
        View rootView = holder.itemView;
        Context context = rootView.getContext();
        ((TextView) rootView.findViewById(R.id.item_share_user_name)).setText(bookShare.getUserName());
        ((TextView) rootView.findViewById(R.id.item_share_type_tip)).setText(getTypeText(type));
        ((TextView) rootView.findViewById(R.id.item_share_datetime)).setText(FormatUtil.dateFormat(bookShare.getCreateAt()));
        ((TextView) rootView.findViewById(R.id.item_share_description)).setText(bookShare.getDescription());
        GlideManager.loadImage(context, bookShare.getBookImagePath(), rootView.findViewById(R.id.item_share_img));
        if (TextUtils.isEmpty(bookShare.getUserAvatarPath())) {
            GlideManager.loadImage(context, R.mipmap.ic_default_avatar, rootView.findViewById(R.id.item_share_user_avatar));
        } else {
            GlideManager.loadImage(context, bookShare.getUserAvatarPath(), rootView.findViewById(R.id.item_share_user_avatar));
        }
    }

    private String getTypeText(int type) {
        switch (type) {
            case TYPE_UPLOAD:
                return " -上传图书:";
            case TYPE_PASS:
                return " -传递图书:";
            case TYPE_LIKE:
                return "";
        }
        return "";
    }
}
