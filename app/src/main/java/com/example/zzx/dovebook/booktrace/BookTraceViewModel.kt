package com.example.zzx.dovebook.booktrace

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.baselibrary.LoadCallback
import com.example.baselibrary.net.HttpUtil
import com.example.zzx.dovebook.base.BaseLiveData
import com.example.zzx.dovebook.base.BaseViewModel
import com.example.zzx.dovebook.base.ValueWrapper
import com.example.zzx.dovebook.base.ValueWrapper.*
import com.example.zzx.dovebook.booktrace.model.BookTrace

class BookTraceViewModel(reponsity: BookTraceRepository): BaseViewModel<BookTraceRepository>(reponsity) {

    val queryBookTraceKeys = arrayOf(
        "bookId",
        "createAt"
    )

    val requestReceiveKeys = arrayOf(
            "bookId",
            "bookShareId",
            "requestUserId",
            "uploadUserId"
    )

    fun queryBookTrace(bookId: String, createAt: Long): LiveData<ValueWrapper<Any>> {
        val result = BaseLiveData<List<BookTrace>>()
        val paraMap = HttpUtil.wrapParams(queryBookTraceKeys, bookId, createAt)
        repository.queryBookTrace(paraMap, object : LoadCallback<List<BookTrace>> {
            override fun loadSuccess(data: List<BookTrace>?) {
                result.postValue(data, TYPE_NORMAL)
            }

            override fun loadFailed(errorMessage: String?) {
                postErrorMessage(errorMessage)
                result.postValue(null, TYPE_FAILED)
            }

            override fun loadNull(message: String?) {
                postErrorMessage(message)
                result.postValue(null, TYPE_NO_CONTENT)
            }

        })
        return result
    }

    fun requestReceive(bookId: String, bookShareId: String, requestUserId: String,
                       uploadUserId: String): LiveData<Boolean> {
        val result = MutableLiveData<Boolean>()
        val paraMap = HttpUtil.wrapParams(requestReceiveKeys, bookId, bookShareId, requestUserId, uploadUserId)
        repository.requestReceive(paraMap, object: LoadCallback<Boolean> {
            override fun loadSuccess(data: Boolean?) {
                result.postValue(data)
            }

            override fun loadFailed(errorMessage: String?) {
                postErrorMessage(errorMessage)
            }

            override fun loadNull(message: String?) {
                postErrorMessage(message)
            }

        })
        return result
    }

}