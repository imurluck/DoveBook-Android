package com.example.zzx.dovebook.base;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import butterknife.ButterKnife;
import com.example.baselibrary.commonutil.Logger;
import com.example.baselibrary.commonutil.ToastUtil;

public abstract class BaseFragment<VM extends BaseViewModel> extends Fragment {

    public String TAG = getClass().getSimpleName();

    private VM mViewModel;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Logger.e(TAG, "onCreate ");
        setupViewModel();
    }

    /**
     * 创建ViewModel
     */
    private void setupViewModel() {
        Class<VM> vmClazz = createViewModel();
        if (vmClazz == null) {
            return ;
        }
        mViewModel = ViewModelProviders.of(this, new ViewModelFactory(createRepository())).get(vmClazz);
    }

    /**
     * 返回ViewModel对应的Reponsity
     * @return
     */
    protected Object createRepository() {
        return null;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        Logger.e(TAG, "onCreateView ");
        View view = inflater.inflate(getLayoutId(), container, false);
        ButterKnife.bind(this, view);

        if (getViewModel() != null) {
            setupErrorMessageToast();
            setupMessageToast();
        }
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Logger.e(TAG, "onActivityCreated ");
        initViews();
        initOptions();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Logger.e(TAG, "onDestroy ");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Logger.e(TAG, "onDestroyView ");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Logger.e(TAG, "onDetach ");
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Logger.e(TAG, "onAttach ");
    }

    @Override
    public void onPause() {
        super.onPause();
        Logger.e(TAG, "onPause ");
    }

    @Override
    public void onStart() {
        super.onStart();
        Logger.e(TAG, "onStart ");
    }

    @Override
    public void onStop() {
        super.onStop();
        Logger.e(TAG, "onStop ");
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Logger.e(TAG, "onViewCreated ");
    }
    

    protected void initOptions() {
    }

    protected void initViews() {
        
    }

    protected void setupMessageToast() {
        getViewModel().getMessage().observe(this, (message) -> {
            ToastUtil.shortToast(getActivity(), (String) message);
        });
    }

    private void setupErrorMessageToast() {
        getViewModel().getErrorMessage().observe(this, (errorMessage) -> {
            ToastUtil.shortToast(getActivity(), (String) errorMessage);
        });
    }

    protected abstract Class<VM> createViewModel();

    protected VM getViewModel() {
        if (mViewModel == null) {
            Logger.e(TAG, "ViewModel is null");
            return null;
        }
        return mViewModel;
    }


    protected abstract int getLayoutId();

    @Override
    public void onResume() {
        super.onResume();
        Logger.e(TAG, "onResume ");
    }
}
