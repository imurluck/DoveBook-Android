package com.example.zzx.dovebook.base

import com.example.baselibrary.LoadCallback
import com.example.baselibrary.constant.Constant
import com.example.baselibrary.net.HttpHelper
import com.example.zzx.dovebook.base.model.BookShare
import com.example.zzx.dovebook.user.model.UserInfo
import okhttp3.MultipartBody

/**
 * 封装一些通用的数据获取方式，例如对BookShare点赞，取消点赞等
 * create by zzx
 * create at 19-4-18
 */
open class CommonRepository {



    fun queryHotBookShare(params: Map<String, String>, loadCallback: LoadCallback<List<BookShare>>) {
        HttpHelper.getInstance().post(Constant.URL_QUERY_HOT_BOOK_SHARE, params,
            loadCallback, TAG_QUERY_HOT_BOOK_SHARE)
    }

    fun bookShareLike(params: Map<String, String>, loadCallback: LoadCallback<Boolean>) {
        HttpHelper.getInstance().post(Constant.URL_BOOK_SHARE_LIKE, params,
            loadCallback, TAG_BOOK_SHARE_LIKE)
    }

    fun cancelBookShareLike(params: Map<String, String>, loadCallback: LoadCallback<Boolean>) {
        HttpHelper.getInstance().post(Constant.URL_CANCEL_BOOK_SHARE_LIKE, params,
            loadCallback, TAG_CANCEL_BOOK_SHARE_LIKE)
    }

    fun follow(paraMap: Map<String, String>, loadCallback: LoadCallback<Int>) {
        HttpHelper.getInstance().post(Constant.URL_USER_FOLLOW, paraMap,
            loadCallback, TAG_USER_FOLLOW)
    }

    fun getUserInfo(params: Map<String, String>, loadCallback: LoadCallback<UserInfo>) {
        HttpHelper.getInstance().post(Constant.URL_GET_USER_INFO, params,
            loadCallback, TAG_GET_USER_INFO)
    }

    fun uploadBookShare(params: Map<String, String>, photoFile: MultipartBody.Part, loadCallback: LoadCallback<Boolean>) {
        HttpHelper.getInstance().post(Constant.URL_UPLOAD_BOOK_SHARE, params,
            loadCallback, TAG_UPLOAD_BOOK_SHARE, photoFile)
    }

    companion object {
        private const val TAG_QUERY_HOT_BOOK_SHARE = "queryHotBookShare"

        private const val TAG_BOOK_SHARE_LIKE = "bookShareLike"

        private const val TAG_CANCEL_BOOK_SHARE_LIKE = "cancelBookShareLike"

        private const val TAG_USER_FOLLOW = "userFollow"

        private const val TAG_GET_USER_INFO = "getUserInfo"

        private const val TAG_UPLOAD_BOOK_SHARE = "uploadBookShare"
    }
}