package com.example.zzx.dovebook.booksearch.searchresult

import android.os.Bundle
import android.text.TextUtils
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView.VERTICAL
import com.example.baselibrary.widget.LoadingStateView
import com.example.library.BaseAdapter
import com.example.zzx.dovebook.R
import com.example.zzx.dovebook.base.BaseFragment
import com.example.zzx.dovebook.base.ValueWrapper.*
import com.example.zzx.dovebook.user.UserManager
import kotlinx.android.synthetic.main.fragment_search_result.*

class SearchResultFragment : BaseFragment<SearchResultViewModel>() {

    private lateinit var emptyView: LoadingStateView
    private lateinit var loadMoreView: LoadingStateView

    private lateinit var adapter: BaseAdapter

    private lateinit var lastKeyword: String

    private var isCreated = false

    override fun createViewModel(): Class<SearchResultViewModel> = SearchResultViewModel::class.java

    override fun getLayoutId(): Int = R.layout.fragment_search_result

    override fun createRepository(): Any {
        return SearchResultRepository()
    }

    override fun initViews() {
        setupRecyclerView()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        lastKeyword = arguments!!.getString(activity!!.getString(R.string.search_keyword_arg_name))
    }

    override fun initOptions() {
        isCreated = true
        searchBookShare(lastKeyword)
    }

    private fun setupRecyclerView() {
        emptyView = LoadingStateView(activity)
        emptyView.setOnRetryListener {
            searchBookShare(lastKeyword)
        }
        loadMoreView = LoadingStateView(activity)
        loadMoreView.setOnRetryListener {
            searchBookShare(lastKeyword)
        }
        adapter = BaseAdapter.Builder()
            .emptyView(emptyView)
            .addRooter(loadMoreView)
            .autoLoadMore()
            .build()
        adapter.setOnLoadMoreListener {
            if (loadMoreView.isNoMore) return@setOnLoadMoreListener
            searchBookShare(lastKeyword)
        }
        adapter.emptyState()
        recyclerView.layoutManager = LinearLayoutManager(activity, VERTICAL, false)
        recyclerView.adapter = adapter
    }

    private fun resetRecyclerView() {
        adapter.emptyState()
        emptyView.loading()
        loadMoreView.loading()
        adapter.clearData()
    }

    fun searchBookShare(keyword: String) {
        if (!TextUtils.equals(lastKeyword, keyword)) {
            resetRecyclerView()
        }
        lastKeyword = keyword
        viewModel.searchBookShare(this, UserManager.getInstance(activity).loginUserId ?: "",
            keyword, adapter.dataCount).observe(this, Observer {
            when (it.type) {
                TYPE_FAILED -> {
                    if (adapter.isInEmptyState) {
                        emptyView.failed()
                    } else {
                        loadMoreView.failed()
                    }
                }
                TYPE_NO_CONTENT -> {
                    if (adapter.isInEmptyState) {
                        emptyView.empty()
                    } else {
                        loadMoreView.noMore()
                    }
                }
                TYPE_NORMAL -> {
                    if (adapter.isInEmptyState) {
                        adapter.cancelEmptyState()
                    }
                    adapter.add(it.value as List<BookShareSearch>)
                }
            }
        })
    }

    /**
     * 第一次创建的时候不会调用此方法
     */
    override fun onHiddenChanged(hidden: Boolean) {
        if (!hidden) {
            val keyword = arguments!!.getString(activity!!.getString(R.string.search_keyword_arg_name))
            searchBookShare(keyword)
        }
    }

    fun isCreated(): Boolean = isCreated
}