package com.example.zzx.dovebook.bookshare.booksharepass

import com.example.baselibrary.LoadCallback
import com.example.baselibrary.constant.Constant
import com.example.baselibrary.net.HttpHelper
import okhttp3.MultipartBody

class BookSharePassRepository {

    private val TAG_PASS_BOOK_SHARE = "passBookShare"

    fun passBookShare(paraMap: Map<String, String>, coverFile: MultipartBody.Part, loadCallback: LoadCallback<Boolean>) {
        HttpHelper.getInstance().post(Constant.URL_PASS_BOOK_SHARE, paraMap,
                loadCallback, TAG_PASS_BOOK_SHARE, coverFile)
    }

}