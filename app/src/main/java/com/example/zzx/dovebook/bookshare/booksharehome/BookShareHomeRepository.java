package com.example.zzx.dovebook.bookshare.booksharehome;

import com.example.baselibrary.LoadCallback;
import com.example.baselibrary.constant.Constant;
import com.example.baselibrary.net.HandleCallbackImpl;
import com.example.baselibrary.net.HttpHelper;
import com.example.baselibrary.net.HttpManager;
import com.example.baselibrary.net.HttpUtil;
import com.example.zzx.dovebook.base.model.BookShare;
import com.example.zzx.dovebook.bookshare.booksharehome.model.Comment;
import com.example.zzx.dovebook.net.Api;

import java.util.List;
import java.util.Map;

public class BookShareHomeRepository {


    private static final String TAG_QUERY_HOT_BOOK_SHARE = "queryHotBookShare";
    private static final String TAG_QUERY_COMMENTS = "queryComment";
    private static final String TAG_BOOK_SHARE_LIKE = "bookShareLike";
    private static final String TAG_CANCEL_BOOK_SHARE_LIKE = "cancelBookShareLike";
    private static final String TAG_FOLLOW = "follow";

    public void queryHotBookShare(String userId, long createAt, LoadCallback<List<BookShare>> loadCallback) {
        Api api = HttpManager.getInstance().getApiService(Api.class, Constant.BASE_URL, TAG_QUERY_HOT_BOOK_SHARE);
        if (api == null) {
            return ;
        }
        api.queryHotBookShare(userId, createAt)
                .enqueue(new HandleCallbackImpl<>(loadCallback, TAG_QUERY_HOT_BOOK_SHARE));
    }

    public void uploadBookShare(Map<String, String> paramMap, String coverFilePath,
                                LoadCallback<Boolean> loadCallback) {
        Api api = HttpManager.getInstance().getApiService(Api.class, Constant.BASE_URL);
        if (api == null) {
            return ;
        }
        api.uploadBookShare(HttpUtil.createMapBody(paramMap),
                        HttpUtil.createFilePart("coverFile", coverFilePath))
                .enqueue(new HandleCallbackImpl<>(loadCallback));
    }

    public void addComment(Map<String, String> paraMap, LoadCallback<Comment> loadCallback) {
        HttpManager.getInstance().getApiService(Api.class, Constant.BASE_URL)
                .addComment(HttpUtil.createMapBody(paraMap))
                .enqueue(new HandleCallbackImpl<>(loadCallback));
    }

    public void replyComment(Map<String, String> paraMap, LoadCallback<Comment> loadCallback) {
        HttpManager.getInstance().getApiService(Api.class, Constant.BASE_URL)
                .replyComment(HttpUtil.createMapBody(paraMap))
                .enqueue(new HandleCallbackImpl<>(loadCallback));
    }

    public void queryComments(Map<String, String> paraMap, LoadCallback<List<Comment>> loadCallback) {
        Api api = HttpManager.getInstance().getApiService(Api.class, Constant.BASE_URL, TAG_QUERY_COMMENTS);
        if (api == null) {
            return ;
        }
        api.queryComments(HttpUtil.createMapBody(paraMap))
                .enqueue(new HandleCallbackImpl<>(loadCallback, TAG_QUERY_COMMENTS));
    }

    public void bookShareLike(String bookShareId, String userId, LoadCallback<Boolean> loadCallback) {
        Api api = HttpManager.getInstance().getApiService(Api.class, Constant.BASE_URL, TAG_BOOK_SHARE_LIKE);
        if (api == null) {
            return ;
        }
        api.bookShareLike(bookShareId, userId)
                .enqueue(new HandleCallbackImpl<>(loadCallback, TAG_BOOK_SHARE_LIKE));
    }

    public void cancelBookShareLike(String bookShareId, String userId, LoadCallback<Boolean> loadCallback) {
        Api api = HttpManager.getInstance().getApiService(Api.class, Constant.BASE_URL, TAG_CANCEL_BOOK_SHARE_LIKE);
        if (api == null) {
            return ;
        }
        api.cancelBookShareLike(bookShareId, userId)
                .enqueue(new HandleCallbackImpl<>(loadCallback, TAG_CANCEL_BOOK_SHARE_LIKE));
    }

    public void follow(Map paraMap, LoadCallback<Integer> loadCallback) {
        HttpHelper.getInstance().post(Constant.URL_USER_FOLLOW, paraMap, loadCallback, TAG_FOLLOW);
    }
}
