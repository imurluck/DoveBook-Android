package com.example.zzx.dovebook.personalcenter.receive

import android.content.Intent
import android.text.TextUtils
import android.view.View
import android.widget.TextView
import com.example.baselibrary.commonutil.FormatUtil
import com.example.baselibrary.image.GlideManager
import com.example.library.BaseAdapter
import com.example.library.IEntity
import com.example.zzx.dovebook.R
import com.example.zzx.dovebook.base.model.BookShare
import com.example.zzx.dovebook.bookshare.booksharepass.BookSharePassActivity

class BookShareReceive(private val bookShare: BookShare) : IEntity<BookShareReceive> {


    override fun bindView(baseAdapter: BaseAdapter?, holder: BaseAdapter.ViewHolder, data: BookShareReceive?, position: Int) {
        holder.itemView.apply {
            findViewById<TextView>(R.id.item_share_user_name).text = bookShare.userName
            findViewById<TextView>(R.id.item_share_datetime).text = FormatUtil.dateFormat(bookShare.createAt)
            findViewById<TextView>(R.id.item_share_type_tip).text = "-接收"
            findViewById<TextView>(R.id.item_share_description).text = bookShare.description
            GlideManager.loadImage(context, bookShare.bookImagePath, findViewById(R.id.item_share_img))
            if (TextUtils.isEmpty(bookShare.userAvatarPath)) {
                GlideManager.loadImage(context, R.mipmap.ic_default_avatar, findViewById(R.id.item_share_user_avatar))
            } else {
                GlideManager.loadImage(context, bookShare.userAvatarPath, findViewById(R.id.item_share_user_avatar))
            }
            if (TextUtils.equals(bookShare.holderUserId, bookShare.receiveUserId)) {
                val passOperation = findViewById<TextView>(R.id.passOperation)
                passOperation.visibility = View.VISIBLE
                passOperation.setOnClickListener {
                    context.startActivity(Intent(context, BookSharePassActivity::class.java).apply {
                        putExtra(context.getString(R.string.book_share_arg_name), bookShare)
                    })
                }
            }
        }
    }

    override fun getLayoutId(): Int {
        return R.layout.item_personal_center_book_share
    }


}