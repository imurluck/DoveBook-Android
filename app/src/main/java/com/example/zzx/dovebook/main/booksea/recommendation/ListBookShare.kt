package com.example.zzx.dovebook.main.booksea.recommendation

import android.text.TextUtils
import android.util.Log
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.example.baselibrary.commonutil.FormatUtil
import com.example.baselibrary.image.GlideManager
import com.example.library.BaseAdapter
import com.example.library.IEntity
import com.example.zzx.dovebook.R
import com.example.zzx.dovebook.base.UserContext
import com.example.zzx.dovebook.base.extensions.startBookShadeDetailActivity
import com.example.zzx.dovebook.base.model.BookShare
import com.example.zzx.dovebook.widget.TagImageView
import com.example.zzx.heartanimation.LikeLayout
import de.hdodenhof.circleimageview.CircleImageView

/**
 * 此类为实体类，表示视图为BookShare在列表中的单个item
 * create by zzx
 * create at 19-4-17
 */
class ListBookShare(val bookShare: BookShare,
                    private val userContext: UserContext): IEntity<ListBookShare> {

    override fun bindView(
        baseAdapter: BaseAdapter,
        holder: BaseAdapter.ViewHolder,
        data: ListBookShare,
        position: Int
    ) {
        holder.itemView.apply {
            setOnClickListener {
                context.startBookShadeDetailActivity(bookShare)
            }
            findViewById<TagImageView>(R.id.bookImage).apply {
                Log.e("ListBookShare", "bindView -> description(${bookShare.description})  url = ${bookShare.bookImagePath}")
                GlideManager.loadImage(context, bookShare.bookImagePath, this)
                tagText = bookShare.bookStatusTip
                tagBackgroundColor = if (bookShare.bookStatus == BookShare.STATUS_DRIFTING) {
                    ContextCompat.getColor(context, R.color.colorAccent)
                } else {
                    ContextCompat.getColor(context, R.color.color_red_dark)
                }
            }
            findViewById<TextView>(R.id.descriptionTv).text = bookShare.description
            findViewById<CircleImageView>(R.id.userAvatarImage).apply {
                if (TextUtils.isEmpty(bookShare.userAvatarPath)) {
                    GlideManager.loadImage(context, R.drawable.default_avatar, this)
                } else {
                    GlideManager.loadImage(context, bookShare.userAvatarPath, this)
                }
            }
            findViewById<TextView>(R.id.userNameTv).text = bookShare.userName
            findViewById<LikeLayout>(R.id.likeLayout).apply {
                tag = bookShare.description
                initState(if (bookShare.isHasLike) STATE_FINAL else STATE_INITIAL)
                setText(FormatUtil.countFormat(bookShare.likeCount))
                setOnClickListener {
                    if (bookShare.isHasLike) {
                        userContext.cancelBookShareLike(this@ListBookShare, this, bookShare.bookShareId)
                    } else {
                        userContext.bookShareLike(this@ListBookShare, this, bookShare.bookShareId)
                    }
                }
            }
        }
    }

    fun updateLike(targetView: LikeLayout) {
        bookShare.isHasLike = true
        bookShare.likeCount++
        targetView.toggle(FormatUtil.countFormat(bookShare.likeCount))
    }

    fun updateCancelLike(targetView: LikeLayout) {
        bookShare.isHasLike = false
        bookShare.likeCount--
        targetView.toggle(FormatUtil.countFormat(bookShare.likeCount))
    }

    override fun getLayoutId(): Int = R.layout.item_book_share

}