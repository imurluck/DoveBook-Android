package com.example.zzx.dovebook.login;

import android.app.Activity;
import android.content.Intent;
import androidx.annotation.Nullable;
import com.example.baselibrary.commonutil.Logger;
import com.example.zzx.dovebook.R;
import com.example.zzx.dovebook.login.ui.LoginActivity;
import com.example.zzx.dovebook.user.UserManager;
import com.qmuiteam.qmui.widget.dialog.QMUIDialog;

import java.lang.ref.SoftReference;
import java.util.HashMap;

public class LoginManager {

    private static final String TAG = "LoginManager";

    public static final int REQUEST_CODE = 999;

    private static volatile LoginManager sInstance;

    private SoftReference<Activity> mActivity;

    private HashMap<String, LoginCallback> mCallbackMap;

    private LoginManager() {
        mCallbackMap = new HashMap<>();
    }

    public static LoginManager getInstance() {
        if (sInstance == null) {
            synchronized (LoginManager.class) {
                if (sInstance == null) {
                    sInstance = new LoginManager();
                }
            }
        }
        return sInstance;
    }

    public void init(@Nullable Activity activity) {
        if (mActivity != null && mActivity.get() == activity) {
            return;
        } else if (mActivity != null) {
            mActivity.clear();
        }
        mActivity = new SoftReference<>(activity);
    }


    public void handleCheckAction(String methodName, LoginCallback callback) {
        if (mActivity.get() == null) {
            Logger.e(TAG, "activity is null");
            return ;
        }
        if (UserManager.getInstance(mActivity.get()).getLoginUser() != null) {
            callback.hasLogin();
            return ;
        }
        String callId = methodName + System.currentTimeMillis();
        mCallbackMap.put(callId, callback);
        showDialog(callId);
    }

    private void showDialog(String callId) {
        new QMUIDialog.MessageDialogBuilder(mActivity.get())
                .setMessage("去登陆?")
                .addAction("取消", (dialog, index) -> dialog.dismiss())
                .addAction("确定", (dialog, index) -> {
                    dialog.dismiss();
                    Intent intent = new Intent(mActivity.get(), LoginActivity.class);
                    intent.putExtra(mActivity.get().getString(R.string.call_id_arg_name), callId);
                    mActivity.get().startActivityForResult(intent, REQUEST_CODE);

                }).create().show();
    }

    public void releaseContext(Activity activity) {
        if (mActivity != null && mActivity.get() == activity) {
            mActivity.clear();
        }
    }

    public void handleResult(Intent data) {
        String callId = data.getStringExtra(mActivity.get().getString(R.string.call_id_arg_name));
        LoginCallback callback = mCallbackMap.get(callId);
        if (callback == null) {
            return ;
        }
        callback.loginSuccess();
        mCallbackMap.remove(callId);
    }
}
