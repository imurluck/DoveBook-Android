package com.example.zzx.dovebook.message.myrequest

import androidx.lifecycle.LiveData
import com.example.baselibrary.LoadCallback
import com.example.baselibrary.net.HttpUtil
import com.example.zzx.dovebook.base.BaseLiveData
import com.example.zzx.dovebook.base.BaseViewModel
import com.example.zzx.dovebook.base.ValueWrapper
import com.example.zzx.dovebook.base.ValueWrapper.*
import com.example.zzx.dovebook.message.model.BookShareMyRequest
import com.example.zzx.dovebook.message.model.BookShareRequest

class MyRequestViewModel(reponsity: MyRequestRepository): BaseViewModel<MyRequestRepository>(reponsity) {

    private val queryMyRequestKeys = arrayOf("requestUserId", "createAt")

    fun queryMyRequest(requestUserId: String, lastItemCreateAt: Long): LiveData<ValueWrapper<Any>> {
        var result = BaseLiveData<List<BookShareMyRequest>>()
        var paraMap = HttpUtil.wrapParams(queryMyRequestKeys, requestUserId, lastItemCreateAt)
        repository.queryMyRequest(paraMap, object: LoadCallback<List<BookShareRequest>> {
            override fun loadSuccess(data: List<BookShareRequest>) {
                var dataList = mutableListOf<BookShareMyRequest>()
                for (bookShareRequest in data) {
                    dataList.add(BookShareMyRequest(bookShareRequest))
                }
                result.postValue(dataList, TYPE_NORMAL)
            }

            override fun loadFailed(errorMessage: String?) {
                postErrorMessage(errorMessage)
                result.postValue(null, TYPE_FAILED)
            }

            override fun loadNull(message: String?) {
                result.postValue(null, TYPE_NO_CONTENT)
            }

        })
        return result
    }

}