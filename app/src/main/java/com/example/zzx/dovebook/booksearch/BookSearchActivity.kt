package com.example.zzx.dovebook.booksearch

import android.os.Bundle
import android.text.TextUtils
import android.util.TypedValue
import android.view.MenuItem
import android.view.View.TEXT_ALIGNMENT_GRAVITY
import android.widget.TextView
import androidx.appcompat.widget.SearchView
import androidx.core.view.ViewCompat
import androidx.fragment.app.Fragment
import com.example.zzx.dovebook.R
import com.example.zzx.dovebook.base.BaseActivity
import com.example.zzx.dovebook.base.BaseViewModel
import com.example.zzx.dovebook.booksearch.searchhistory.SearchHistoryFragment
import com.example.zzx.dovebook.booksearch.searchresult.SearchResultFragment
import kotlinx.android.synthetic.main.activity_book_search.*

class BookSearchActivity: BaseActivity<BaseViewModel<Any>>() {


    private val searchHistoryFragment = SearchHistoryFragment()
    private val searchResultFragment = SearchResultFragment()

    private lateinit var currentFragment: Fragment

    override fun getLayoutId(): Int = R.layout.activity_book_search

    override fun createViewModel(): Class<BaseViewModel<Any>>? = null

    private lateinit var searchTextView: TextView

    override fun initViews(savedInstanceState: Bundle?) {
        setupToolbar()

        ViewCompat.setTransitionName(toolbar, getString(R.string.search_transition_name))
    }

    override fun initOptions(savedInstanceState: Bundle?) {
        replaceFragment(searchHistoryFragment, R.id.container)
        currentFragment = searchHistoryFragment
    }

    private fun setupToolbar() {
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        searchView.apply {
            queryHint = getString(R.string.hint_search)
            searchTextView = findViewById<TextView>(R.id.search_src_text)
            searchTextView.textAlignment = TEXT_ALIGNMENT_GRAVITY
            searchTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, resources.getDimension(R.dimen.titleHintSize))
            isIconified = false
            //关闭
            onActionViewExpanded()
            isSubmitButtonEnabled = true

            setOnQueryTextListener(object : SearchView.OnQueryTextListener {
                override fun onQueryTextSubmit(query: String?): Boolean {
                    return if (TextUtils.isEmpty(query)) {
                        false
                    } else {
                        searchHistoryFragment.addHistory(query!!)
                        searchBookShare(query)
                        true
                    }
                }

                override fun onQueryTextChange(newText: String?): Boolean {
                    if (TextUtils.isEmpty(query)) {
                        if (currentFragment !== searchHistoryFragment) {
                            replaceFragment(searchHistoryFragment, R.id.container)
                            currentFragment = searchHistoryFragment
                        }
                    }
                    return true
                }

            })
        }
    }

    fun searchBookShare(keyword: String) {
        searchTextView.text = keyword
        if (currentFragment !== searchResultFragment) {
            searchResultFragment.arguments = Bundle().apply {
                putString(getString(R.string.search_keyword_arg_name), keyword)
            }
            replaceFragment(searchResultFragment, R.id.container)
            currentFragment = searchResultFragment
        } else if (searchResultFragment.isCreated()) {
            searchResultFragment.searchBookShare(keyword)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> finish()
        }
        return super.onOptionsItemSelected(item)
    }
}