package com.example.zzx.dovebook.setting

import android.os.Bundle
import android.view.MenuItem
import com.example.baselibrary.commonutil.ToastUtil
import com.example.baselibrary.image.GlideManager
import com.example.zzx.dovebook.R
import com.example.zzx.dovebook.base.BaseActivity
import com.example.zzx.dovebook.base.BaseViewModel
import com.example.zzx.dovebook.utils.getVersionInfo
import com.qmuiteam.qmui.widget.grouplist.QMUICommonListItemView
import com.qmuiteam.qmui.widget.grouplist.QMUIGroupListView
import kotlinx.android.synthetic.main.activity_setting.*

class SettingActivity : BaseActivity<BaseViewModel<*>>() {

    override fun getLayoutId(): Int = R.layout.activity_setting

    override fun createViewModel(): Class<BaseViewModel<*>> = BaseViewModel::class.java

    override fun getStatusBarColor(): Int = STATUS_BAR_LIGHT

    override fun initViews(savedInstanceState: Bundle?) {

        setupToolbar()

        setupItems()
    }

    private fun setupItems() {

        val sectionBasic = QMUIGroupListView.newSection(this).setTitle(getString(R.string.setting_base))
            .addItemView(SettingItemProvider.getUserInfoItem(groupListView)) {
                ToastUtil.shortToast(this@SettingActivity, "后续开发中")
            }.addItemView(SettingItemProvider.getCacheInfoItem(groupListView).apply {
                detailText = GlideManager.getDiskCacheSize(this@SettingActivity)
            }) {
                clearCacheInfo((it as QMUICommonListItemView))
            }.addTo(groupListView)

        val sectionExplain = QMUIGroupListView.newSection(this).setTitle(getString(R.string.setting_explain))
            .addItemView(SettingItemProvider.getVersionInfoItem(groupListView).apply {
                detailText = getVersionInfo()
            }) {
                ToastUtil.shortToast(this@SettingActivity, (it as QMUICommonListItemView).detailText as String)
            }.addTo(groupListView)
    }

    private fun setupToolbar() {
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    private fun clearCacheInfo(itemView: QMUICommonListItemView) {
        GlideManager.clearDiskCache(this) {
            mainHandler.post {
                itemView.detailText = it
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> finish()
        }


        return super.onOptionsItemSelected(item)
    }

}