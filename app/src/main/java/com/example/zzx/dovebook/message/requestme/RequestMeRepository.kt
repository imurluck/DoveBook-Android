package com.example.zzx.dovebook.message.requestme

import com.example.baselibrary.LoadCallback
import com.example.baselibrary.constant.Constant
import com.example.baselibrary.net.HttpHelper
import com.example.zzx.dovebook.message.model.BookShareRequest

class RequestMeRepository {

    private val TAG_QUERY_REQUEST_ME = "queryRequestMe"
    private val TAG_AGREE_REQUEST = "agreeQuest"

    fun queryRequestMe(paraMap: Map<String, String>, loadCallback: LoadCallback<List<BookShareRequest>>) {
        HttpHelper.getInstance().post(Constant.URL_QUERY_REQUEST_ME, paraMap, loadCallback, TAG_QUERY_REQUEST_ME)
    }

    fun agreeRequest(paraMap: Map<String, String>, loadCallback: LoadCallback<Boolean>) {
        HttpHelper.getInstance().post(Constant.URL_AGREE_REQUEST, paraMap, loadCallback, TAG_AGREE_REQUEST)
    }

}
