package com.example.zzx.dovebook.message

import android.os.Bundle
import android.view.MenuItem
import androidx.fragment.app.Fragment
import com.example.dovebookui.widget.IndicatorAdapter
import com.example.zzx.dovebook.R
import com.example.zzx.dovebook.base.BaseActivity
import com.example.zzx.dovebook.message.myrequest.MyRequestFragment
import com.example.zzx.dovebook.message.requestme.RequestMeFragment
import com.example.zzx.dovebook.personalcenter.FragmentAdapter
import kotlinx.android.synthetic.main.activity_message.*
import net.lucode.hackware.magicindicator.ViewPagerHelper
import net.lucode.hackware.magicindicator.buildins.commonnavigator.CommonNavigator

class MessageActivity : BaseActivity<MessageViewModel>() {

    private lateinit var fragmentAdapter: FragmentAdapter

    lateinit var myRequestFragment: MyRequestFragment
    lateinit var requestMeFragment: RequestMeFragment

    override fun getLayoutId(): Int {
        return R.layout.activity_message
    }

    override fun createViewModel(): Class<MessageViewModel> {
        return MessageViewModel::class.java
    }

    override fun getStatusBarColor(): Int {
        return STATUS_BAR_LIGHT
    }

    override fun createRepository(): Any {
        return MessageRepository()
    }

    override fun initViews(savedInstanceState: Bundle?) {
        setupToolbar()

        setupFragment(savedInstanceState)

        setupViewPager()
    }

    private fun setupFragment(savedInstanceState: Bundle?) {
        if (savedInstanceState != null) {
            return
        }
        myRequestFragment = MyRequestFragment()
        requestMeFragment = RequestMeFragment()
    }

    private fun setupViewPager() {
        val titleList = resources.getStringArray(R.array.message_type)
        val fragmentList = mutableListOf<Fragment>()
        fragmentList.add(myRequestFragment)
        fragmentList.add(requestMeFragment)
        fragmentAdapter = FragmentAdapter(supportFragmentManager, fragmentList, titleList)
        indicator.navigator = CommonNavigator(this).apply {
            adapter = IndicatorAdapter().apply {
                this.titleList = titleList.asList()
                onItemClickListener = { index ->
                    viewPager.currentItem = index
                }
            }
            isAdjustMode = true

        }
        ViewPagerHelper.bind(indicator, viewPager)
        viewPager.adapter = fragmentAdapter

    }

    private fun setupToolbar() {
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> finish()
        }
        return super.onOptionsItemSelected(item)
    }


}