package com.example.zzx.dovebook.main.me

import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import com.example.baselibrary.commonutil.FormatUtil
import com.example.baselibrary.commonutil.ToastUtil
import com.example.baselibrary.image.GlideManager
import com.example.zzx.dovebook.R
import com.example.zzx.dovebook.base.BaseFragment
import com.example.zzx.dovebook.base.CommonRepository
import com.example.zzx.dovebook.base.CommonViewModel
import com.example.zzx.dovebook.base.ValueWrapper.TYPE_NORMAL
import com.example.zzx.dovebook.base.extensions.*
import com.example.zzx.dovebook.login.LoginCheck
import com.example.zzx.dovebook.user.UserManager
import com.example.zzx.dovebook.user.model.UserInfo
import com.qmuiteam.qmui.widget.dialog.QMUIDialog
import com.qmuiteam.qmui.widget.dialog.QMUIDialogAction.ACTION_PROP_NEGATIVE
import kotlinx.android.synthetic.main.fragment_me.*

class MeFragment: BaseFragment<CommonViewModel<*>>() {

    override fun getLayoutId(): Int = R.layout.fragment_me

    override fun createViewModel(): Class<CommonViewModel<*>>? = CommonViewModel::class.java

    override fun createRepository(): Any? = CommonRepository()

    private var userInfo: UserInfo? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_me, container, false)
    }

    override fun initViews() {
        settingTv.setOnClickListener {
            activity?.startSettingActivity()
        }
        aboutTv.setOnClickListener {
            ToastUtil.shortToast(activity, "后续添加中")
        }
        logoutTv.setOnClickListener {
            QMUIDialog.MessageDialogBuilder(activity)
                .setMessage("真的要退出登录吗?")
                .addAction("取消") { dialog, _ ->
                    dialog.dismiss()
                }.addAction(0, "确定", ACTION_PROP_NEGATIVE) { dialog, _ ->
                    dialog.dismiss()
                    logout()
                }.create().show()
        }
    }

    override fun initOptions() {
        getUserInfo()
    }

    /**
     * 获取用户的信息，需要检查是否已经登录
     */
    @LoginCheck
    private fun getUserInfo() {
        viewModel.getUserInfo(UserManager.getInstance(activity).loginUserId)
            .observe(this, Observer {
                when (it.type) {
                    TYPE_NORMAL -> {
                        userInfo = it.value as UserInfo
                        bindUserInfo()
                    }
                    else -> ToastUtil.shortToast(activity, " 获取用户信息失败")
                }
            })
    }

    private fun bindUserInfo() {
        userInfo?.apply {
            userNameTv.text = userName
            userIdTv.text = "ID $userId"
            followingCountTv.text = "关注\n${FormatUtil.countFormat(followingCount)}"
            followerCountTv.text = "粉丝\n${FormatUtil.countFormat(followerCount)}"
            if (TextUtils.isEmpty(userAvatarPath)) {
                GlideManager.loadImage(activity, R.drawable.default_avatar, userAvatarImage)
                GlideManager.loadImageBlur(activity, R.drawable.default_avatar, bgImage)
            } else {
                GlideManager.loadImage(activity, userAvatarPath, userAvatarImage)
                GlideManager.loadImageBlur(activity, userAvatarPath, bgImage)
            }
            userAvatarImage.setOnClickListener {
                activity?.startUserDetailActivity(userId, userName)
            }
            followerCountTv.setOnClickListener {
                activity?.startFollowerActivity(userId, userName)
            }
            followingCountTv.setOnClickListener {
                activity?.startFollowingActivity(userId, userName)
            }
            historyRecord.setOnClickListener {
                ToastUtil.shortToast(activity, "待开发")
            }
        }
    }

    override fun onHiddenChanged(hidden: Boolean) {
        if (!hidden) {
            checkUserInfo()
        }
    }

    private fun checkUserInfo() {
        if (userInfo == null) {
            getUserInfo()
        }
    }

    private fun logout() {
        activity?.finish()
        UserManager.getInstance(activity).logout()
        activity?.startLoginActivity(null)
    }
}