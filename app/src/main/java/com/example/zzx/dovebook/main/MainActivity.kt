package com.example.zzx.dovebook.main

import android.os.Bundle
import com.example.zzx.dovebook.R
import com.example.zzx.dovebook.base.BaseActivity
import com.example.zzx.dovebook.main.booksea.BookSeaFragment
import com.example.zzx.dovebook.main.mainmessage.MainMessageFragment
import com.example.zzx.dovebook.main.me.MeFragment
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : BaseActivity<MainViewModel>() {

    private val bookSeaFragment = BookSeaFragment()
    private val meFragment = MeFragment()
    private val mainMessageFragment = MainMessageFragment()

    override fun getLayoutId(): Int = R.layout.activity_main

    override fun createViewModel(): Class<MainViewModel>? = null

    override fun getStatusBarColor(): Int = STATUS_BAR_LIGHT

    override fun initViews(savedInstanceState: Bundle?) {
        bottomNavigationView.setOnNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.navigationBookSea -> {
                    replaceFragment(bookSeaFragment, R.id.container)
                    true
                }
                R.id.navigationMe -> {
                    replaceFragment(meFragment, R.id.container)
                    true
                }
                R.id.navigationMessage -> {
                    replaceFragment(mainMessageFragment, R.id.container)
                    true
                }
                else -> false
            }
        }
        replaceFragment(bookSeaFragment, R.id.container)
    }
}
