package com.example.zzx.dovebook.contentprovider;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.example.zzx.dovebook.database.AppDatabaseHelper;

public class AppContentProvider extends ContentProvider {

    private Context mContext;

    @Override
    public boolean onCreate() {
        mContext = getContext();
        return true;
    }

    private static String getTableName(Uri uri) {
        return uri.getLastPathSegment();
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs, @Nullable String sortOrder) {
        return AppDatabaseHelper.getInstance(mContext).query(getTableName(uri), uri, projection, selection, selectionArgs, sortOrder);
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        return null;
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues values) {
        return AppDatabaseHelper.getInstance(mContext).insert(getTableName(uri), uri, values);
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {
        return AppDatabaseHelper.getInstance(mContext).delete(getTableName(uri), uri, selection, selectionArgs);
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues values, @Nullable String selection, @Nullable String[] selectionArgs) {
        return AppDatabaseHelper.getInstance(mContext).update(getTableName(uri), uri, values, selection, selectionArgs);
    }
}
