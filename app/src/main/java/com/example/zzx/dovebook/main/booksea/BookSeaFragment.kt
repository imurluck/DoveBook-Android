package com.example.zzx.dovebook.main.booksea

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.Toolbar
import androidx.core.app.ActivityOptionsCompat
import com.example.dovebookui.widget.IndicatorAdapter
import com.example.zzx.dovebook.R
import com.example.zzx.dovebook.base.BaseFragment
import com.example.zzx.dovebook.base.extensions.startScanActivity
import com.example.zzx.dovebook.booksearch.BookSearchActivity
import com.example.zzx.dovebook.main.booksea.recommendation.PopularFragment
import kotlinx.android.synthetic.main.book_sea_toolbar.*
import kotlinx.android.synthetic.main.fragment_book_sea.*
import net.lucode.hackware.magicindicator.ViewPagerHelper
import net.lucode.hackware.magicindicator.buildins.commonnavigator.CommonNavigator

class BookSeaFragment : BaseFragment<BookSeaViewModel>(), Toolbar.OnMenuItemClickListener {

    override fun createViewModel(): Class<BookSeaViewModel>? = null

    override fun getLayoutId(): Int {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    private lateinit var commonNavigator: CommonNavigator

    private lateinit var fragmentAdapter: BookSeaFragmentAdapter

    override fun onAttach(context: Context) {
        super.onAttach(context)
        commonNavigator = CommonNavigator(context)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.fragment_book_sea, container, false)


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        inflateMenu()
        searchToolbar.setOnMenuItemClickListener(this)
        searchToolbar.setOnClickListener {
            navigationToBookSearchActivity()
        }

        setupViewPager()
    }

    private fun setupViewPager() {
        commonNavigator.adapter = IndicatorAdapter().apply {
            titleList = listOf("热门")
            onItemClickListener = { index ->
                viewPager.currentItem = index
            }
        }
        indicator.navigator = commonNavigator
        val fragmentList = listOf<BaseFragment<*>>(
            PopularFragment()
        )
        fragmentAdapter = BookSeaFragmentAdapter(fragmentList, fragmentManager!!)
        ViewPagerHelper.bind(indicator, viewPager)
        viewPager.adapter = fragmentAdapter
    }


    private fun inflateMenu() {
        searchToolbar.inflateMenu(R.menu.book_sea_toolbar_menu)
        searchToolbar.menu.findItem(R.id.actionPublish).apply {
            actionView.setOnClickListener {
                onMenuItemClick(this)
            }
        }
    }

    private fun navigationToBookSearchActivity() {
        val intent = Intent(activity, BookSearchActivity::class.java)
        ActivityOptionsCompat.makeSceneTransitionAnimation(
            activity!!, searchToolbar,
            getString(R.string.search_transition_name)
        ).apply {
            startActivity(intent, toBundle())
        }
    }

    override fun onMenuItemClick(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.actionPublish -> activity?.startScanActivity()
        }
        return true
    }
}