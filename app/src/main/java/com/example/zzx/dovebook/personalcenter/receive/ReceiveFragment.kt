package com.example.zzx.dovebook.personalcenter.receive

import android.widget.TextView
import androidx.lifecycle.Observer
import com.example.baselibrary.widget.LoadingStateView
import com.example.library.BaseAdapter
import com.example.zzx.dovebook.R
import com.example.zzx.dovebook.base.BaseFragment
import com.example.zzx.dovebook.base.ValueWrapper.TYPE_FAILED
import com.example.zzx.dovebook.base.ValueWrapper.TYPE_NO_CONTENT
import com.example.zzx.dovebook.base.model.BookShare
import com.example.zzx.dovebook.personalcenter.model.PersonalCenterBookShare
import com.example.zzx.dovebook.user.UserManager
import kotlinx.android.synthetic.main.fragment_personal_center_receive.*

class ReceiveFragment: BaseFragment<ReceiveViewModel>() {

    private lateinit var loadMoreView: LoadingStateView
    private lateinit var emptyView: LoadingStateView


    private lateinit var adapter: BaseAdapter

    private var lastItemCreateAt: Long = System.currentTimeMillis()

    override fun createViewModel(): Class<ReceiveViewModel> {
        return ReceiveViewModel::class.java
    }

    override fun getLayoutId(): Int {
        return R.layout.fragment_personal_center_receive
    }

    override fun createRepository(): Any {
        return ReceiveRepository()
    }

    override fun initViews() {
        setupRecycleView()
    }

    override fun initOptions() {
        queryReceiveBookShare()
    }

    private fun setupRecycleView() {
        emptyView = LoadingStateView(activity)
        emptyView.setOnRetryListener { stateView ->
            stateView.loading()
            queryReceiveBookShare()
        }
        loadMoreView = LoadingStateView(activity)
        adapter = BaseAdapter.Builder()
                .autoLoadMore()
                .addRooter(loadMoreView)
                .emptyView(emptyView)
                .build()
        adapter.emptyState()
        adapter.setOnLoadMoreListener {
            recyclerView.post {
                if (loadMoreView.isNoMore) {
                    return@post
                }
                queryReceiveBookShare()
            }
        }
        recyclerView.adapter = adapter
    }

    private fun queryReceiveBookShare() {
        if (adapter.lastItem != null) {
            lastItemCreateAt = (adapter.lastItem as PersonalCenterBookShare).get().createAt
        }
        val userId = UserManager.getInstance(activity).loginUserId
        viewModel.queryReceiveBookShare(userId, lastItemCreateAt).observe(this, Observer {
            it!!.apply {
                if (value == null) {
                    if (adapter.dataCount > 0) {
                        loadMoreView.noMore()
                    } else {
                        if (type == TYPE_FAILED) {
                            emptyView.failed()
                        } else if (type == TYPE_NO_CONTENT) {
                            emptyView.empty()
                        }
                    }
                    return@Observer
                }

                val receiveBookShareList = value as List<BookShareReceive>

                if (adapter.isInEmptyState) {
                    adapter.cancelEmptyState()
                }
                if (receiveBookShareList.size < 10) {
                    adapter.add(receiveBookShareList)
                    loadMoreView.noMore()
                } else {
                    adapter.add(receiveBookShareList)
                }
            }
        })
    }

    fun passBookShare(bookShare: BookShare, passOperation: TextView) {

    }

}