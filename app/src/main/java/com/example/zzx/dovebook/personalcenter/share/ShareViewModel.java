package com.example.zzx.dovebook.personalcenter.share;

import androidx.lifecycle.LiveData;
import com.example.baselibrary.LoadCallback;
import com.example.zzx.dovebook.base.BaseLiveData;
import com.example.zzx.dovebook.base.BaseViewModel;
import com.example.zzx.dovebook.base.ValueWrapper;
import com.example.zzx.dovebook.base.model.BookShare;
import com.example.zzx.dovebook.personalcenter.model.PersonalCenterBookShare;

import java.util.ArrayList;
import java.util.List;

import static com.example.zzx.dovebook.base.ValueWrapper.*;


public class ShareViewModel extends BaseViewModel<ShareRepository> {

    public ShareViewModel(ShareRepository reponsity) {
        super(reponsity);
    }

    public LiveData<ValueWrapper> queryUploadBookShare(String userId, long lastItemCreateAt) {
        BaseLiveData<List<PersonalCenterBookShare>> result = new BaseLiveData<>();
        getRepository().queryUploadBookShare(userId, lastItemCreateAt, new LoadCallback<List<BookShare>>() {
            @Override
            public void loadSuccess(List<BookShare> data) {
                List<PersonalCenterBookShare> list = new ArrayList<>();
                for (int i = 0; i < data.size(); i++) {
                    list.add(new PersonalCenterBookShare(data.get(i), PersonalCenterBookShare.TYPE_UPLOAD));
                }
                result.postValue(list, TYPE_NORMAL);
            }

            @Override
            public void loadFailed(String errorMessage) {
                postErrorMessage(errorMessage);
                result.postValue(null, TYPE_FAILED);
            }

            @Override
            public void loadNull(String message) {
                result.postValue(null, TYPE_NO_CONTENT);
            }
        });
        return result;
    }
}
