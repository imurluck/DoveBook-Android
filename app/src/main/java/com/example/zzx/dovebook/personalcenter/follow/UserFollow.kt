package com.example.zzx.dovebook.personalcenter.follow

import android.content.Context
import android.graphics.drawable.GradientDrawable
import android.text.TextUtils
import android.view.View.GONE
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.example.baselibrary.image.GlideManager
import com.example.library.BaseAdapter
import com.example.library.IEntity
import com.example.zzx.dovebook.R
import com.example.zzx.dovebook.base.extensions.startUserDetailActivity
import com.example.zzx.dovebook.personalcenter.follow.follower.FollowerActivity
import com.example.zzx.dovebook.personalcenter.follow.following.FollowingActivity

data class UserFollow(
        var userId: String,
        var userName: String,
        var userSex: String,
        var userAvatarPath: String,
        var userAge: Int,
        var userProfile: String,
        var ufId: String,
        var createAt: Long,
        var count: Int,
        var otherUfId: String,
        var followStatus: Int
): IEntity<UserFollow> {

    @Transient
    private lateinit var operationTextView: TextView

    override fun bindView(baseAdapter: BaseAdapter?, holder: BaseAdapter.ViewHolder, data: UserFollow?, position: Int) {
        holder.itemView.apply {
            setOnClickListener {
                context.startUserDetailActivity(userId, userName)
            }
            val activity = this!!.context
            if (TextUtils.isEmpty(userAvatarPath)) {
                GlideManager.loadImage(activity, R.drawable.default_avatar, findViewById(R.id.userAvatar))
            } else {
                GlideManager.loadImage(activity, userAvatarPath, findViewById(R.id.userAvatar))
            }
            findViewById<TextView>(R.id.userName).text = userName
            operationTextView = findViewById(R.id.followOperation)
            updateOperationView(activity)

        }
    }

    override fun getLayoutId(): Int {
        return R.layout.item_user_follow
    }

    fun updateOperationView(activity: Context) {
        if (activity is FollowingActivity) {
            if (!activity.isLoginUser()) {
                operationTextView.visibility = GONE
                return
            }
        }
        if (activity is FollowerActivity) {
            if (!activity.isLoginUser()) {
                operationTextView.visibility = GONE
                return
            }
        }
        when (followStatus) {
            STATUS_NOT_FOLLOW -> {
                operationTextView.text = "关注"
                operationTextView.background?.run {
                    if (this is GradientDrawable) {
                        setColor(ContextCompat.getColor(activity, R.color.colorAccent))
                    }
                }
                operationTextView.setOnClickListener {
                    if (activity is FollowerActivity) {
                        activity.follow(this)
                    } else if (activity is FollowingActivity) {
                        activity.follow(this)
                    }
                }
            }
            STATUS_FOLLOWED -> {
                operationTextView.text = "已关注"
                operationTextView.background?.run {
                    if (this is GradientDrawable) {
                        setColor(ContextCompat.getColor(activity, R.color.select_bg))
                    }
                }
                operationTextView.setOnClickListener {
                    if (activity is FollowerActivity) {
                        activity.cancelFollow(this)
                    } else if (activity is FollowingActivity) {
                        activity.cancelFollow(this)
                    }
                }
            }
            STATUS_FOLLOW_EACH_OTHER -> {
                operationTextView.text = "互相关注"
                operationTextView.background?.run {
                    if (this is GradientDrawable) {
                        setColor(ContextCompat.getColor(activity, R.color.select_bg))
                    }
                }
                operationTextView.setOnClickListener {
                    if (activity is FollowerActivity) {
                        activity.cancelFollow(this)
                    } else if (activity is FollowingActivity) {
                        activity.cancelFollow(this)
                    }
                }
            }
        }
    }

    companion object {
        /**
         * 未关注
         */
        private val STATUS_NOT_FOLLOW = 0

        /**
         * 已关注
         */
        private val STATUS_FOLLOWED = 1

        /**
         * 互相关注
         */
        private val STATUS_FOLLOW_EACH_OTHER = 2
    }

}