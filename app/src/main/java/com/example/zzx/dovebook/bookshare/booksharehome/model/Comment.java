package com.example.zzx.dovebook.bookshare.booksharehome.model;

import android.content.ContextWrapper;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;
import com.example.baselibrary.commonutil.FormatUtil;
import com.example.baselibrary.image.GlideManager;
import com.example.library.BaseAdapter;
import com.example.library.IGroupEntity;
import com.example.zzx.dovebook.R;
import com.example.zzx.dovebook.bookshare.booksharedetail.BookShareDetailActivity;
import com.qmuiteam.qmui.widget.dialog.QMUIDialog;

import java.util.List;

public class Comment extends IGroupEntity<CommentReply, Comment> {

    private String bookShareCommentId;

    private String parentId;

    private String bookShareId;

    private String fromUserId;

    private String toUserId;

    private String content;

    private long createAt;

    private String userName;

    private String userAvatarPath;

    private List<CommentReply> replyList;

    public String getBookShareCommentId() {
        return bookShareCommentId;
    }

    public void setBookShareCommentId(String bookShareCommentId) {
        this.bookShareCommentId = bookShareCommentId;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getBookShareId() {
        return bookShareId;
    }

    public void setBookShareId(String bookShareId) {
        this.bookShareId = bookShareId;
    }

    public String getFromUserId() {
        return fromUserId;
    }

    public void setFromUserId(String fromUserId) {
        this.fromUserId = fromUserId;
    }

    public String getToUserId() {
        return toUserId;
    }

    public void setToUserId(String toUserId) {
        this.toUserId = toUserId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public long getCreateAt() {
        return createAt;
    }

    public void setCreateAt(long createAt) {
        this.createAt = createAt;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserAvatarPath() {
        return userAvatarPath;
    }

    public void setUserAvatarPath(String userAvatarPath) {
        this.userAvatarPath = userAvatarPath;
    }

    public List<CommentReply> getReplyList() {
        return replyList;
    }

    public void setReplyList(List<CommentReply> replyList) {
        this.replyList = replyList;
    }

    @Override
    public List<CommentReply> returnChildList() {
        return replyList;
    }

    @Override
    public int returnChildSize() {
        if (replyList != null) {
            return replyList.size();
        }
        return 0;
    }

    @Override
    public int getLayoutId() {
        return R.layout.item_comment;
    }

    @Override
    public void bindView(BaseAdapter baseAdapter, BaseAdapter.ViewHolder holder, Comment data, int position) {
        View rootView = holder.itemView;
        BookShareDetailActivity activity = (BookShareDetailActivity) ((ContextWrapper) rootView.getContext()).getBaseContext();
        rootView.setOnClickListener((view) -> {
            String[] types = rootView.getContext().getResources().getStringArray(R.array.comment_type);
            new QMUIDialog.MenuDialogBuilder(activity)
                    .addItems(types, (dialog, which) -> {
                        switch (which) {
                            case 0:
                                activity.showReplyEdit(Comment.this);
                                break;
                            case 1:

                                break;
                        }
                        dialog.dismiss();
                    }).create().show();
        });
        ((TextView) rootView.findViewById(R.id.item_comment_user_name_tv))
                .setText("@" + userName);
        if (TextUtils.isEmpty(userAvatarPath)) {
            GlideManager.loadImage(activity, R.mipmap.ic_default_avatar, rootView.findViewById(R.id.item_comment_avatar_img));
        } else {
            GlideManager.loadImage(activity, userAvatarPath, rootView.findViewById(R.id.item_comment_avatar_img));
        }
        ((TextView) rootView.findViewById(R.id.item_comment_datetime_tv))
                .setText(FormatUtil.dateFormat(createAt));
        ((TextView) rootView.findViewById(R.id.item_comment_content_tv)).setText(content);
    }
}
