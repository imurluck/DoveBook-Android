package com.example.zzx.dovebook.personalcenter.receive

import com.example.baselibrary.LoadCallback
import com.example.baselibrary.constant.Constant
import com.example.baselibrary.net.HttpHelper
import com.example.zzx.dovebook.base.model.BookShare

class ReceiveRepository {

    private val TAG_QUERY_RECEIVE_BOOK_SHARE = "queryReceiveBookShare"

    fun queryReceiveBookShare(paraMap: Map<String, String>, loadCallback: LoadCallback<List<BookShare>>) {
        HttpHelper.getInstance().post(Constant.URL_QUERY_RECEIVE_BOOK_SHARE, paraMap, loadCallback, TAG_QUERY_RECEIVE_BOOK_SHARE)
    }


}