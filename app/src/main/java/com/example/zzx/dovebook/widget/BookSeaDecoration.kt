package com.example.dovebookui.widget

import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.example.zzx.dovebook.R

class BookSeaDecoration: RecyclerView.ItemDecoration() {

    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
        val space = parent.context.resources.getDimension(R.dimen.itemSpace).toInt()
        val layoutManager = parent.layoutManager
        var spanCount = 0
        if (layoutManager is GridLayoutManager) {
            spanCount = layoutManager.spanCount
        }
        if (layoutManager is StaggeredGridLayoutManager) {
            spanCount = layoutManager.spanCount
        }
        outRect.apply {
            val position = parent.getChildAdapterPosition(view)
            right = if (position % spanCount - 1 == 0) space else 0
            top = if (position < spanCount) space else 0
            left = space
            bottom = space
        }
    }
}