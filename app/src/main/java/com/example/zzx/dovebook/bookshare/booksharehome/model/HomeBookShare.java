package com.example.zzx.dovebook.bookshare.booksharehome.model;

import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.amap.api.services.geocoder.GeocodeResult;
import com.amap.api.services.geocoder.GeocodeSearch;
import com.amap.api.services.geocoder.RegeocodeAddress;
import com.amap.api.services.geocoder.RegeocodeResult;
import com.example.baselibrary.commonutil.FormatUtil;
import com.example.baselibrary.commonutil.Logger;
import com.example.baselibrary.image.GlideManager;
import com.example.library.BaseAdapter;
import com.example.library.IEntity;
import com.example.zzx.dovebook.R;
import com.example.zzx.dovebook.base.model.BookShare;
import com.example.zzx.dovebook.bookshare.booksharehome.BookShareHomeActivity;
import com.example.zzx.dovebook.user.UserManager;
import com.example.zzx.dovebook.utils.LocationUtil;
import com.example.zzx.heartanimation.LikeLayout;

import java.io.Serializable;

public class HomeBookShare implements Serializable, IEntity<HomeBookShare> {

    private static final String TAG = "HomeBookShare";

    public BookShare bookShare;

    public HomeBookShare(BookShare bookShare) {
        this.bookShare = bookShare;
    }


    @Override
    public int getLayoutId() {
        return R.layout.recycler_item_book_share_home;
    }

    @Override
    public void bindView(BaseAdapter baseAdapter, BaseAdapter.ViewHolder holder, HomeBookShare data, int position) {
        RelativeLayout rootView = (RelativeLayout) holder.itemView;
        BookShareHomeActivity activity = (BookShareHomeActivity) rootView.getContext();
        ((TextView)rootView.findViewById(R.id.book_share_home_recycler_item_user_name)).setText("@" + bookShare.getUserName());
        ((ImageView) rootView.findViewById(R.id.book_share_home_recycler_item_sex_icon))
                .setImageResource(TextUtils.equals(bookShare.getUserSex(), "女")
                        ? R.drawable.ic_sex_woman_white_16dp : R.drawable.ic_sex_man_white_16dp);
        GlideManager.loadImage(rootView.getContext(), bookShare.getBookImagePath(),
                rootView.findViewById(R.id.book_share_home_recycler_item_bg));
        if (TextUtils.isEmpty(bookShare.getUserAvatarPath())) {
            GlideManager.loadImage(rootView.getContext(), R.mipmap.ic_default_avatar, rootView.findViewById(R.id.book_share_home_recycler_item_avatar));
        } else {
            GlideManager.loadImage(rootView.getContext(), bookShare.getUserAvatarPath(),
                    rootView.findViewById(R.id.book_share_home_recycler_item_avatar));
        }
        TextView locationTv = rootView.findViewById(R.id.book_share_home_recycler_item_place_text);
        RelativeLayout locationLayout = rootView.findViewById(R.id.book_share_home_recycler_item_place_layout);
        if (isLocationAvaliable()) {
            locationLayout.setVisibility(View.VISIBLE);
            LocationUtil.INSTANCE.geocodeSearch(activity, bookShare.getLatitude(), bookShare.getLongitude(),
                    new GeocodeSearch.OnGeocodeSearchListener() {
                @Override
                public void onRegeocodeSearched(RegeocodeResult regeocodeResult, int resultCode) {
                    if (resultCode != 1000) {
                        Logger.e(TAG, "onRegeocodeSearched -> latitude:" + bookShare.getLatitude()
                                + "  longitude:" + bookShare.getLongitude() + "\n获取地址失败!");
                        return;
                    }
                    RegeocodeAddress address = regeocodeResult.getRegeocodeAddress();
                    locationTv.setText(address.getProvince() + " " + address.getCity());
                }

                @Override
                public void onGeocodeSearched(GeocodeResult geocodeResult, int i) {

                }
            });
        } else {
            locationLayout.setVisibility(View.GONE);
        }
        ((TextView) rootView.findViewById(R.id.book_share_home_recycler_item_description)).setText(bookShare.getDescription());
        ((TextView) rootView.findViewById(R.id.book_share_home_recycler_item_book_name)).setText(bookShare.getBookTitle());
        ((TextView) rootView.findViewById(R.id.book_share_home_recycler_item_book_state))
                .setText(bookShare.getBookStatusTip());

        LikeLayout likeLayout = rootView.findViewById(R.id.book_share_home_recycler_item_like_layout);
        likeLayout.setText(FormatUtil.countFormat(bookShare.getLikeCount()));
        if (bookShare.isHasLike()) {
            likeLayout.initState(likeLayout.getSTATE_FINAL());
        } else {
            likeLayout.initState(likeLayout.getSTATE_INITIAL());
        }
        likeLayout.setOnClickListener(layout -> {
            if (bookShare.isHasLike()) {
                activity.cancelBookShareLike(bookShare, likeLayout);
            } else {
                activity.bookShareLike(bookShare, likeLayout);
            }
        });
        LikeLayout commentLayout = rootView.findViewById(R.id.book_share_home_recycler_item_comment_layout);
        commentLayout.setText(FormatUtil.countFormat(bookShare.getCommentCount()));
        commentLayout.setOnClickListener((view) -> {
            activity.showCommentDialog(bookShare);
        });
        LikeLayout traceLayout = rootView.findViewById(R.id.book_share_home_recycler_item_track_layout);
        traceLayout.setOnClickListener((view) -> {
            activity.startBookTraceActivity(bookShare.getBookId(), bookShare.getBookStatus());
        });
        TextView followTv = rootView.findViewById(R.id.book_share_home_recycler_item_follow);
        String loginUserId = UserManager.getInstance(activity).getLoginUserId();
        if (bookShare.isHasFollowed() || TextUtils.equals(bookShare.getUserId(), loginUserId)) {
            followTv.setVisibility(View.GONE);
        } else {
            followTv.setVisibility(View.VISIBLE);
            followTv.setOnClickListener((view) -> activity.follow(bookShare, followTv));
        }
    }

    private boolean isLocationAvaliable() {
        return !(bookShare.getLatitude() == 0.0D && bookShare.getLongitude() == 0.0D);
    }
}
