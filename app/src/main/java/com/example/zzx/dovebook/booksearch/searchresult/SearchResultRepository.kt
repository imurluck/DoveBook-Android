package com.example.zzx.dovebook.booksearch.searchresult

import com.example.baselibrary.LoadCallback
import com.example.baselibrary.constant.Constant
import com.example.baselibrary.net.HttpHelper
import com.example.zzx.dovebook.base.model.BookShare

class SearchResultRepository {

    fun searchBookShare(params: Map<String, String>, loadCallback: LoadCallback<List<BookShare>>) {
        HttpHelper.getInstance().post(Constant.URL_SEARCH_BOOK_SHARE, params, loadCallback,
            TAG_SEARCH_BOOK_SHARE + params["startPosition"])
    }

    companion object {

        private const val TAG_SEARCH_BOOK_SHARE = "searchBookShare"
    }
}