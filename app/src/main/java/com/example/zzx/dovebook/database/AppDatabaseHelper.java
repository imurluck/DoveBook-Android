package com.example.zzx.dovebook.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;

import com.example.baselibrary.commonutil.Logger;

public class AppDatabaseHelper extends SQLiteOpenHelper {

    private static final String TAG = "AppDatabaseHelper";

    private static volatile AppDatabaseHelper sInstance;

    private static final String DB_NAME = "dovebook.db";
    private static final int DB_VERSION = 1;

    public static final int DELETE_ERROR = -1;
    public static final int UPDATE_ERROR = -1;

    public static final String USER_TABLE_NAME = "user";
    public static final String SEARCH_TABLE_NAME = "searchhistory";

    private SQLiteDatabase mDb;
    private Context mContext;

    private static final String CREATE_TABLE_USER = "CREATE TABLE IF NOT EXISTS " +
            USER_TABLE_NAME + "(user_id VARCHAR(255) PRIMARY KEY, " +
            "user_name VARCHAR(255), " +
            "user_password VARCHAR(255), " +
            "user_phone BIGINT(20), " +
            "user_sex VARCHAR(255), " +
            "user_avatar_path VARCHAR(255), " +
            "user_latitude VARCHAR(255), " +
            "user_longitude VARCHAR(255), " +
            "create_at BIGINT(20)," +
            "update_at BIGINT(20), " +
            "login_at BIGINT(20), " +
            "is_login INTEGER)";

    private static final String CREATE_TABLE_SEARCH_HISTORY = "CREATE TABLE IF NOT EXISTS " +
            SEARCH_TABLE_NAME + "(content VARCHAR(20) PRIMARY KEY, " +
            "updateAt TIMESTAMP)";

    private AppDatabaseHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
        mDb = getWritableDatabase();
        mContext = context;
    }

    public static AppDatabaseHelper getInstance(Context context) {
        if (sInstance == null) {
            synchronized (AppDatabaseHelper.class) {
                if (sInstance == null) {
                    sInstance = new AppDatabaseHelper(context);
                }
            }
        }
        return sInstance;
    }

    private boolean checkTableName(String tableName) {
        switch (tableName) {
            case USER_TABLE_NAME:
                return true;
            case SEARCH_TABLE_NAME:
                return true;
            default:
                    return false;
        }
    }

    public Cursor query(String table, Uri uri, String[] projection, String selection,
                        String[] selectionArgs, String sortOrder) {
        if (checkTableName(table) == false) {
            Logger.e(TAG, "the table " + table + " does'n exits");
            return null;
        }
        return mDb.query(table, projection, selection, selectionArgs, null, null, sortOrder, null);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_USER);
        db.execSQL(CREATE_TABLE_SEARCH_HISTORY);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public Uri insert(String table, Uri uri, ContentValues values) {
        if (checkTableName(table) == false) {
            Logger.e(TAG, "the table " + table + " does'n exits");
            return null;
        }
        mDb.insert(table, null, values);
        if (uri != null) {
            mContext.getContentResolver().notifyChange(uri, null);
        }
        return uri;
    }

    public int delete(String table, Uri uri, String selection, String[] selectionArgs) {
        if (checkTableName(table) == false) {
            Logger.e(TAG, "the table " + table + " does'n exits");
            return DELETE_ERROR;
        }
        return mDb.delete(table, selection, selectionArgs);
    }

    public int update(String table, Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        if (checkTableName(table) == false) {
            Logger.e(TAG, "the table " + table + " does'n exits");
            return UPDATE_ERROR;
        }
        return mDb.update(table, values, selection, selectionArgs);
    }
}
