package com.example.zzx.dovebook.booksearch.searchhistory

import android.app.Application

class SearchHistoryRepository(application: Application) {

    private val historyManager = HistoryManager(application)

    fun getHistoryFromLocal(): List<String>? = historyManager.getHistory()
    fun updateHistory(content: String): Boolean = historyManager.updateHistory(content)
    fun deleteHistory(content: String): Boolean = historyManager.deleteHistory(content)
    fun addHistory(content: String): Boolean = historyManager.addHistory(content)
}