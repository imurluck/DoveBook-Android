package com.example.zzx.dovebook.message.model

data class BookShareRequest(
        var bsqId: String,
        var bookId: String,
        var bookShareId: String,
        var requestUserId: String,
        var uploadUserId: String,
        var status: Int,
        var createAt: Long,
        var userName: String,
        var userSex: String,
        var userAge: Int,
        var userAvatarPath: String,
        var bookTitle: String,
        var bookImagePath: String,
        var bookAuthor: String
) {

    companion object {
        val STATUS_REQUESTING = 0
        val STATUS_REJECTED = 1
        val STATUS_AGREED = 2
        val STATUS_IGNORED = 3
    }
}