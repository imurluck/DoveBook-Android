package com.example.zzx.dovebook.personalcenter.pass;


import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import com.example.baselibrary.widget.LoadingStateView;
import com.example.library.BaseAdapter;
import com.example.zzx.dovebook.R;
import com.example.zzx.dovebook.base.BaseFragment;
import com.example.zzx.dovebook.personalcenter.model.PersonalCenterBookShare;
import com.example.zzx.dovebook.user.UserManager;

import java.util.List;

import static com.example.zzx.dovebook.base.ValueWrapper.TYPE_FAILED;
import static com.example.zzx.dovebook.base.ValueWrapper.TYPE_NO_CONTENT;

public class PassFragment extends BaseFragment<PassViewModel> {
    @BindView(R.id.person_center_pass_recycler)
    RecyclerView recyclerView;

    private LoadingStateView mLoadMoreView;
    private LoadingStateView mEmptyView;


    private BaseAdapter mAdapter;

    private long mLastItemCreateAt;


    @Override
    protected int getLayoutId() {
        return R.layout.fragment_personal_center_pass;
    }

    @Override
    protected void initViews() {
        setupRecyclerView();
    }

    @Override
    protected Class<PassViewModel> createViewModel() {
        return PassViewModel.class;
    }

    @Override
    protected Object createRepository() {
        return new PassRepository();
    }

    private void setupRecyclerView() {
        mEmptyView = new LoadingStateView(getActivity());
        mEmptyView.setOnRetryListener((stateView -> {
            stateView.loading();
            queryPassedBookShare();
        }));
        mLoadMoreView = new LoadingStateView(getActivity());
        mAdapter = new BaseAdapter.Builder()
                .autoLoadMore()
                .addRooter(mLoadMoreView)
                .emptyView(mEmptyView)
                .build();
        mAdapter.emptyState();
        mAdapter.setOnLoadMoreListener(new BaseAdapter.OnLoadMoreListener() {
            @Override
            public void loadMore(BaseAdapter baseAdapter) {
                recyclerView.post(() -> {
                    if (mLoadMoreView.isNoMore()) {
                        return ;
                    }
                    queryPassedBookShare();
                });
            }
        });
        recyclerView.setAdapter(mAdapter);
    }



    @Override
    protected void initOptions() {
        queryPassedBookShare();
    }

    private void queryPassedBookShare() {
        if (mLastItemCreateAt == 0L) {
            mLastItemCreateAt = System.currentTimeMillis();
        }
        if (mAdapter.getLastItem() != null) {
            mLastItemCreateAt = ((PersonalCenterBookShare) mAdapter.getLastItem()).get().getCreateAt();
        }
        String userId = UserManager.getInstance(getActivity()).getLoginUserId();
        getViewModel().queryPassedBookShare(userId, mLastItemCreateAt).observe(this, (wrapper) -> {
            if (wrapper.value == null) {
                if (mAdapter.getDataCount() > 0) {
                    mLoadMoreView.noMore();
                } else {
                    if (wrapper.type == TYPE_FAILED) {
                        mEmptyView.failed();
                    } else if (wrapper.type == TYPE_NO_CONTENT) {
                        mEmptyView.empty();
                    }
                }
                return ;
            }
            List<PersonalCenterBookShare> bookShareList = (List<PersonalCenterBookShare>) wrapper.value;
            if (mAdapter.isInEmptyState()) {
                mAdapter.cancelEmptyState();
            }
            if (bookShareList.size() < 10) {
                mAdapter.add(bookShareList);
                mLoadMoreView.noMore();
            } else {
                mAdapter.add(bookShareList);
            }
        });
    }
}
