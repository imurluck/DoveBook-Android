package com.example.zzx.dovebook.base.model;

import java.io.Serializable;

public class BookShare implements Serializable {

    /**
     * 图书正在漂流中
     */
    public static final int STATUS_DRIFTING = 2;
    /**
     * 已经有用户接收了这个图书
     */
    public static final int STATUS_RECEIVED = 1;
    /**
     * 漂流结束
     */
    public static final int STATUS_CLOSED = 0;

    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>User相关属性<<<<<<<<<<<<<<<<<<<<<<<<<<<<<//
    private String userName;

    private String userAvatarPath;

    private String userSex;

    private String userId;

    private boolean hasFollowed;

    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>Book相关属性<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<//
    private String bookTitle;

    private String bookAuthor;

    private String bookIsbn;

    private int bookStatus;

    private int bookLike;

    private String originUserId;

    private String holderUserId;

    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>BookShare相关属性<<<<<<<<<<<<<<<<<<<<<<<<<<<<//
    private String bookShareId;

    private String uploadUserId;

    private String receiveUserId;

    private String bookId;

    private String bookImagePath;

    private String description;

    private long createAt;

    private long updateAt;

    private double latitude;

    private double longitude;

    private int commentCount;

    private int likeCount;

    private boolean hasLike;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserAvatarPath() {
        return userAvatarPath;
    }

    public void setUserAvatarPath(String userAvatarPath) {
        this.userAvatarPath = userAvatarPath;
    }

    public String getUserSex() {
        return userSex;
    }

    public void setUserSex(String userSex) {
        this.userSex = userSex;
    }

    public String getBookTitle() {
        return bookTitle;
    }

    public void setBookTitle(String bookTitle) {
        this.bookTitle = bookTitle;
    }

    public String getBookAuthor() {
        return bookAuthor;
    }

    public void setBookAuthor(String bookAuthor) {
        this.bookAuthor = bookAuthor;
    }

    public String getBookImagePath() {
        return bookImagePath;
    }

    public void setBookImagePath(String bookImagePath) {
        this.bookImagePath = bookImagePath;
    }

    public String getBookIsbn() {
        return bookIsbn;
    }

    public void setBookIsbn(String bookIsbn) {

        this.bookIsbn = bookIsbn;
    }

    public int getBookStatus() {
        return bookStatus;
    }

    public String getBookStatusTip() {
        switch (bookStatus) {
            case STATUS_DRIFTING:
                return "漂流中";
            case STATUS_RECEIVED:
                return "已捞起";
            default:
                return "已退出";
        }
    }

    public void setBookStatus(int bookStatus) {
        this.bookStatus = bookStatus;

    }

    public String getHolderUserId() {
        return holderUserId;
    }

    public void setHolderUserId(String holderUserId) {
        this.holderUserId = holderUserId;
    }

    public int getBookLike() {
        return bookLike;
    }

    public void setBookLike(int bookLike) {
        this.bookLike = bookLike;
    }

    public String getBookShareId() {
        return bookShareId;
    }

    public void setBookShareId(String bookShareId) {
        this.bookShareId = bookShareId;
    }

    public String getUploadUserId() {
        return uploadUserId;
    }

    public void setUploadUserId(String uploadUserId) {
        this.uploadUserId = uploadUserId;
    }

    public String getOriginUserId() {
        return originUserId;
    }

    public void setOriginUserId(String originUserId) {
        this.originUserId = originUserId;
    }

    public String getReceiveUserId() {
        return receiveUserId;
    }

    public void setReceiveUserId(String receiveUserId) {
        this.receiveUserId = receiveUserId;
    }

    public String getBookId() {
        return bookId;
    }

    public void setBookId(String bookId) {
        this.bookId = bookId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getCreateAt() {
        return createAt;
    }

    public void setCreateAt(long createAt) {
        this.createAt = createAt;
    }

    public long getUpdateAt() {
        return updateAt;
    }

    public void setUpdateAt(long updateAt) {
        this.updateAt = updateAt;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public int getCommentCount() {
        return commentCount;
    }

    public void setCommentCount(int commentCount) {
        this.commentCount = commentCount;
    }

    public int getLikeCount() {
        return likeCount;
    }

    public void setLikeCount(int likeCount) {
        this.likeCount = likeCount;
    }

    public boolean isHasLike() {
        return hasLike;
    }

    public void setHasLike(boolean hasLike) {
        this.hasLike = hasLike;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public boolean isHasFollowed() {
        return hasFollowed;
    }

    public void setHasFollowed(boolean hasFollowed) {
        this.hasFollowed = hasFollowed;
    }
}
