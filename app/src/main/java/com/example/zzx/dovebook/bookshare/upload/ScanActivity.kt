package com.example.zzx.dovebook.bookshare.upload

import android.graphics.Bitmap
import android.os.Bundle
import android.view.MenuItem
import androidx.lifecycle.Observer
import com.example.baselibrary.commonutil.Logger
import com.example.baselibrary.constant.Constant
import com.example.zzx.dovebook.R
import com.example.zzx.dovebook.base.BaseActivity
import com.example.zzx.dovebook.base.ValueWrapper.*
import com.example.zzx.dovebook.base.extensions.startBookShareUploadActivity
import com.qmuiteam.qmui.widget.dialog.QMUITipDialog
import com.qmuiteam.qmui.widget.dialog.QMUITipDialog.Builder.*
import com.uuzuche.lib_zxing.activity.CaptureFragment
import com.uuzuche.lib_zxing.activity.CodeUtils
import kotlinx.android.synthetic.main.activity_scan.*

class ScanActivity: BaseActivity<ScanViewModel>() {

    private val captureFragment = CaptureFragment()

    override fun getLayoutId(): Int = R.layout.activity_scan

    override fun createViewModel(): Class<ScanViewModel> = ScanViewModel::class.java

    override fun createRepository(): Any = ScanRepository()

    override fun getStatusBarColor(): Int = STATUS_BAR_TRANSPARENT

    private lateinit var tipDialog: QMUITipDialog

    override fun initViews(savedInstanceState: Bundle?) {
        setupToolbar()
        setupCaptureFragment()

        tipDialog = QMUITipDialog.Builder(this)
            .setIconType(ICON_TYPE_LOADING)
            .setTipWord("查询中")
            .create(false)
    }

    private fun setupToolbar() {
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    override fun initOptions(savedInstanceState: Bundle?) {
        replaceFragment(captureFragment, R.id.container)
    }

    private fun setupCaptureFragment() {
        captureFragment.analyzeCallback = object: CodeUtils.AnalyzeCallback {
            override fun onAnalyzeSuccess(mBitmap: Bitmap, result: String) {
                getBookInfoFromNet(result)
            }

            override fun onAnalyzeFailed() {
            }

        }
    }

    /**
     * 从米软科技提供的ISBN查询接口查询图书信息
     */
    private fun getBookInfoFromNet(ISBN: String) {
        tipDialog.show()
        viewModel.getBookInfoFromNet(ISBN).observe(this@ScanActivity, Observer {
            tipDialog.dismiss()
            when (it.type) {
                TYPE_NORMAL -> {
                    Logger.e(TAG, "getBookInfoFromNet -> ${it.value}")
                    (it.value as BookNet).apply {
                        PhotoUrl = Constant.BASE_URL_BOOK_PHOTO + PhotoUrl
                        startBookShareUploadActivity(this)
                        finish()
                        return@Observer
                    }
                }
                TYPE_NO_CONTENT -> {
                    tipDialog = QMUITipDialog.Builder(this)
                        .setIconType(ICON_TYPE_NOTHING)
                        .setTipWord("没有查询到此图书")
                        .create()
                    tipDialog.show()
                }
                TYPE_FAILED -> {
                    tipDialog = QMUITipDialog.Builder(this)
                        .setIconType(ICON_TYPE_FAIL)
                        .setTipWord("查询出错")
                        .create()
                    tipDialog.show()
                }
            }
            mainHandler.postDelayed({
                if (tipDialog != null && tipDialog.isShowing) {
                    tipDialog.dismiss()
                }
            }, 1500)
        })
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> finish()
        }
        return super.onOptionsItemSelected(item)
    }

}