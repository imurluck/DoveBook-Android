package com.example.zzx.dovebook.bookshare.booksharedetail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.baselibrary.LoadCallback
import com.example.baselibrary.net.HttpUtil
import com.example.zzx.dovebook.base.BaseLiveData
import com.example.zzx.dovebook.base.CommonViewModel
import com.example.zzx.dovebook.base.ValueWrapper
import com.example.zzx.dovebook.base.ValueWrapper.*
import com.example.zzx.dovebook.bookshare.booksharehome.model.Comment
import com.example.zzx.dovebook.bookshare.booksharehome.model.CommentReply

class BookShareDetailViewModel(repository: BookShareDetailRepository):
    CommonViewModel<BookShareDetailRepository>(repository) {


    private val queryCommentKeys = arrayOf(
        "userId",
        "bookShareId",
        "date"
    )

    private val addCommentKeys = arrayOf(
        "bookShareId",
        "fromUserId",
        "content"
    )

    private val replyCommentKeys = arrayOf(
        "bookShareId",
        "parentId",
        "fromUserId",
        "toUserId",
        "content"
    )

    fun queryComments(userId: String, loginUserId: String, date: Long): LiveData<ValueWrapper<*>> {
        val result = BaseLiveData<List<Comment>>()
        val paramMap = HttpUtil.wrapParams(
            queryCommentKeys, userId,
            loginUserId, date
        )
        repository.queryComments(paramMap, object : LoadCallback<List<Comment>> {
            override fun loadSuccess(data: List<Comment>) {
                result.postValue(data, TYPE_NORMAL)
            }

            override fun loadFailed(errorMessage: String) {
                result.postValue(null, TYPE_FAILED)
            }

            override fun loadNull(message: String) {
                postErrorMessage(message)
                result.postValue(null, TYPE_NO_CONTENT)
            }
        })
        return result
    }

    fun addComment(comment: Comment): LiveData<Comment> {
        val paraMap = HttpUtil.wrapParams(
            addCommentKeys,
            comment.bookShareId, comment.fromUserId, comment.content
        )
        val result = MutableLiveData<Comment>()
        repository.addComment(paraMap, object : LoadCallback<Comment> {

            override fun loadSuccess(data: Comment) {
                result.postValue(data)
            }

            override fun loadFailed(errorMessage: String) {
                postErrorMessage(errorMessage)
            }

            override fun loadNull(message: String) {

            }
        })
        return result
    }

    fun replyComment(commentReply: Comment): LiveData<CommentReply> {
        val paraMap = HttpUtil.wrapParams(
            replyCommentKeys,
            commentReply.bookShareId, commentReply.parentId, commentReply.fromUserId,
            commentReply.toUserId, commentReply.content
        )
        val result = MutableLiveData<CommentReply>()
        repository.replyComment(paraMap, object : LoadCallback<Comment> {
            override fun loadSuccess(comment: Comment) {
                val reply = CommentReply()
                reply.replyBSCId = comment.bookShareCommentId
                reply.replyFromUserId = comment.fromUserId
                reply.replyContent = comment.content
                reply.replyToUserId = comment.toUserId
                result.postValue(reply)
            }

            override fun loadFailed(errorMessage: String) {
                postErrorMessage(errorMessage)
            }

            override fun loadNull(message: String) {

            }
        })
        return result
    }
}