package com.example.zzx.dovebook.base

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.baselibrary.LoadCallback
import com.example.baselibrary.net.HttpUtil
import com.example.zzx.dovebook.base.ValueWrapper.*
import com.example.zzx.dovebook.user.model.UserInfo

/**
 * 这里封装一些通用的数据请求方式, 例如对BookShare点赞，取消点赞等
 * create by zzx
 * create at 19-4-18
 */
open class CommonViewModel<R: CommonRepository>(repository: R): BaseViewModel<R>(repository) {

    private val bookShareLikeKeys = arrayOf(
        "bookShareId",
        "userId"
    )

    private val cancelBookShareKeys = arrayOf(
        "bookShareId",
        "userId"
    )

    private val followKeys = arrayOf(
        "fromUserId",
        "toUserId"
    )

    private val getUserInfoKeys = arrayOf(
        "userId"
    )

    private val uploadBookShareKeys = arrayOf(
        "book.bookTitle",
        "book.bookAuthor",
        "book.bookIsbn",
        "bookShare.uploadUserId",
        "bookShare.description",
        "bookShare.latitude",
        "bookShare.longitude"
    )


    fun uploadBookShare(bookTitle: String, author: String,
                        ISBN: String, uploadUserId: String,
                        description: String, latitude: Double,
                        longitude: Double, photoUrl: String): BaseLiveData<Boolean> {
        val result = BaseLiveData<Boolean>()
        val params = HttpUtil.wrapParams(uploadBookShareKeys, bookTitle, author, ISBN,
            uploadUserId, description, latitude, longitude)
        val photoFile = HttpUtil.createFilePart("coverFile", photoUrl)
        repository.uploadBookShare(params, photoFile, object: LoadCallback<Boolean> {
            override fun loadSuccess(data: Boolean) {
                result.postValue(data, TYPE_NORMAL)
            }

            override fun loadFailed(errorMessage: String?) {
                postErrorMessage(errorMessage)
                result.postValue(null, TYPE_FAILED)
            }

            override fun loadNull(message: String?) {
            }

        })
        return result
    }

    fun bookShareLike(bookShareId: String, userId: String): LiveData<Boolean> {
        val result = MutableLiveData<Boolean>()
        val params = HttpUtil.wrapParams(bookShareLikeKeys, bookShareId, userId)
        repository.bookShareLike(params, object: LoadCallback<Boolean> {
            override fun loadSuccess(data: Boolean) {
                result.postValue(data)
            }

            override fun loadFailed(errorMessage: String?) {
                postErrorMessage(errorMessage)
            }

            override fun loadNull(message: String?) {

            }

        })
        return result
    }

    fun cancelBookShareLike(bookShareId: String, userId: String): LiveData<Boolean> {
        val result = MutableLiveData<Boolean>()
        val params = HttpUtil.wrapParams(cancelBookShareKeys, bookShareId, userId)
        repository.cancelBookShareLike(params, object: LoadCallback<Boolean> {
            override fun loadSuccess(data: Boolean) {
                result.postValue(data)
            }

            override fun loadFailed(errorMessage: String?) {
                postErrorMessage(errorMessage)
            }

            override fun loadNull(message: String?) {

            }

        })
        return result
    }

    //用户关注
    fun follow(fromUserId: String, userId: String): LiveData<Int> {
        val result = MutableLiveData<Int>()
        val paraMap = HttpUtil.wrapParams(followKeys, fromUserId, userId)
        repository.follow(paraMap, object : LoadCallback<Int> {

            override fun loadSuccess(data: Int?) {
                result.postValue(data)
            }

            override fun loadFailed(errorMessage: String) {
                postErrorMessage(errorMessage)
            }

            override fun loadNull(message: String) {

            }
        })
        return result
    }

    /**
     * 获取用户信息，会包含用户关注数，粉丝数等额外信息
     */
    fun getUserInfo(userId: String): BaseLiveData<UserInfo> {
        val result = BaseLiveData<UserInfo>()
        val params = HttpUtil.wrapParams(getUserInfoKeys, userId)
        repository.getUserInfo(params, object: LoadCallback<UserInfo> {
            override fun loadSuccess(data: UserInfo) {
                result.postValue(data, TYPE_NORMAL)
            }

            override fun loadFailed(errorMessage: String?) {
                result.postValue(null, TYPE_FAILED)
            }

            override fun loadNull(message: String?) {
                result.postValue(null, TYPE_NO_CONTENT)
            }
        })
        return result
    }
}