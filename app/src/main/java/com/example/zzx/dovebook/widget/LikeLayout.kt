package com.example.zzx.heartanimation

import android.content.Context
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import android.view.Gravity
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.example.baselibrary.R

class LikeLayout @JvmOverloads constructor(
        context: Context, attributeSet: AttributeSet? = null, defStyleAttr: Int = 0
): LinearLayout(context, attributeSet, defStyleAttr) {

    val STATE_INITIAL = 0
    val STATE_FINAL = 1
    var currentState = STATE_INITIAL
        get

    val MODE_LIKE = 0
    val MODE_ICON = 1

    val LAYOUT_MODE_VERTICAL = 3
    val LAYOUT_MODE_HORIZONTAL = 4

    var mode: Int

    var initialColor: Int
    var finalColor: Int
    var iconDrawableRes: Drawable? = null

    lateinit var heartView: HeartView
    lateinit var iconView: ImageView
    lateinit var hintTextView: TextView
    private var hintText: CharSequence? = ""

    init {
        val a = context.obtainStyledAttributes(attributeSet, R.styleable.LikeLayout)
        initialColor = a.getColor(R.styleable.LikeLayout_initialColor, ContextCompat.getColor(context, R.color.color_white))
        finalColor = a.getColor(R.styleable.LikeLayout_finalColor, ContextCompat.getColor(context, R.color.colorAccent))
        mode = a.getInt(R.styleable.LikeLayout_mode, MODE_LIKE)
        layoutMode = a.getInt(R.styleable.LikeLayout_layoutMode, LAYOUT_MODE_VERTICAL)
        try {
            iconDrawableRes = a.getDrawable(R.styleable.LikeLayout_iconDrawable)
        } catch (e: Exception) {

        }
        hintText = a.getText(R.styleable.LikeLayout_hintText)
        a.recycle()

        orientation = if (layoutMode == LAYOUT_MODE_VERTICAL) VERTICAL else HORIZONTAL

        initChild()
    }


    private fun initChild() {
        if (mode == MODE_LIKE) {
            heartView = HeartView(context)
            heartView.setColor(initialColor, finalColor)
            val layoutParams = LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT)
//            if (layoutMode == LAYOUT_MODE_VERTICAL) {
//                layoutParams.bottomMargin = 5
//            } else {
//                layoutParams.rightMargin = 5
//            }
            heartView.layoutParams = layoutParams
        } else if (mode == MODE_ICON) {
            iconView = ImageView(context)
            iconView.setImageDrawable(iconDrawableRes)
            iconView.scaleType = ImageView.ScaleType.CENTER_CROP
            val layoutParams = LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT)
            if (layoutMode == LAYOUT_MODE_VERTICAL) {
//                layoutParams.bottomMargin = 5
                layoutParams.gravity = Gravity.CENTER_HORIZONTAL
            } else {
//                layoutParams.rightMargin = 5
                layoutParams.gravity = Gravity.CENTER_VERTICAL
            }
            iconView.layoutParams = layoutParams
        }
        hintTextView = TextView(context)
        hintTextView.text = hintText
        hintTextView.setTextAppearance(context, R.style.TextAppearance_AppCompat_Caption)
        hintTextView.setTextColor(initialColor)
        hintTextView.gravity = Gravity.CENTER
        val layoutParams = LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT)
        hintTextView.layoutParams = layoutParams
        if (mode == MODE_LIKE) {
            addView(heartView)
        } else if (mode == MODE_ICON) {
            addView(iconView)
        }
        addView(hintTextView)
    }

    fun toggle(hintText: String) {
        currentState = (currentState +1) % 2
        if (mode == MODE_LIKE) {
            heartView.toggle()
        }
        hintTextView.text = hintText
    }

    fun setText(hintText: String) {
        this.hintText = hintText
        hintTextView.text = hintText
    }

    fun initState(state: Int) {
        currentState = if (state == STATE_FINAL) STATE_FINAL else STATE_INITIAL
        if (mode == MODE_LIKE) {
            heartView.initState(state)
        }
    }

    fun isInitial(): Boolean = currentState == STATE_INITIAL

    fun isFinal(): Boolean = currentState == STATE_FINAL
}