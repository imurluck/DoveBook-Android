package com.example.zzx.dovebook.bookshare.booksharehome;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.amap.api.services.core.LatLonPoint;
import com.example.baselibrary.LoadCallback;
import com.example.baselibrary.net.HttpUtil;
import com.example.zzx.dovebook.base.BaseLiveData;
import com.example.zzx.dovebook.base.BaseViewModel;
import com.example.zzx.dovebook.base.ValueWrapper;
import com.example.zzx.dovebook.base.model.BookShare;
import com.example.zzx.dovebook.bookshare.booksharehome.model.Comment;
import com.example.zzx.dovebook.bookshare.booksharehome.model.CommentReply;
import com.example.zzx.dovebook.bookshare.booksharehome.model.HomeBookShare;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.example.zzx.dovebook.base.ValueWrapper.*;

public class BookShareHomeViewModel extends BaseViewModel<BookShareHomeRepository> {

    public BookShareHomeViewModel(BookShareHomeRepository reponsity) {
        super(reponsity);
    }

    private String[] mAddBookShareKeys = new String[] {
            "book.bookTitle",
            "book.bookAuthor",
            "book.bookIsbn",
            "bookShare.uploadUserId",
            "bookShare.description"
    };

    private String[] mAddBookShareLocationKeys = new String[] {
            "bookShare.latitude",
            "bookShare.longitude"
    };

    private String[] mAddCommentKeys = new String[] {
            "bookShareId",
            "fromUserId",
            "content"
    };

    private String[] mReplyCommentKeys = new String[] {
            "bookShareId",
            "parentId",
            "fromUserId",
            "toUserId",
            "content"
    };

    private String[] mQueryCommentKeys = new String[] {
            "userId",
            "bookShareId",
            "date"
    };

    private String[] mFollowKeys = new String[] {
            "fromUserId",
            "toUserId"
    };

    public LiveData<ValueWrapper> queryHotBookShare(String userId, long createAt) {
        BaseLiveData<List<HomeBookShare>> result = new BaseLiveData<>();
        getRepository().queryHotBookShare(userId, createAt, new LoadCallback<List<BookShare>>() {
            @Override
            public void loadSuccess(List<BookShare> data) {
                List<HomeBookShare> wrapData = new ArrayList<>();
                for (BookShare bookShare : data) {
                    wrapData.add(new HomeBookShare(bookShare));
                }
                result.postValue(wrapData, TYPE_NORMAL);
            }

            @Override
            public void loadFailed(String errorMessage) {
                postErrorMessage(errorMessage);
                result.postValue(null, TYPE_FAILED);
            }

            @Override
            public void loadNull(String message) {
                postErrorMessage(message);
                result.postValue(null, TYPE_FAILED);
            }
        });
        return result;
    }

    public LiveData<ValueWrapper> uploadBookShare(String mCoverPath, String bookName, String bookAuthor, String bookIsbn, String uploadUserId,
                                                  String description, LatLonPoint latLonPoint) {
        BaseLiveData<Boolean> result = new BaseLiveData<>();
        String[] keys;
        Map paramMap;
        if (latLonPoint != null) {
            keys = new String[mAddBookShareKeys.length + 2];
            System.arraycopy(mAddBookShareKeys, 0, keys, 0, mAddBookShareKeys.length);
            System.arraycopy(mAddBookShareLocationKeys, 0, keys, mAddBookShareKeys.length, mAddBookShareLocationKeys.length);
            paramMap = HttpUtil.wrapParams(keys, bookName,
                    bookAuthor, bookIsbn, uploadUserId, description, latLonPoint.getLatitude(), latLonPoint.getLongitude());
        } else {
            keys = mAddCommentKeys;
            paramMap = HttpUtil.wrapParams(keys, bookName,
                    bookAuthor, bookIsbn, uploadUserId, description);
        }
        getRepository().uploadBookShare(paramMap, mCoverPath, new LoadCallback<Boolean>() {
            @Override
            public void loadSuccess(Boolean data) {
                result.postValue(data, TYPE_NORMAL);
            }

            @Override
            public void loadFailed(String errorMessage) {
                postErrorMessage(errorMessage);
                result.postValue(null, TYPE_FAILED);
            }

            @Override
            public void loadNull(String message) {
                result.postValue(null, TYPE_NO_CONTENT);
            }
        });
        return result;
    }

    public LiveData<Comment> addComment(Comment comment) {
        Map<String, String> paraMap = HttpUtil.wrapParams(mAddCommentKeys,
                comment.getBookShareId(), comment.getFromUserId(), comment.getContent());
        MutableLiveData<Comment> result = new MutableLiveData<>();
        getRepository().addComment(paraMap, new LoadCallback<Comment>() {

            @Override
            public void loadSuccess(Comment data) {
                result.postValue(data);
            }

            @Override
            public void loadFailed(String errorMessage) {
                postErrorMessage(errorMessage);
            }

            @Override
            public void loadNull(String message) {

            }
        });
        return result;
    }

    public LiveData<CommentReply> replyComment(Comment commentReply) {
        Map<String, String> paraMap = HttpUtil.wrapParams(mReplyCommentKeys,
                commentReply.getBookShareId(), commentReply.getParentId(), commentReply.getFromUserId(),
                commentReply.getToUserId(), commentReply.getContent());
        MutableLiveData<CommentReply> result = new MutableLiveData<>();
        getRepository().replyComment(paraMap, new LoadCallback<Comment>() {
            @Override
            public void loadSuccess(Comment comment) {
                CommentReply reply = new CommentReply();
                reply.setReplyBSCId(comment.getBookShareCommentId());
                reply.setReplyFromUserId(comment.getFromUserId());
                reply.setReplyContent(comment.getContent());
                reply.setReplyToUserId(comment.getToUserId());
                result.postValue(reply);
            }

            @Override
            public void loadFailed(String errorMessage) {
                postErrorMessage(errorMessage);
            }

            @Override
            public void loadNull(String message) {

            }
        });
        return result;
    }

    public LiveData<ValueWrapper> queryComments(String userId, String loginUserId, long date) {
        BaseLiveData<List<Comment>> result = new BaseLiveData<>();
        Map<String, String> paramMap = HttpUtil.wrapParams(mQueryCommentKeys, userId,
                loginUserId, date);
        getRepository().queryComments(paramMap, new LoadCallback<List<Comment>>() {
            @Override
            public void loadSuccess(List<Comment> data) {
                result.postValue(data, TYPE_NORMAL);
            }

            @Override
            public void loadFailed(String errorMessage) {
                result.postValue(null, TYPE_FAILED);
            }

            @Override
            public void loadNull(String message) {
                postErrorMessage(message);
                result.postValue(null, TYPE_NO_CONTENT);
            }
        });
        return result;
    }

    public LiveData<Boolean> bookShareLike(String bookShareId, String userId) {
        MutableLiveData<Boolean> result = new MutableLiveData<>();
        getRepository().bookShareLike(bookShareId, userId, new LoadCallback<Boolean>() {

            @Override
            public void loadSuccess(Boolean data) {
                result.postValue(data);
            }

            @Override
            public void loadFailed(String errorMessage) {
                postErrorMessage(errorMessage);
            }

            @Override
            public void loadNull(String message) {
                postErrorMessage(message);
            }
        });
        return result;
    }

    public LiveData<Boolean> cancelBookShareLike(String bookShareId, String userId) {
        MutableLiveData<Boolean> result = new MutableLiveData<>();
        getRepository().cancelBookShareLike(bookShareId, userId, new LoadCallback<Boolean>() {
            @Override
            public void loadSuccess(Boolean data) {
                result.postValue(data);
            }

            @Override
            public void loadFailed(String errorMessage) {
                postErrorMessage(errorMessage);
            }

            @Override
            public void loadNull(String message) {
                postErrorMessage(message);
            }
        });
        return result;
    }

    public LiveData<Integer> follow(String fromUserId, String userId) {
        MutableLiveData<Integer> result = new MutableLiveData<>();
        Map paraMap = HttpUtil.wrapParams(mFollowKeys, fromUserId, userId);
        getRepository().follow(paraMap, new LoadCallback<Integer>() {

            @Override
            public void loadSuccess(Integer data) {
                result.postValue(data);
            }

            @Override
            public void loadFailed(String errorMessage) {
                postErrorMessage(errorMessage);
            }

            @Override
            public void loadNull(String message) {

            }
        });
        return result;
    }
}
