package com.example.zzx.dovebook.main.booksea.recommendation

import com.example.baselibrary.LoadCallback
import com.example.baselibrary.net.HttpUtil
import com.example.zzx.dovebook.base.BaseLiveData
import com.example.zzx.dovebook.base.CommonRepository
import com.example.zzx.dovebook.base.CommonViewModel
import com.example.zzx.dovebook.base.UserContext
import com.example.zzx.dovebook.base.ValueWrapper.*
import com.example.zzx.dovebook.base.model.BookShare

class PopularViewModel(repository: CommonRepository): CommonViewModel<CommonRepository>(repository) {

    private val queryHotBookShareKeys = arrayOf(
        "userId",
        "createAt"
    )

    fun queryHotBookShare(userContext: UserContext, userId: String, lastCreateAt: Long): BaseLiveData<List<ListBookShare>> {
        val result = BaseLiveData<List<ListBookShare>>()
        val params = HttpUtil.wrapParams(queryHotBookShareKeys, userId, lastCreateAt)
        repository.queryHotBookShare(params, object: LoadCallback<List<BookShare>> {

            override fun loadSuccess(data: List<BookShare>) {
                result.postValue(mutableListOf<ListBookShare>().apply {
                    for (bookShare in data) {
                        add(ListBookShare(bookShare, userContext))
                    }
                }, TYPE_NORMAL)
            }

            override fun loadFailed(errorMessage: String?) {
                result.postValue(null, TYPE_FAILED)
            }

            override fun loadNull(message: String?) {
                result.postValue(null, TYPE_NO_CONTENT)
            }
        })
        return result
    }

}