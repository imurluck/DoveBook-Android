package com.example.zzx.dovebook.bookshare.booksharehome.model;

import android.content.ContextWrapper;
import android.text.TextUtils;
import com.example.baselibrary.commonutil.ToastUtil;
import com.example.baselibrary.widget.FixSpanTextView;
import com.example.library.BaseAdapter;
import com.example.library.IChildEntity;
import com.example.library.IEntity;
import com.example.zzx.dovebook.R;
import com.example.zzx.dovebook.bookshare.booksharedetail.BookShareDetailActivity;

import java.util.ArrayList;
import java.util.List;

public class CommentReply extends IChildEntity {

    private String replyBSCId;

    private String replyFromUserId;

    private String replyFromUserName;

    private String replyToUserId;

    private String replyToUserName;

    private String replyContent;

    public String getReplyBSCId() {
        return replyBSCId;
    }

    public void setReplyBSCId(String replyBSCId) {
        this.replyBSCId = replyBSCId;
    }

    public String getReplyFromUserId() {
        return replyFromUserId;
    }

    public void setReplyFromUserId(String replyFromUserId) {
        this.replyFromUserId = replyFromUserId;
    }

    public String getReplyFromUserName() {
        return replyFromUserName;
    }

    public void setReplyFromUserName(String replyFromUserName) {
        this.replyFromUserName = replyFromUserName;
    }

    public String getReplyToUserId() {
        return replyToUserId;
    }

    public void setReplyToUserId(String replyToUserId) {
        this.replyToUserId = replyToUserId;
    }

    public String getReplyToUserName() {
        return replyToUserName;
    }

    public void setReplyToUserName(String replyToUserName) {
        this.replyToUserName = replyToUserName;
    }

    public String getReplyContent() {
        return replyContent;
    }

    public void setReplyContent(String replyContent) {
        this.replyContent = replyContent;
    }

    @Override
    public int getLayoutId() {
        return R.layout.item_comment_reply;
    }

    @Override
    public void bindView(BaseAdapter baseAdapter, BaseAdapter.ViewHolder holder, IEntity data, int position) {
        FixSpanTextView fixSpanTextView = holder.itemView.findViewById(R.id.item_comment_reply_content);
        BookShareDetailActivity activity = (BookShareDetailActivity) ((ContextWrapper) fixSpanTextView.getContext()).getBaseContext();
        String text = "";
        List<String> spanTexts = new ArrayList<>();
        spanTexts.add("@" + replyFromUserName);
        text += "@" + replyFromUserName;
        if (!TextUtils.isEmpty(replyToUserName)) {
            if (TextUtils.equals(replyFromUserId, replyToUserId)) {
                text += ":";
            } else {
                spanTexts.add("@" + replyToUserName);
                text += "回复" + "@" + replyToUserName + ":";
            }
        }
        text += replyContent;
        fixSpanTextView.setSpanText(text, spanTexts.toArray(new String[]{}), (clickText) -> {
            ToastUtil.shortToast(fixSpanTextView.getContext(), clickText);
        });
        fixSpanTextView.setOnClickListener((view) -> {
            activity.showReplyEdit(this, (Comment) groupEntity);
        });
    }
}
