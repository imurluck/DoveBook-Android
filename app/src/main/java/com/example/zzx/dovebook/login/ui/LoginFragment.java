package com.example.zzx.dovebook.login.ui;

import android.os.Build;
import android.widget.EditText;
import android.widget.RelativeLayout;
import butterknife.BindView;
import butterknife.OnClick;
import com.example.zzx.dovebook.R;
import com.example.zzx.dovebook.base.BaseFragment;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class LoginFragment extends BaseFragment {

    @BindView(R.id.login_sign_fab)
    FloatingActionButton signupFab;
    @BindView(R.id.login_root_layout)
    RelativeLayout rootLayout;

    @BindView(R.id.login_user_name_edit)
    EditText userNameEdit;
    @BindView(R.id.login_user_password_edit)
    EditText passwordEdit;

    @OnClick(R.id.login_login_btn)
    public void login() {
        ((LoginActivity) getActivity()).login(userNameEdit.getText().toString(),
                passwordEdit.getText().toString());
    }


    @Override
    protected Class createViewModel() {
        return null;
    }

    @Override
    protected void initViews() {
        setupSignupFab();
    }

    private void setupSignupFab() {
        signupFab.setOnClickListener((view) -> {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                ((LoginActivity) getActivity()).startSignupFragment(signupFab,
                        signupFab.getTransitionName());
            } else {
                ((LoginActivity) getActivity()).startSignupFragment(null, null);
            }
        });
    }


    @Override
    protected int getLayoutId() {
        return R.layout.fragment_login;
    }
}