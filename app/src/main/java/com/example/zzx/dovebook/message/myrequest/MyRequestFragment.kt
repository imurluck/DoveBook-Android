package com.example.zzx.dovebook.message.myrequest

import android.graphics.Color
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import com.example.baselibrary.widget.LoadingStateView
import com.example.library.BaseAdapter
import com.example.zzx.dovebook.R
import com.example.zzx.dovebook.base.BaseFragment
import com.example.zzx.dovebook.base.ValueWrapper.TYPE_FAILED
import com.example.zzx.dovebook.base.ValueWrapper.TYPE_NO_CONTENT
import com.example.zzx.dovebook.message.model.BookShareMyRequest
import com.example.zzx.dovebook.user.UserManager
import kotlinx.android.synthetic.main.fragment_request.*


class MyRequestFragment: BaseFragment<MyRequestViewModel>() {

    private lateinit var adapter: BaseAdapter
    private lateinit var emptyView: LoadingStateView
    private lateinit var loadMoreView: LoadingStateView

    private var lastItemCreateAt = System.currentTimeMillis()

    override fun createViewModel(): Class<MyRequestViewModel> {
        return MyRequestViewModel::class.java
    }

    override fun getLayoutId(): Int {
        return R.layout.fragment_request
    }

    override fun createRepository(): Any {
        return MyRequestRepository()
    }

    override fun initViews() {
        setupRecycler()
    }

    override fun initOptions() {
        queryMyRequest()
    }

    private fun queryMyRequest() {
        val requestUserId = UserManager.getInstance(activity).loginUserId ?: ""
        adapter.lastItem?.let {
            lastItemCreateAt = (it as BookShareMyRequest).bookShareRequest.createAt
        }
        viewModel.queryMyRequest(requestUserId, lastItemCreateAt).observe(this, Observer {
            it!!.value ?: run {
                if (adapter.dataCount > 0) {
                    when (it.type) {
                        TYPE_FAILED -> loadMoreView.failed()
                        TYPE_NO_CONTENT -> loadMoreView.noMore()
                    }
                } else {
                    when (it.type) {
                        TYPE_FAILED -> emptyView.failed()
                        TYPE_NO_CONTENT -> emptyView.empty()
                    }
                }
                return@Observer
            }
            emptyView.success()
            if (adapter.isInEmptyState) adapter.cancelEmptyState()
            val myRequestList = it.value as List<BookShareMyRequest>
            if (myRequestList.size < 20) {
                loadMoreView.noMore()
            }
            adapter.add(myRequestList)
        })
    }

    private fun setupRecycler() {
        emptyView = LoadingStateView(activity)
        loadMoreView = LoadingStateView(activity)
        emptyView.setTextColor(ContextCompat.getColor(activity!!, R.color.support_text))
        loadMoreView.setTextColor(Color.TRANSPARENT)
        adapter = BaseAdapter.Builder().emptyView(emptyView).addRooter(loadMoreView).build()
        adapter.emptyState()
        adapter.setOnLoadMoreListener {
            if (!loadMoreView.isNoMore) {
                queryMyRequest()
            }
        }
        loadMoreView.setOnRetryListener {
            loadMoreView.loading()
            queryMyRequest()
        }
        emptyView.setOnRetryListener {
            emptyView.loading()
            queryMyRequest()
        }
        adapter.setOnLoadMoreListener {
            if (loadMoreView.isNoMore) {
                return@setOnLoadMoreListener
            }
            queryMyRequest()
        }
        recyclerView.adapter = adapter
    }
}