package com.example.zzx.dovebook.personalcenter;

import com.example.baselibrary.LoadCallback;
import com.example.baselibrary.constant.Constant;
import com.example.baselibrary.net.HttpHelper;
import com.example.zzx.dovebook.personalcenter.follow.UserFollowCount;

import java.util.Map;

import okhttp3.MultipartBody;

public class PersonalCenterRepository {

    private static final String TAG_UPDATE_USER_AVATAR = "updateUserAvatar";
    private static final String TAG_QUERY_FOLLOW_COUNT = "queryFollowCount";

    public void updateUserAvatar(Map<String, String> paraMap, MultipartBody.Part avatarFile, LoadCallback<String> loadCallback) {
        HttpHelper.getInstance().post(Constant.URL_USER_UPDATE_AVATAR, paraMap, loadCallback, TAG_UPDATE_USER_AVATAR, avatarFile);
    }

    public void queryFollowCount(Map paraMap, LoadCallback<UserFollowCount> loadCallback) {
        HttpHelper.getInstance().post(Constant.URL_USER_QUERY_FOLLOW_COUNT, paraMap, loadCallback, TAG_QUERY_FOLLOW_COUNT);
    }
}
