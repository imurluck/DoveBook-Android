package com.example.zzx.dovebook.personalcenter;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.example.baselibrary.LoadCallback;
import com.example.baselibrary.net.HttpUtil;
import com.example.zzx.dovebook.base.BaseViewModel;
import com.example.zzx.dovebook.personalcenter.follow.UserFollowCount;
import okhttp3.MultipartBody;

import java.util.Map;

public class PersonalCenterViewModel extends BaseViewModel<PersonalCenterRepository> {

    public PersonalCenterViewModel(PersonalCenterRepository repository) {
        super(repository);
    }

    private String[] updateUserAvatarKeys = {"userId"};

    private String[] queryFollowCountKeys = {"userId"};

    public LiveData<String> updateUserAvatarPath(String avatarFilePath, String userId) {
        MutableLiveData<String> result = new MutableLiveData<>();
        Map<String, String> paraMap = HttpUtil.wrapParams(updateUserAvatarKeys, userId);
        MultipartBody.Part avatarFile = HttpUtil.createFilePart("avatarFile", avatarFilePath);
        getRepository().updateUserAvatar(paraMap, avatarFile, new LoadCallback<String>() {

            @Override
            public void loadSuccess(String data) {
                result.postValue(data);
            }

            @Override
            public void loadFailed(String errorMessage) {
                postErrorMessage(errorMessage);
            }

            @Override
            public void loadNull(String message) {

            }
        });
        return result;
    }

    public LiveData<UserFollowCount> queryFollowCount(String userId) {
        MutableLiveData<UserFollowCount> result = new MutableLiveData<>();
        Map paraMap = HttpUtil.wrapParams(queryFollowCountKeys, userId);
        getRepository().queryFollowCount(paraMap, new LoadCallback<UserFollowCount>() {

            @Override
            public void loadSuccess(UserFollowCount data) {
                result.postValue(data);
            }

            @Override
            public void loadFailed(String errorMessage) {

            }

            @Override
            public void loadNull(String message) {

            }
        });
        return result;
    }
}
