package com.example.zzx.dovebook.base;

import android.text.TextUtils;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class BaseViewModel<R> extends ViewModel {

    private static final String DEFAULT_ERROR_MESSAGE = "发生了未知错误，我们正在努力排查...";

    private R mRepository;

    private MutableLiveData<String> mErrorMessage = new MutableLiveData<>();
    private MutableLiveData<String> mMessage = new MutableLiveData<>();

    public BaseViewModel() {
        this(null);
    }

    public BaseViewModel(R repository) {
        this.mRepository = repository;
    }

    public LiveData<String> getErrorMessage() {
        return mErrorMessage;
    }

    public LiveData<String> getMessage() {
        return mMessage;
    }

    protected void setMessage(String message) {
        if (TextUtils.isEmpty(message)) {
            return ;
        }
        mMessage.setValue(message);
    }

    protected void postMessage(String message) {
        if (TextUtils.isEmpty(message)) {
            return ;
        }
        mMessage.postValue(message);
    }

    protected void setErrorMessage(String errorMessage) {
        if (errorMessage == null) {
            mErrorMessage.setValue(DEFAULT_ERROR_MESSAGE);
            return ;
        }

        mErrorMessage.setValue(errorMessage);
    }

    protected void postErrorMessage(String message) {
        if (message == null) {
            mErrorMessage.postValue(DEFAULT_ERROR_MESSAGE);
            return ;
        }
        mErrorMessage.postValue(message);
    }

    protected R getRepository() {
        return mRepository;
    }
}
