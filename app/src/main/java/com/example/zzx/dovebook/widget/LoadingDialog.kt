package com.example.baselibrary.widget

import android.content.Context
import android.os.Bundle
import android.view.Gravity
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatDialog
import com.airbnb.lottie.LottieAnimationView
import com.example.baselibrary.R

class LoadingDialog @JvmOverloads constructor(
        context: Context
): AppCompatDialog(context) {

    var loadingView: LottieAnimationView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val contentView = layoutInflater.inflate(R.layout.view_loading, null)
        val layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT)
        loadingView = findViewById(R.id.loading_view)
        setContentView(contentView, layoutParams)

        val winLayoutParams = window.attributes
        winLayoutParams.gravity = Gravity.CENTER
        window.attributes = winLayoutParams
        setCanceledOnTouchOutside(false)
    }

    override fun dismiss() {
        loadingView?.cancelAnimation()
        super.dismiss()
    }
}