package com.example.zzx.dovebook.main.mainmessage

import android.view.MenuItem
import androidx.appcompat.widget.Toolbar
import com.example.baselibrary.commonutil.ToastUtil
import com.example.zzx.dovebook.R
import com.example.zzx.dovebook.base.BaseFragment
import com.example.zzx.dovebook.base.CommonRepository
import com.example.zzx.dovebook.base.extensions.startRequestMessageActivity
import kotlinx.android.synthetic.main.fragment_main_message.*
import kotlinx.android.synthetic.main.toolbar_common.*

class MainMessageFragment: BaseFragment<MainMessageViewModel>(), Toolbar.OnMenuItemClickListener {

    override fun createViewModel(): Class<MainMessageViewModel> = MainMessageViewModel::class.java

    override fun getLayoutId(): Int = R.layout.fragment_main_message

    override fun createRepository(): Any {
        return CommonRepository()
    }

    override fun initViews() {

        setupToolbar()

        bindMessageItems()
    }

    private fun bindMessageItems() {
        requestMessageItem.setOnClickListener {
            activity?.startRequestMessageActivity()
        }
        officialMessageItem.setOnClickListener {
            ToastUtil.shortToast(activity, "暂无消息")
        }
    }

    private fun setupToolbar() {
        inflateMenu()
    }

    private fun inflateMenu() {
        toolbar.setTitleTextAppearance(activity, R.style.BoldTitleStyle)
        toolbar.title = getString(R.string.message)
//        toolbar.inflateMenu(R.menu.main_message)
//        toolbar.setOnMenuItemClickListener(this)
//        toolbar.menu.findItem(R.id.actionFriend).setOnMenuItemClickListener {
//            onMenuItemClick(it)
//        }
    }

    override fun onMenuItemClick(item: MenuItem): Boolean {
        return false
    }
}