package com.example.zzx.dovebook.base;

public class ValueWrapper<D> {

    public static final int TYPE_NORMAL = 0;
    public static final int TYPE_NO_CONTENT = 1;
    public static final int TYPE_FAILED = 2;

    public int type;

    public D value;

    public ValueWrapper(int type, D value) {
        this.type = type;
        this.value = value;
    }
}
