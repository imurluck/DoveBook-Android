package com.example.zzx.dovebook.utils

import android.Manifest
import android.content.Context
import com.amap.api.location.AMapLocationClient
import com.amap.api.location.AMapLocationClientOption
import com.amap.api.location.AMapLocationListener
import com.amap.api.services.core.LatLonPoint
import com.amap.api.services.geocoder.GeocodeSearch
import com.amap.api.services.geocoder.GeocodeSearch.OnGeocodeSearchListener
import com.amap.api.services.geocoder.RegeocodeQuery
import com.example.baselibrary.commonutil.Logger
import com.example.baselibrary.permission.PermissionCheck
import com.example.zzx.dovebook.base.BaseApplication
import com.example.zzx.dovebook.user.UserManager

object LocationUtil {

    private val TAG = "LocationUtil"

    /**
     * 逆地址编码查询，用于通过坐标查询实际地址信息
     */
    fun geocodeSearch(context: Context, latitude: Double, longitude: Double, listener: OnGeocodeSearchListener) {
        val geocodeSearch = GeocodeSearch(context)
        val latLonPoint = LatLonPoint(latitude, longitude)
        val query = RegeocodeQuery(latLonPoint, 200F, GeocodeSearch.AMAP)
        geocodeSearch.setOnGeocodeSearchListener(listener)
        geocodeSearch.getFromLocationAsyn(query)
    }

    /**
     * 定位，并做了预处理，确保定位成功，并且对不成功的情况做了处理
     * @param listener AMapLocationListener
     */
    @PermissionCheck(permission = Manifest.permission.ACCESS_FINE_LOCATION)
    fun location(listener: AMapLocationListener) {
        val locationClient = AMapLocationClient(BaseApplication.getContext())
        locationClient.setLocationListener {
            if (it == null) {
                return@setLocationListener
            }
            if (it.errorCode != 0) {
                Logger.e(TAG, "onLocationChanged -> 定位失败!\n"
                        + it.errorCode + "-> "
                        + it.errorInfo)
                return@setLocationListener
            }
            locationClient.stopLocation()
            listener.onLocationChanged(it)
        }
        val locationOption = AMapLocationClientOption()
        locationOption.locationMode = AMapLocationClientOption.AMapLocationMode.Hight_Accuracy
        locationOption.isOnceLocationLatest = true
        locationOption.httpTimeOut = 10000L
        locationOption.isLocationCacheEnable = false
        locationClient.startLocation()
    }

    /**
     * 查询地理位置信息，如果事先没有定位过，则会先定位在查询
     * @param context Context
     * @param listener OnGeocodeSearchListener
     * @param refresh 是否刷新当前用户的坐标
     */
    fun locationAndGeocode(context: Context, listener: OnGeocodeSearchListener, refresh: Boolean) {
        if (UserManager.getInstance(context).hasLocationInfo() && !refresh) {
            val latitude = UserManager.getInstance(context).latitude
            val longitude = UserManager.getInstance(context).longitude
            geocodeSearch(context, latitude, longitude, listener)
            return
        }
        location(AMapLocationListener {
            UserManager.getInstance(context).saveLocation(it.latitude, it.longitude)
            geocodeSearch(context, it.latitude, it.longitude, listener)
        })
    }

}