package com.example.zzx.dovebook.bookshare.upload

import com.example.baselibrary.LoadCallback
import com.example.baselibrary.constant.Constant
import com.example.baselibrary.net.HttpHelper

class ScanRepository {


    fun getBookInfoFromNet(ISBN: String, loadCallback: LoadCallback<BookNet>) {
        HttpHelper.getInstance().get(Constant.BASE_URL_BOOK_ISBN, "query?isbn=$ISBN", loadCallback, TAG_GET_BOOK_FROM_NET)
    }

    companion object {
        private const val TAG_GET_BOOK_FROM_NET = "getBookFromNet"
    }
}