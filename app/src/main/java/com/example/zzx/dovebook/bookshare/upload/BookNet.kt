package com.example.zzx.dovebook.bookshare.upload

import android.os.Parcel
import android.os.Parcelable

/**
 * Bean类，从http://isbn.szmesoft.com/isbn/query?isbn=ISBN码接口返回的bean，
 * 此接口是米软科技提供的免费ISBN查询接口
 * http://isbn.szmesoft.com/ISBN/GetBookPhoto?ID=返回结果.PhotoUrl此接口是图片URL
 * create by zzx
 * create at 19-4-30
 */
data class BookNet(val ISBN: String,
                   val BookName: String,
                   val Author: String,
                   val Publishing: String,
                   val BookCode: String,
                   val ASIN: String,
                   val Brand: String?,
                   val Wright: String?,
                   val Size: String,
                   val Pages: String,
                   val Price: String?,
                   var PhotoUrl: String?,
                   val ID: String,
                   val CallTimes: Int,
                   val ErrorCode: Int,
                   val ErrorMessage: String?): Parcelable {

    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readInt(),
        parcel.readInt(),
        parcel.readString()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(ISBN)
        parcel.writeString(BookName)
        parcel.writeString(Author)
        parcel.writeString(Publishing)
        parcel.writeString(BookCode)
        parcel.writeString(ASIN)
        parcel.writeString(Brand)
        parcel.writeString(Wright)
        parcel.writeString(Size)
        parcel.writeString(Pages)
        parcel.writeString(Price)
        parcel.writeString(PhotoUrl)
        parcel.writeString(ID)
        parcel.writeInt(CallTimes)
        parcel.writeInt(ErrorCode)
        parcel.writeString(ErrorMessage)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<BookNet> {
        override fun createFromParcel(parcel: Parcel): BookNet {
            return BookNet(parcel)
        }

        override fun newArray(size: Int): Array<BookNet?> {
            return arrayOfNulls(size)
        }
    }


}