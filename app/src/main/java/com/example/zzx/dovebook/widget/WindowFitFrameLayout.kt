package com.example.zzx.dovebook.widget

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.view.WindowInsets
import android.widget.FrameLayout
import androidx.core.view.children
/**
 * 不同界面子View设置fitSystemWindows为true时，替换fragment会出现一个界面消费WindowInsets后，
 * 不能分发到其他界面，所以需要在替换fragment时重新分发一次
 * create by zzx
 * create at 19-4-28
 */
class WindowFitFrameLayout @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr) {
    /**
     * 在添加子View，也即是替换fragment时，申请重新分发WindowInsets
     */
    init {
        setOnHierarchyChangeListener(object: OnHierarchyChangeListener {
            override fun onChildViewRemoved(parent: View?, child: View?) {

            }

            override fun onChildViewAdded(parent: View?, child: View?) {
                requestApplyInsets()
            }

        })
    }

    override fun onApplyWindowInsets(insets: WindowInsets): WindowInsets {
        for (child in children) {
            child.dispatchApplyWindowInsets(insets)
        }
        return insets
    }
}