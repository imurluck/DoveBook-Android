package com.example.zzx.dovebook.booktrace.model;

import android.text.TextUtils;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.amap.api.services.core.PoiItem;
import com.amap.api.services.geocoder.GeocodeResult;
import com.amap.api.services.geocoder.GeocodeSearch;
import com.amap.api.services.geocoder.RegeocodeAddress;
import com.amap.api.services.geocoder.RegeocodeResult;
import com.example.baselibrary.commonutil.FormatUtil;
import com.example.baselibrary.commonutil.Logger;
import com.example.library.BaseAdapter;
import com.example.library.IEntity;
import com.example.zzx.dovebook.R;
import com.example.zzx.dovebook.booktrace.BookTraceActivity;
import com.example.zzx.dovebook.utils.LocationUtil;
import com.example.zzx.heartanimation.LikeLayout;

public class BookTrace implements IEntity<BookTrace> {

    private static final String TAG = "BookTrace";

    private String bookShareId;

    private String bookId;

    private long createAt;

    private String originUserId;

    private String uploadUserId;

    private String uploadUserName;

    private String uploadUserSex;

    private String uploadUserAvatarPath;

    private String receiveUserId;

    private String receiveUserName;

    private String receiveUserSex;

    private String receiveUserAvatarPath;

    private double latitude;

    private double longitude;

    public String getBookShareId() {
        return bookShareId;
    }

    public void setBookShareId(String bookShareId) {
        this.bookShareId = bookShareId;
    }

    public String getBookId() {
        return bookId;
    }

    public void setBookId(String bookId) {
        this.bookId = bookId;
    }

    public long getCreateAt() {
        return createAt;
    }

    public void setCreateAt(long createAt) {
        this.createAt = createAt;
    }

    public String getOriginUserId() {
        return originUserId;
    }

    public void setOriginUserId(String originUserId) {
        this.originUserId = originUserId;
    }

    public String getUploadUserId() {
        return uploadUserId;
    }

    public void setUploadUserId(String uploadUserId) {
        this.uploadUserId = uploadUserId;
    }

    public String getUploadUserName() {
        return uploadUserName;
    }

    public void setUploadUserName(String uploadUserName) {
        this.uploadUserName = uploadUserName;
    }

    public String getUploadUserSex() {
        return uploadUserSex;
    }

    public void setUploadUserSex(String uploadUserSex) {
        this.uploadUserSex = uploadUserSex;
    }

    public String getUploadUserAvatarPath() {
        return uploadUserAvatarPath;
    }

    public void setUploadUserAvatarPath(String uploadUserAvatarPath) {
        this.uploadUserAvatarPath = uploadUserAvatarPath;
    }

    public String getReceiveUserId() {
        return receiveUserId;
    }

    public void setReceiveUserId(String receiveUserId) {
        this.receiveUserId = receiveUserId;
    }

    public String getReceiveUserName() {
        return receiveUserName;
    }

    public void setReceiveUserName(String receiveUserName) {
        this.receiveUserName = receiveUserName;
    }

    public String getReceiveUserSex() {
        return receiveUserSex;
    }

    public void setReceiveUserSex(String receiveUserSex) {
        this.receiveUserSex = receiveUserSex;
    }

    public String getReceiveUserAvatarPath() {
        return receiveUserAvatarPath;
    }

    public void setReceiveUserAvatarPath(String receiveUserAvatarPath) {
        this.receiveUserAvatarPath = receiveUserAvatarPath;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    @Override
    public int getLayoutId() {
        return R.layout.item_book_trace;
    }

    @Override
    public void bindView(BaseAdapter baseAdapter, BaseAdapter.ViewHolder holder, BookTrace data, int position) {
        RelativeLayout rootView = (RelativeLayout) holder.itemView;
        BookTraceActivity activity = (BookTraceActivity) rootView.getContext();
        ((TextView) rootView.findViewById(R.id.uploadUserName)).setText(uploadUserName);
        ((TextView) rootView.findViewById(R.id.uploadType)).setText(getType());
        ((TextView) rootView.findViewById(R.id.timeTv)).setText(FormatUtil.dateFormatWithN(createAt));
        if (TextUtils.isEmpty(receiveUserId)) {
            rootView.findViewById(R.id.receiveLayout).setVisibility(View.GONE);
            rootView.findViewById(R.id.contactTv).setVisibility(View.VISIBLE);
            rootView.findViewById(R.id.contactTv).setOnClickListener((view) -> {
                activity.requestReceive(bookId, bookShareId, uploadUserId);
            });
        }
        ((TextView) rootView.findViewById(R.id.receiveUserName)).setText(receiveUserName + "");
        LikeLayout locationLayout = rootView.findViewById(R.id.locationLayout);
        TextView detailLocationTv = rootView.findViewById(R.id.detailLocationTv);
        if (isLocationAvailable()) {
            locationLayout.setVisibility(View.VISIBLE);
            LocationUtil.INSTANCE.geocodeSearch(activity, latitude, longitude, new GeocodeSearch.OnGeocodeSearchListener() {
                @Override
                public void onRegeocodeSearched(RegeocodeResult regeocodeResult, int i) {
                    if (i != 1000) {
                        Logger.e(TAG, "onRegeocodeSearched -> 位置搜索失败("
                                + latitude + ", " + longitude + ")");
                        return;
                    }
                    RegeocodeAddress address = regeocodeResult.getRegeocodeAddress();
                    String locationInfo = address.getProvince() + address.getCity() + address.getDistrict();
                    String detailLocationInfo;
                    if (address.getPois().size() > 0) {
                        PoiItem poiItem = address.getPois().get(0);
                        detailLocationTv.setVisibility(View.VISIBLE);
                        detailLocationInfo = poiItem.getTitle();
                        detailLocationTv.setText(detailLocationInfo);
                    } else {
                        detailLocationTv.setVisibility(View.GONE);
                    }
                    ((LikeLayout) rootView.findViewById(R.id.locationLayout)).setText(locationInfo);
                }

                @Override
                public void onGeocodeSearched(GeocodeResult geocodeResult, int i) {

                }
            });
        } else {
            locationLayout.setVisibility(View.GONE);
        }
    }

    private String getType() {
        if (TextUtils.equals(uploadUserId, originUserId)) {
            return "上传";
        } else {
            return "传递";
        }
    }

    private boolean isLocationAvailable() {
        return !(latitude == 0.0 && longitude == 0.0);
    }
}
