package com.example.zzx.dovebook.login.ui;


import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.FrameLayout;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleRegistry;
import butterknife.BindView;
import com.example.zzx.dovebook.R;
import com.example.zzx.dovebook.base.BaseActivity;
import com.example.zzx.dovebook.main.MainActivity;
import com.example.zzx.dovebook.user.UserManager;


public class LoginActivity extends BaseActivity<LoginViewModel> {

    @BindView(R.id.login_container)
    FrameLayout container;

    private FragmentManager mFragmentManager;

    private Fragment mLoginFragment;
    private Fragment mSignupFragment;

    private String mCallId;

    //插件的BUG，需要手动设置生命周期，否则LiveData无法回调
    @Override
    protected void onStart() {
        super.onStart();
        ((LifecycleRegistry) getLifecycle()).handleLifecycleEvent(Lifecycle.Event.ON_START);
    }

    @Override
    protected void onPause() {
        super.onPause();
        ((LifecycleRegistry) getLifecycle()).handleLifecycleEvent(Lifecycle.Event.ON_PAUSE);
    }

    @Override
    protected void onResume() {
        super.onResume();
        ((LifecycleRegistry) getLifecycle()).handleLifecycleEvent(Lifecycle.Event.ON_RESUME);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ((LifecycleRegistry) getLifecycle()).handleLifecycleEvent(Lifecycle.Event.ON_DESTROY);
    }

    @Override
    protected void onStop() {
        super.onStop();
        ((LifecycleRegistry) getLifecycle()).handleLifecycleEvent(Lifecycle.Event.ON_STOP);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((LifecycleRegistry) getLifecycle()).handleLifecycleEvent(Lifecycle.Event.ON_CREATE);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_login;
    }

    @Override
    protected Class<LoginViewModel> createViewModel() {
        return LoginViewModel.class;
    }

    @Override
    protected Object createRepository() {
        return new LoginRepository(UserManager.getInstance(getApplication()));
    }


    /**
     * 初始化View
     * @param savedInstanceState
     */
    @Override
    protected void initViews(Bundle savedInstanceState) {

    }

    @Override
    protected void initIntent(Bundle savedInstanceState) {
        super.initIntent(savedInstanceState);
        Intent intent = getIntent();
        mCallId = intent.getStringExtra(getString(R.string.call_id_arg_name));
    }

    @Override
    protected void initOptions(Bundle savedInstanceState) {
        setupFragmentManager(savedInstanceState);
    }

    private void setupFragmentManager(Bundle savedInstanceState) {
        if (savedInstanceState == null) {
            mLoginFragment = new LoginFragment();
            mSignupFragment = new SignupFragment();
        }
        mFragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = mFragmentManager.beginTransaction();
        transaction
                .add(R.id.login_container, mLoginFragment, mLoginFragment.getClass().getSimpleName())
                .commit();
    }

    @Override
    protected int getStatusBarColor() {
        return STATUS_BAR_TRANSPARENT;
    }

    public void startSignupFragment(View shareView, String transtionName) {
        FragmentTransaction fragmentTransaction= mFragmentManager.beginTransaction();
        if (shareView != null && !TextUtils.isEmpty(transtionName)) {
            fragmentTransaction.addSharedElement(shareView, transtionName);
        }
        fragmentTransaction.addToBackStack(mLoginFragment.getClass().getSimpleName())
                .replace(R.id.login_container, mSignupFragment)
                .commit();
    }

    public void login(String userName, String password) {
        getViewModel().login(userName, password).observe(this, (user) -> {
            finishForResult();
        });
    }

    public void signUp(String userName, String password) {
        getViewModel().signUp(userName, password).observe(this, (user) -> {
            finishForResult();
        });
    }

    public void finishForResult() {
        if (mCallId != null) {
            Intent result = new Intent();
            result.putExtra(getString(R.string.call_id_arg_name), mCallId);
            setResult(RESULT_OK, result);
            finish();
        } else {
            finish();
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
        }
    }
}
