package com.example.zzx.dovebook.message.requestme

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.baselibrary.LoadCallback
import com.example.baselibrary.net.HttpUtil
import com.example.zzx.dovebook.base.BaseLiveData
import com.example.zzx.dovebook.base.BaseViewModel
import com.example.zzx.dovebook.base.ValueWrapper
import com.example.zzx.dovebook.base.ValueWrapper.*
import com.example.zzx.dovebook.message.model.BookShareRequest
import com.example.zzx.dovebook.message.model.BookShareRequestMe

class RequestMeViewModel(reponsity: RequestMeRepository): BaseViewModel<RequestMeRepository>(reponsity) {

    private val queryRequestMeKeys = arrayOf("uploadUserId", "createAt")

    private val agreeRequestKeys = arrayOf("bsqId", "uploadUserId")

    fun queryRequestMe(uploadUserId: String, lastItemCreateAt: Long): LiveData<ValueWrapper<Any>> {
        val result = BaseLiveData<List<BookShareRequestMe>>()
        val paraMap = HttpUtil.wrapParams(queryRequestMeKeys, uploadUserId, lastItemCreateAt)
        repository.queryRequestMe(paraMap, object: LoadCallback<List<BookShareRequest>> {
            override fun loadSuccess(data: List<BookShareRequest>) {
                val requestMeList = mutableListOf<BookShareRequestMe>()
                for (bookShareRequest in data) {
                    requestMeList.add(BookShareRequestMe(bookShareRequest))
                }
                result.postValue(requestMeList, TYPE_NORMAL)
            }

            override fun loadFailed(errorMessage: String?) {
                postErrorMessage(errorMessage)
                result.postValue(null, TYPE_FAILED)
            }

            override fun loadNull(message: String?) {
                result.postValue(null, TYPE_NO_CONTENT)
            }

        })
        return result
    }

    fun agreeRequest(bsqId: String, uploadUserId: String): LiveData<Boolean> {
        val result = MutableLiveData<Boolean>()
        val paraMap = HttpUtil.wrapParams(agreeRequestKeys, bsqId, uploadUserId)
        repository.agreeRequest(paraMap, object: LoadCallback<Boolean> {
            override fun loadSuccess(data: Boolean) {
                result.postValue(data)
            }

            override fun loadFailed(errorMessage: String?) {
                postErrorMessage(errorMessage)
            }

            override fun loadNull(message: String?) {

            }

        })
        return result
    }

}