package com.example.zzx.dovebook.net;

import com.example.baselibrary.net.Response;
import com.example.zzx.dovebook.base.model.BookShare;
import com.example.zzx.dovebook.bookshare.booksharehome.model.Comment;

import java.util.List;
import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;

public interface Api {

    /**
     * 查询用户传递的图书分享集合
     * @param userId
     * @return
     */
//    @FormUrlEncoded
//    @POST(value = "book/bookShare/queryPassedBookShare")
//    Call<Response<List<BookShare>>> queryPassedBookShare(@Field("userId") String userId,
//                                                         @Field("createAt") long createAt);

    @FormUrlEncoded
    @POST(value = "book/bookShare/queryPassedBookShare")
    Call<Response<List<BookShare>>> queryPassedBookShare(@Field("userId") String userId,
                                                         @Field("createAt") long createAt);

    @FormUrlEncoded
    @POST(value = "book/bookShare/queryUploadBookShare")
    Call<Response<List<BookShare>>> queryUploadBookShare(@Field("userId") String userId,
                                                         @Field("createAt") long createAt);

    @FormUrlEncoded
    @POST(value = "book/bookShare/queryHotBookShare")
    Call<Response<List<BookShare>>> queryHotBookShare(@Field("userId") String userId,
                                                      @Field("createAt") long createAt);

    @Multipart
    @POST(value = "book/bookShare/addBookShare")
    Call<Response<Boolean>> uploadBookShare(@PartMap Map<String, RequestBody> params,
                                              @Part MultipartBody.Part coverFile);

    @Multipart
    @POST(value = "book/bookShare/queryComments")
    Call<Response<List<Comment>>> queryComments(@PartMap Map<String, RequestBody> params);

    @Multipart
    @POST(value = "book/bookShare/addComment")
    Call<Response<Comment>> addComment(@PartMap Map<String, RequestBody> params);

    @Multipart
    @POST(value = "book/bookShare/replyComment")
    Call<Response<Comment>> replyComment(@PartMap Map<String, RequestBody> params);

    @FormUrlEncoded
    @POST(value = "book/bookShare/bookShareLike")
    Call<Response<Boolean>> bookShareLike(@Field("bookShareId") String bookShareId,
                                          @Field("userId") String userId);
    @FormUrlEncoded
    @POST(value = "book/bookShare/cancelBookShareLike")
    Call<Response<Boolean>> cancelBookShareLike(@Field("bookShareId") String bookShareId,
                                                @Field("userId") String userId);
}
