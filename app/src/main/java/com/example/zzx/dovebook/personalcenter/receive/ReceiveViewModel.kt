package com.example.zzx.dovebook.personalcenter.receive

import androidx.lifecycle.LiveData
import com.example.baselibrary.LoadCallback
import com.example.baselibrary.net.HttpUtil
import com.example.zzx.dovebook.base.BaseLiveData
import com.example.zzx.dovebook.base.BaseViewModel
import com.example.zzx.dovebook.base.ValueWrapper
import com.example.zzx.dovebook.base.ValueWrapper.*
import com.example.zzx.dovebook.base.model.BookShare

class ReceiveViewModel(repository: ReceiveRepository): BaseViewModel<ReceiveRepository>(repository) {

    private val queryReceiveBookShareKeys = arrayOf("userId", "createAt")

    fun queryReceiveBookShare(userId: String, lastItemCreateAt: Long): LiveData<ValueWrapper<Any>> {
        val result = BaseLiveData<List<BookShareReceive>>()
        val paraMap = HttpUtil.wrapParams(queryReceiveBookShareKeys, userId, lastItemCreateAt)
        repository.queryReceiveBookShare(paraMap, object: LoadCallback<List<BookShare>> {
            override fun loadSuccess(data: List<BookShare>) {
                val dataList = mutableListOf<BookShareReceive>()
                for (bookShare in data) {
                    dataList.add(BookShareReceive(bookShare))
                }
                result.postValue(dataList, TYPE_NORMAL)
            }

            override fun loadFailed(errorMessage: String?) {
                postErrorMessage(errorMessage)
                result.postValue(null, TYPE_FAILED)
            }

            override fun loadNull(message: String?) {
                result.postValue(null, TYPE_NO_CONTENT)
            }

        })
        return result
    }


}