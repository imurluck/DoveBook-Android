package com.example.zzx.dovebook.personalcenter.follow.follower

import com.example.baselibrary.LoadCallback
import com.example.baselibrary.constant.Constant
import com.example.baselibrary.net.HttpHelper
import com.example.zzx.dovebook.personalcenter.follow.UserFollow

class FollowerRepository {

    private val TAG_QUERY_FOLLOWERS = "queryFollowers"

    private val TAG_FOLLOW = "follow"

    private val TAG_CANCEL_FOLLOW = "cancelFollow"

    fun queryFollowers(paraMap: Map<String, String>, loadCallback: LoadCallback<List<UserFollow>>) {
        HttpHelper.getInstance().post(Constant.URL_QUERY_FOLLOWERS, paraMap, loadCallback, TAG_QUERY_FOLLOWERS)
    }

    fun follow(paraMap: Map<String, String>, loadCallback: LoadCallback<Int>) {
        HttpHelper.getInstance().post(Constant.URL_USER_FOLLOW, paraMap, loadCallback, TAG_FOLLOW)
    }

    fun cancelFollow(paraMap: Map<String, String>, loadCallback: LoadCallback<Int>) {
        HttpHelper.getInstance().post(Constant.URL_USER_CANCEL_FOLLOW, paraMap, loadCallback, TAG_CANCEL_FOLLOW)
    }

}