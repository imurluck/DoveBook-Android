package com.example.zzx.dovebook.booksearch.searchhistory

import com.example.zzx.dovebook.base.BaseViewModel

class SearchHistoryViewModel(repository: SearchHistoryRepository): BaseViewModel<SearchHistoryRepository>(repository) {


    fun getHistoryFromLocal(searchHistoryFragment: SearchHistoryFragment): List<SearchHistory>? {
        val historyList = repository.getHistoryFromLocal()
        return if (historyList == null) {
            null
        } else {
            mutableListOf<SearchHistory>().apply {
                for (content in historyList) {
                    add(SearchHistory(searchHistoryFragment, content))
                }
            }
        }
    }

    fun updateHistory(content: String): Boolean = repository.updateHistory(content)


    fun deleteHistory(content: String): Boolean = repository.deleteHistory(content)
    fun addHistory(content: String): Boolean = repository.addHistory(content)
}