package com.example.zzx.dovebook.personalcenter.follow

data class UserFollowCount(var followerCount: Int,
                           var followingCount: Int)