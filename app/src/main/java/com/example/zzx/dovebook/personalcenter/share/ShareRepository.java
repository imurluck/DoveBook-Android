package com.example.zzx.dovebook.personalcenter.share;

import com.example.baselibrary.LoadCallback;
import com.example.baselibrary.constant.Constant;
import com.example.baselibrary.net.HandleCallbackImpl;
import com.example.baselibrary.net.HttpManager;
import com.example.zzx.dovebook.base.model.BookShare;
import com.example.zzx.dovebook.net.Api;

import java.util.List;

public class ShareRepository {

    private static final String TAG_QUERY_UPLOAD_BOOK_SHARE = "queryUploadBookShare";

    public void queryUploadBookShare(String userId, long createAt, LoadCallback<List<BookShare>> loadCallback) {
        Api api = HttpManager.getInstance().getApiService(Api.class, Constant.BASE_URL, TAG_QUERY_UPLOAD_BOOK_SHARE);
        if (api == null) {
            return ;
        }
        api.queryUploadBookShare(userId, createAt)
                .enqueue(new HandleCallbackImpl<>(loadCallback, TAG_QUERY_UPLOAD_BOOK_SHARE));
    }
}
