package com.example.zzx.dovebook.booksearch.searchhistory

import android.widget.ImageView
import android.widget.TextView
import com.example.library.BaseAdapter
import com.example.library.IEntity
import com.example.zzx.dovebook.R

class SearchHistory(
    private val searchHistoryFragment: SearchHistoryFragment,
    val historyContent: String
): IEntity<SearchHistory> {

    override fun bindView(
        baseAdapter: BaseAdapter,
        holder: BaseAdapter.ViewHolder,
        data: SearchHistory,
        position: Int
    ) {
        holder.itemView.apply {
            findViewById<ImageView>(R.id.deleteIcon).setOnClickListener {
                searchHistoryFragment.onDeleteIconClick(historyContent, holder.adapterPosition)
            }
            findViewById<TextView>(R.id.searchContentTv).text = historyContent
            setOnClickListener {
                searchHistoryFragment.onHistoryClick(historyContent, holder.adapterPosition)
            }
        }
    }

    override fun getLayoutId(): Int = R.layout.item_search_history
}