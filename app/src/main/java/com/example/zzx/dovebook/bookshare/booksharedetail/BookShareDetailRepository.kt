package com.example.zzx.dovebook.bookshare.booksharedetail

import com.example.baselibrary.LoadCallback
import com.example.baselibrary.constant.Constant
import com.example.baselibrary.net.HttpHelper
import com.example.zzx.dovebook.base.CommonRepository
import com.example.zzx.dovebook.bookshare.booksharehome.model.Comment

class BookShareDetailRepository: CommonRepository() {

    private val TAG_QUERY_COMMENTS = "queryComment"

    private val TAG_ADD_COMMENT = "addComment"

    private val TAG_REPLY_COMMENT = "replyComment"

    fun queryComments(params: Map<String, String>, loadCallback: LoadCallback<List<Comment>>) {
        HttpHelper.getInstance().post(Constant.URL_QUERY_COMMENTS, params,
            loadCallback, TAG_QUERY_COMMENTS)
    }

    fun addComment(params: Map<String, String>, loadCallback: LoadCallback<Comment>) {
        HttpHelper.getInstance().post(Constant.URL_ADD_COMMENT, params,
            loadCallback, TAG_ADD_COMMENT)
    }

    fun replyComment(params: Map<String, String>, loadCallback: LoadCallback<Comment>) {
        HttpHelper.getInstance().post(Constant.URL_REPLY_COMMENT, params,
            loadCallback, TAG_REPLY_COMMENT)
    }
}