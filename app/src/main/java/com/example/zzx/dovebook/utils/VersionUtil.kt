package com.example.zzx.dovebook.utils

import android.content.Context
import android.os.Build

internal fun Context.getVersionInfo(): String {
    val versionCode = getVersionCode()
    return "V${versionCode / 100}.${versionCode % 100 / 10}.${versionCode % 10}"
}

internal fun Context.getVersionCode(): Long =
    if (Build.VERSION.SDK_INT >= 28) {
        packageManager.getPackageInfo(packageName, 0).longVersionCode
    } else {
        packageManager.getPackageInfo(packageName, 0).versionCode.toLong()
    }