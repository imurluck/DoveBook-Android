package com.example.zzx.dovebook.base

import com.example.zzx.dovebook.main.booksea.recommendation.ListBookShare
import com.example.zzx.heartanimation.LikeLayout

interface UserContext {

    fun bookShareLike(listBookShare: ListBookShare, targetView: LikeLayout, bookShareId: String)

    fun cancelBookShareLike(listBookShare: ListBookShare, targetView: LikeLayout, bookShareId: String)
}