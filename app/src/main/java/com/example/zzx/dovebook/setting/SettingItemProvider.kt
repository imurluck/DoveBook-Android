package com.example.zzx.dovebook.setting

import com.example.zzx.dovebook.R
import com.qmuiteam.qmui.widget.grouplist.QMUICommonListItemView
import com.qmuiteam.qmui.widget.grouplist.QMUICommonListItemView.ACCESSORY_TYPE_CHEVRON
import com.qmuiteam.qmui.widget.grouplist.QMUICommonListItemView.VERTICAL
import com.qmuiteam.qmui.widget.grouplist.QMUIGroupListView

object SettingItemProvider {

    fun getUserInfoItem(groupListView: QMUIGroupListView): QMUICommonListItemView =
        groupListView.createItemView(groupListView.context.getString(R.string.setting_profile)).apply {
            accessoryType = ACCESSORY_TYPE_CHEVRON
        }

    fun getCacheInfoItem(groupListView: QMUIGroupListView): QMUICommonListItemView =
            groupListView.createItemView(groupListView.context.getString(R.string.setting_clear_buffer)).apply {
                orientation = VERTICAL
            }

    fun getVersionInfoItem(groupListView: QMUIGroupListView): QMUICommonListItemView =
        groupListView.createItemView(groupListView.context.getString(R.string.setting_version_code)).apply {
            orientation = VERTICAL
        }
}