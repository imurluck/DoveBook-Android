package com.example.zzx.dovebook.login.ui;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.transition.Transition;
import android.transition.TransitionInflater;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.animation.AccelerateInterpolator;
import android.widget.EditText;
import android.widget.RelativeLayout;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import butterknife.BindView;
import butterknife.OnClick;
import com.example.baselibrary.commonutil.SnackbarUtil;
import com.example.zzx.dovebook.R;
import com.example.zzx.dovebook.base.BaseFragment;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class SignupFragment extends BaseFragment {

    @BindView(R.id.signup_login_fab)
    FloatingActionButton loginFab;

    @BindView(R.id.signup_root_layout)
    RelativeLayout rootLayout;

    @BindView(R.id.signup_username_edit)
    EditText userNameEdit;
    @BindView(R.id.signup_user_password_edit)
    EditText passwordEdit;
    @BindView(R.id.signup_user_password_confirm_edit)
    EditText passwordConfirmEdit;


    @OnClick(R.id.signup_signup_btn)
    public void signUp() {
        String password = passwordEdit.getText().toString();
        String passwordConfim = passwordConfirmEdit.getText().toString();
        if (!TextUtils.equals(password, passwordConfim)) {
            SnackbarUtil.shortSnackbar(passwordEdit, "两次密码不相同");
            return ;
        }
        ((LoginActivity) getActivity()).signUp(userNameEdit.getText().toString(), password);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            showEnterAnimation();
        }
    }

    @Override
    protected void initViews() {
        setupLoginFab();
    }

    private void showEnterAnimation() {
        Transition transition = TransitionInflater.from(getActivity())
                .inflateTransition(R.transition.fab_transition);
        setSharedElementEnterTransition(transition);
        transition.addListener(new Transition.TransitionListener() {
            @Override
            public void onTransitionStart(Transition transition) {
                rootLayout.setVisibility(View.GONE);
            }

            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onTransitionEnd(Transition transition) {
                animateRevealShow();
                transition.removeListener(this);
            }

            @Override
            public void onTransitionCancel(Transition transition) {

            }

            @Override
            public void onTransitionPause(Transition transition) {

            }

            @Override
            public void onTransitionResume(Transition transition) {

            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public void animateRevealClose() {
        Animator mAnimator = ViewAnimationUtils.createCircularReveal(rootLayout, rootLayout.getWidth() / 2, 0,
                rootLayout.getHeight(), loginFab.getWidth() / 2);
        mAnimator.setDuration(500);
        mAnimator.setInterpolator(new AccelerateInterpolator());
        mAnimator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                rootLayout.setVisibility(View.INVISIBLE);
                getActivity().onBackPressed();
            }

            @Override
            public void onAnimationStart(Animator animation) {
                loginFab.setClickable(false);
                super.onAnimationStart(animation);
            }
        });
        mAnimator.start();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void animateRevealShow() {
        Animator mAnimator = ViewAnimationUtils.createCircularReveal(rootLayout, rootLayout.getWidth() / 2, 0,
                loginFab.getWidth() / 2, rootLayout.getHeight());
        mAnimator.setDuration(500);
        mAnimator.setInterpolator(new AccelerateInterpolator());
        mAnimator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
            }

            @Override
            public void onAnimationStart(Animator animation) {
                rootLayout.setVisibility(View.VISIBLE);
                super.onAnimationStart(animation);
            }
        });
        mAnimator.start();
    }

    private void setupLoginFab() {
        loginFab.setOnClickListener((view) -> {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                animateRevealClose();
            } else {
                getActivity().onBackPressed();
            }
        });

    }

    @Override
    protected Class createViewModel() {
        return null;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_signup;
    }
}
